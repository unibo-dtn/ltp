local build(tag, failure) =
  {
    image: "gcc:" + tag,
    stage: "build",
    allow_failure: failure,
    script: '( apt-get update && apt-get install -y make wget mercurial automake autoconf patch nmap tcl libc6-dev nano tcl-dev libdb-dev libssl-dev g++ libjson-c-dev --no-install-recommends && mkdir -p "${ION_FOLDER}" && cd "${ION_FOLDER}" && wget "http://downloads.sourceforge.net/project/ion-dtn/${ION_VERSION}.tar.gz" --no-check-certificate && tar -zxvf "${ION_VERSION}.tar.gz" && cd "${ION_VERSION}" && autoreconf -if && ./configure && make && make install && ldconfig ) && make "ION_DIR=${ION_FOLDER}/${ION_VERSION}" BP=${BP_VERSION}'
  };

local builds = [
  {
    tag: "9.5",
    allow_failure: false
  }
  ,{
    tag: "10.4",
    allow_failure: false
  }
  ,{
    tag: "11.3",
    allow_failure: false
  }
  ,{
    tag: "12.1",
    allow_failure: true
  }
  ,{
    tag: "12.2",
    allow_failure: true
  }
  ,{
    tag: "latest",
    allow_failure: true
  }
];

{
  ['gcc:' + current.tag]: build(current.tag, current.allow_failure)
  for current in builds
}
