PROJECT_ROOT = $(dir $(abspath $(lastword $(MAKEFILE_LIST))))/src/

DEBUG=0
BP=bpv7
DESTDIR=
INSTALL_PATH=usr/local/bin

HAVE_UNIBO_BP := 0
HAVE_ION := 0

ifneq ($(strip $(ION_DIR)),)
HAVE_ION := 1
all: bin-ion
else ifeq ($(UNIBO_BP),1)
HAVE_UNIBO_BP := 1
all: bin-unibo-bp
else
all: help
endif

C_FILES  = $(PROJECT_ROOT)/*.c
C_FILES += $(PROJECT_ROOT)/receiver/*.c
C_FILES += $(PROJECT_ROOT)/sender/*.c
C_FILES += $(PROJECT_ROOT)/adapters/protocol/*.c
C_FILES += $(PROJECT_ROOT)/adapters/protocol/lowerProtocols/*.c
C_FILES += $(PROJECT_ROOT)/adapters/protocol/upperProtocol/*.c
C_FILES += $(PROJECT_ROOT)/sdnv/*.c
C_FILES += $(PROJECT_ROOT)/LTPsegment/*.c
C_FILES += $(PROJECT_ROOT)/LTPsession/*.c
C_FILES += $(PROJECT_ROOT)/LTPspan/*.c
C_FILES += $(PROJECT_ROOT)/contact/*.c
C_FILES += $(PROJECT_ROOT)/range/*.c
C_FILES += $(PROJECT_ROOT)/list/*.c
C_FILES += $(PROJECT_ROOT)/timer/*.c
C_FILES += $(PROJECT_ROOT)/generic/*.c
C_FILES += $(PROJECT_ROOT)/spanFile/*.c
C_FILES += $(PROJECT_ROOT)/logger/*.c
C_FILES += $(PROJECT_ROOT)/network/network_utils.c

BIN_NAME = UniboLTP

INSTALLED=$(wildcard $(INSTALL_PATH)/$(BIN_NAME))

ifeq ($(DEBUG),0)
CFLAGS = -O2
else
CFLAGS = -g -fno-inline -O0
endif

CFLAGS += -rdynamic
LDFLAGS = -pthread -lm -ljson-c

ifeq ($(HAVE_ION),1)
LDFLAGS += -lbp -lici
CFLAGS += -I "$(ION_DIR)" -I "$(ION_DIR)/$(BP)/include" -I "$(ION_DIR)/$(BP)/library" -I "$(ION_DIR)/ici/include"
#CPPFLAGS = -DHAVE_ION -DBUILD_FOR_ION=1
CPPFLAGS = -DHAVE_ION 
else ifeq ($(HAVE_UNIBO_BP),1)
LDFLAGS += -lunibo-bp-api
CPPFLAGS = -DHAVE_UNIBO_BP -DBUILD_FOR_ION=0
endif

ifeq ($(BP),bpv6)
CFLAGS += -DBP=6 
else
CFLAGS += -DBP=7
endif

OBJS = *.o

bin-ion:
# Check if ION_DIR is correct, if not exists print error & exit
	@if test ! -d "$(ION_DIR)"; then echo "Directory \"$(ION_DIR)\" not exists. Please check out ION_DIR variable"; false; else true; fi
	@if test ! -d "$(ION_DIR)/$(BP)"; then echo "Directory \"$(ION_DIR)/$(BP)\" not exists. Please check out BP variable"; false; else true; fi

	$(CC) $(CFLAGS) $(CPPFLAGS) $(C_FILES) $(LDFLAGS) -o $(BIN_NAME) $^

bin-unibo-bp:
	$(CC) $(CFLAGS) $(CPPFLAGS) $(C_FILES) $(LDFLAGS) -o $(BIN_NAME) $^

uninstall:
	@if test `echo $(INSTALLED) | wc -w` -eq 1 -a -f "$(INSTALLED)"; then rm -rf $(INSTALLED); else echo "NOT INSTALLED"; fi

install:
	cp $(BIN_NAME) $(DESTDIR)/$(INSTALL_PATH)/

clean:
	rm -fr $(BIN_NAME) $(OBJS)

help:
	@echo "Usage: make < ION_DIR=<ion_dir> | UNIBO_BP=1 >"
	@echo "To compile with debug symbols, add DEBUG=1"
	@echo "To set compatibility with BPv6, add BP=bpv6; BP=bpv7 is default. For old ION versions use BP=bp"

