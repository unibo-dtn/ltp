/** \file list.h
 *
 * \brief This file contains the headers of my Linked List generic tool. Please check out the git: https://gitlab.com/andreabisacchi/Generic-List
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef LIST_H_
#define LIST_H_

#include <stdbool.h>
#include <stddef.h>

/**
 * \brief Node
 */
typedef struct Node
{
	/**
	 * \brief data
	 */
	void* data;
	/**
	 * \brief data size
	 */
	size_t data_size;

	/**
	 * \brief Next node
	 */
	struct Node* next;
} Node;

/**
 * \brief List
 */
typedef Node* List;

/**
 * \brief An empty list
 *
 * \hideinitializer
 */
#define empty_list NULL

/**
 * \par Function Name:
 *      list_duplicate
 *
 * \brief Duplicates a list
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return List
 *
 * \retval The new list
 *
 * \param  list		The list to duplicate
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
List list_duplicate(List list);

/**
 * \par Function Name:
 *      list_destroy
 *
 * \brief Destroys a list
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  list		The list to destroy
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void list_destroy(List* list);

/**
 * \par Function Name:
 *      list_insert_index
 *
 * \brief Inserts a data in the list at a specified index
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void*
 *
 * \retval The pointer to the new data allocated
 *
 * \param  list			The list
 * \param  data			The data to insert
 * \param  data_size	The data size
 * \param  index		The index
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void* list_insert_index(List* list, void* data, size_t data_size, int index);

/**
 * \par Function Name:
 *      list_push_front
 *
 * \brief Inserts at the start of the list an element
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void*
 *
 * \retval The pointer of the new data allocated
 *
 * \param  list			The list
 * \param  data			The data to insert
 * \param  data_size	The data size
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void* list_push_front(List* list, void* data, size_t data_size);

/**
 * \par Function Name:
 *      list_push_ordered
 *
 * \brief Inserts in an ordered way in a list an element according to a compare function passed
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void*
 *
 * \retval The pointer of the new data allocated
 *
 * \param  list			The list
 * \param  data			The data to insert
 * \param  data_size	The data size
 * \param  compare		The compare function
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void* list_push_ordered(List *list, void* data, size_t data_size, int (*compare)(void*,size_t,void*,size_t));

/**
 * \par Function Name:
 *      list_push_back
 *
 * \brief Inserts at the end of the list an element
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void*
 *
 * \retval The pointer of the new data allocated
 *
 * \param  list			The list
 * \param  data			The data to insert
 * \param  data_size	The data size
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void* list_push_back(List* list, void* data, size_t data_size);

/**
 * \par Function Name:
 *      list_append
 *
 * \brief Inserts at the end of the list an element
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void*
 *
 * \retval The pointer of the new data allocated
 *
 * \param  list			The list
 * \param  data			The data to insert
 * \param  data_size	The data size
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void* list_append(List* list, void* data, size_t data_size);

/**
 * \par Function Name:
 *      list_get_value_index
 *
 * \brief Gets the value at the specified index from the list
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void*
 *
 * \retval A copy of the data in the list. You should FREE the resource!
 *
 * \param  list			The list
 * \param  index		The index
 * \param  data_size	The data size of the data returned. If NULL is ignored
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void* list_get_value_index(List list, int index, size_t* data_size);

/**
 * \par Function Name:
 *      list_get_pointer_index
 *
 * \brief Returns the pointer to the data at the specified index
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void*
 *
 * \retval The pointer of the data inside the list
 *
 * \param  list			The list
 * \param  index		The index
 * \param  data_size	The data size of the data returned. If NULL is ignored
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void* list_get_pointer_index(List list, int index, size_t* data_size);

/**
 * \par Function Name:
 *      list_length
 *
 * \brief Returns the length of the list
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return int
 *
 * \retval The length of the list
 *
 * \param  list			The list
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
int list_length(List list);

/**
 * \par Function Name:
 *      list_remove_index_get_pointer
 *
 * \brief Removes the element at the specified index from the index and returns the pointer
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void*
 *
 * \retval The pointer of the data removed from the list. You should FREE the resource!
 *
 * \param  list			The list
 * \param  index		The index
 * \param  data_size	The data size of the removed data. If NULL it is ignored
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void* list_remove_index_get_pointer(List* list, int index, size_t* data_size);

/**
 * \par Function Name:
 *      list_pop_front
 *
 * \brief Removes the first element and returns the data
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void*
 *
 * \retval The pointer of the data in the first element. You should FREE the resource!
 *
 * \param  list			The list
 * \param  data_size	The data size of the returned value. If NULL is ignored
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void* list_pop_front(List* list, size_t* data_size);

/**
 * \par Function Name:
 *      list_pop_back
 *
 * \brief Removes and returns the last value in the list
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void*
 *
 * \retval The pointer of the data removed. You should FREE the resource!
 *
 * \param  list			The list
 * \param  data_size	The data size of the removed element. If NULL is ignored.
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void* list_pop_back(List* list, size_t* data_size);

/**
 * \par Function Name:
 *      list_remove_index
 *
 * \brief Remove from the list the element in position according to list_length
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  list			The list
 * \param  index 		The index
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void list_remove_index(List* list, int index);

/**
 * \par Function Name:
 *      list_remove_first
 *
 * \brief Removes fromt he list the first element
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  list			The list
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void list_remove_first(List* list);

/**
 * \par Function Name:
 *      list_remove_last
 *
 * \brief Removes from the list the last element
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  list			The list
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void list_remove_last(List* list);

/**
 * \par Function Name:
 *      list_remove_data
 *
 * \brief Removes from the list the element equals to the passed element. It will compare using the compare function pointer
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return bool
 *
 * \retval True if removed, false if not
 *
 * \param  list					The list
 * \param  data_to_search		The data to search
 * \param  data_to_search_size	The data to search size
 * \param  compare				The function used to compare elements. If compare is NULL it will use default compare. The default compare returns 0 if the data size are equals and if the data are the same. You compare function have to follow this scheme: compare(a,b) --> Negative:a<b, Zero: a=b, Positive:a>b
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
bool list_remove_data(List* list, void* data_to_search, size_t data_to_search_size, int (*compare)(void*,size_t,void*,size_t));

/**
 * \par Function Name:
 *      list_remove_if
 *
 * \brief Removes from the list the elements which the function isToRemove is true is obteined by compare param.
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return bool
 *
 * \retval TRUE if removed at least one element, FALSE if not.
 *
 * \param  list			The list
 * \param  isToRemove	The function tells if is to remove the element
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
bool list_remove_if(List* list, bool (*isToRemove)(void*,size_t));

/**
 * \par Function Name:
 *      list_for_each
 *
 * \brief Executes the function (param function) for each list element.
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  list			The list
 * \param  function		The function which is executed on every element
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void list_for_each(List list, void (*function)(void*,size_t));
void list_for_each_fparam(List list, void (*function)(void*,size_t, void*), void* functionParam);
/**
 * \par Function Name:
 *      list_find
 *
 * \brief Returns the index of the element passed. The way it will compare the elements is obteined by compare param.
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return int
 *
 * \retval	The index of the found element
 *
 * \param  list					The list
 * \param  data_to_search		The data to search
 * \param  data_to_search_size	The data to search size
 * \param  compare				The function used to compare elements. If compare is NULL it will use default compare. The default compare returns 0 if the data size are equals and if the data are the same. You compare function have to follow this scheme: compare(a,b) --> Negative:a<b, Zero: a=b, Positive:a>b
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
int list_find(List list, void* data_to_search, size_t data_to_search_size, int (*compare)(void*,size_t,void*,size_t));

/**
 * \par Function Name:
 *      list_get_pointer_data
 *
 * \brief Returns the pointer to the first data which compare returns 0.
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void*
 *
 * \retval Returns the pointer to the first data which compare returns 0.
 *
 * \param  list					The list
 * \param  data_to_search		The data to search
 * \param  data_to_search_size	The data to search size
 * \param  compare				The function used to compare elements. If compare is NULL it will use default compare. The default compare returns 0 if the data size are equals and if the data are the same. You compare function have to follow this scheme: compare(a,b) --> Negative:a<b, Zero: a=b, Positive:a>b
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void* list_get_pointer_data(List list, void* data_to_search, size_t data_to_search_size, int (*compare)(void*,size_t,void*,size_t));

/**
 * \par Function Name:
 *      list_get_value_data
 *
 * \brief Returns a pointer to a copy to the first data which compare returns 0. You should FREE the resource!
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void*
 *
 * \retval Returns a pointer to a copy to the first data which compare returns 0. You should FREE the resource!
 *
 * \param  list					The list
 * \param  data_to_search		The data to search
 * \param  data_to_search_size	The data to search size
 * \param  compare				The function used to compare elements. If compare is NULL it will use default compare. The default compare returns 0 if the data size are equals and if the data are the same. You compare function have to follow this scheme: compare(a,b) --> Negative:a<b, Zero: a=b, Positive:a>b
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void* list_get_value_data(List list, void* data_to_search, size_t data_to_search_size, int (*compare)(void*,size_t,void*,size_t));


/***************************************************
 *               DEFAULT FUNCTIONS                 *
 ***************************************************/

/**
 * \par Function Name:
 *      list_get_value_data
 *
 * \brief 	If data_size are equals 	--> compare bit per bit according to memcmp
 * 			If not 						--> returns (data1_size - data2_size)
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return int
 *
 * \retval If size are equals returns the result of memcmp of the 2.
 *
 * \param  data1				The data1
 * \param  data1_size			The data1 size
 * \param  data2				The data2
 * \param  data2_size			The data2 size
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
int default_compare(void* data1, size_t data1_size, void* data2, size_t data2_size);

#endif /* DTNPERF_SRC_AL_BP_UTILITY_LIST_H_ */
