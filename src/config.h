/** \file config.h
 *
 * \brief This file contains the defines that can be configured by an expert user
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef SRC_CONFIG_CONFIG_H_
#define SRC_CONFIG_CONFIG_H_

#ifndef BUILD_FOR_ION
/**
 * \brief 0= standard malloc and free; 1=malloc and free from ION reserved memory
 *
 * \hideinitializer
 */
#define BUILD_FOR_ION (0)
#endif

/**
 * \brief Max number of CP, RS, CS CR retransmissions before canceling (CP,RS) or closing (CS,CR) the red session
 *
 * \hideinitializer
 */
#define MAX_RETX_NUMBER (5) //10

/**
 * \brief Number of copies of each original signal segment (to proactively counteract losses)
 *
 * \hideinitializer
 */

#define CP_COPIES (1)
#define RS_COPIES (1)
#define RA_COPIES (1)
#define NN_COPIES (1)
#define PN_COPIES (1)
#define CAS_COPIES (1)
#define CS_COPIES (1)
#define CAS_COPIES (1)
#define CR_COPIES (1)
#define CAR_COPIES (1)
#define GREEN_EOB_COPIES (1)
#define ORANGE_EOB_COPIES (1)

/**
 * \brief 0 = Disabled. 1 = Enabled.<br>
 *
 * If DISTRIBUTED_COPIES=1 CP, RS, CS, CR are retransmitted at RTO/XX_COPIES intervals instead of in a burst
 * and the total number of retx is multiplied by xx_copies
 *
 * \hideinitializer
 */
#define DISTRIBUTED_COPIES (0)


/**
 * \brief Max claim number in a single Report Segment
 *
 * \hideinitializer
 */
#define MAX_NUMBER_OF_CLAIMS_IN_REPORT_SEGMENT (20)

/**
 * \brief Name of JSON config file
 *
 * \hideinitializer
 */
#define CONFIG_FILE_NAME "spans.json"

/**
 * \brief Listen UDP default port
 *
 * \hideinitializer
 */
#define LISTEN_UDP_DEFAULT_PORT "1113"


/**
 * \brief Define how to handle an ongoing session when the link is paused (contact ends).
 *  1 = All active sessions will be deleted
 *  0 = All active sessions will be paused
 *
 * \hideinitializer
 */
#define DELETE_SESSION_WHEN_SPAN_PAUSED (0)


/**
 * \brief Max time between two green or orange segments (after that time the session will expire)
 *
 * \hideinitializer
 */
#define MAX_INTERSEGMENT_TIME (3*1000 ) 	// [ms] //REIMPOSTA A 1000

/**
 * \brief Max duration of an RX red session (ms); after this time a cancellation procedure CS will start
 *
 * \hideinitializer
 */
#define MAX_RX_RED_SESSION_TIME (24*60*60*1000) 	//[ms]

/**
 * \brief Max duration of a TX red session (ms); after this time a cancellation procedure CR will start
 *
 * \hideinitializer
 */
#define MAX_TX_RED_SESSION_TIME (24*60*60*1000)		//[ms]

/**
 * \brief Recently closed session list purging interval (s) (guard time before possible reuse of terminatedSessionIDs)
 *
 * \hideinitializer
 */
#define RECENTLY_CLOSED_LIST_UPDATE_TIME (1000)//1 [s]

/**
 * \brief Token bucket update time (ms)
 *
 * \hideinitializer
 */
#define TOKEN_BUCKET_UPDATE_TIME (50)//[ms]


/**
 * \brief Nominal interval between timer cycles [micros]
 *
 * \hideinitializer
 */
#define TIMER_TICK (100000*1)//(micros) CCaini it must be lower than timer interval (500ms is enough if RTO is dynamic, otherwise <=RTO_FIXED


/**
 * \brief Seconds before trying to update contact plan
 *
 * \hideinitializer
 */
#define CONTACT_PLAN_UPDATE_TIME (6000) //60 [s]


#endif /* SRC_CONFIG_CONFIG_H_ */
