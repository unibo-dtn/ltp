/** \file generic.h
 *
 * \brief This file contains the implementation of some utility functions
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include <stdio.h>
#include <string.h>

#include "generic.h"
#include "../logger/logger.h"
#include "../config.h"


#if (BUILD_FOR_ION)
#include "ion.h"
/**
 * \brief Dynamic memory allocation (use this as malloc()).
 *
 * \hideinitializer
 */
#define MWITHDRAW(size) allocFromIonMemory(__FILE__, __LINE__, size)
/**
 * \brief Dynamic memory deallocation (use this as free()).
 *
 * \hideinitializer
 */
#define MDEPOSIT(addr) releaseToIonMemory(__FILE__, __LINE__, addr)
#else
#define MWITHDRAW(size) malloc(size)
#define MDEPOSIT(addr) free(addr)
#endif

static unsigned long numberOfMalloc = 0;
static unsigned long numberOfFree = 0;

void* mallocWrapper(size_t size) {

	void* result = MWITHDRAW(size);

	if (result == NULL) {
		fprintf(stderr, "Malloc error, NULL returned\n");
		exit(1);
	}

	numberOfMalloc++;

	return result;
}

/*
 * Used by the FREE macro to set NULL after the free.
*/
void freeWrapper(void* pointer) {
	if ( pointer==NULL ) return;
	MDEPOSIT(pointer);
	numberOfFree++;
}


void printMallocFreeStat() {
	doLog("Number of malloc=%lu\t\tNumber of free=%lu\t\tDiff=%d\n", numberOfMalloc, numberOfFree, numberOfMalloc-numberOfFree);
}

char** str_split(char* a_str, const char a_delim, int* count) {
	char** result    = 0;
	    char* tmp        = a_str;
	    char* last_comma = 0;
	    char delim[2];
	    delim[0] = a_delim;
	    delim[1] = 0;
	    *count=0;

	    // Count how many elements will be extracted.
	    while (*tmp)
	    {
	        if (a_delim == *tmp)
	        {
	            (*count)++;
	            last_comma = tmp;
	        }
	        tmp++;
	    }


	    // Add space for trailing token.
	    *count += last_comma < (a_str + strlen(a_str) - 1);


	    result = mallocWrapper(sizeof(char*) * *count);


	    if (result) {

	        size_t idx  = 0;
	        char* token = strtok(a_str, delim);

	        while (token) {
	            *(result + idx) = mallocWrapper((sizeof(char) * (strlen(token)+1)));
	            memcpy(*(result + idx),token,(sizeof(char) * (strlen(token)+1)));

	            token = strtok(0, delim);
	            idx++;
	        }

	    }

	    return result;
}

static size_t compute_length_before_delim(char* str, const char delim) {
    char* first_char = str;
    while (*str != delim && *str != '\0') {
        str++;
    }
    return str - first_char;
}

void extract_hostname_and_port(char* str, char** hostname_str, size_t* hostname_length, char** port_str, size_t* port_length) {
    *hostname_length = compute_length_before_delim(str, ' ');
    *hostname_str = str;
    if (*(*hostname_str + *hostname_length) == '\0') {
        *port_str = "";
        *port_length = 0;
        return;
    }
    *port_str = *hostname_str + *hostname_length + 1;
    *port_length = compute_length_before_delim(*port_str, ' ');
}
