/** \file generic.h
 *
 * \brief This file contains the defines of some utility functions
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef SRC_GENERIC_GENERIC_H_
#define SRC_GENERIC_GENERIC_H_

#include <stdlib.h>


/**
 * \brief Gets the min of 2 numbers
 *
 * \hideinitializer
 */
#define min(a,b) (((a)<(b))?(a):(b))

/**
 * \brief Gets the max of 2 numbers
 *
 * \hideinitializer
 */
#define max(a,b) (((a)>(b))?(a):(b))

/**
 * \par Function Name:
 *      _malloc
 *
 * \brief  Same as malloc. It adds a check if memory allocated != NULL
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void*
 *
 * \retval  The allocated area or NULL in case of errors
 *
 * \param  size		The amount of memory to allocate
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void* mallocWrapper(size_t size);

/**
 * \par Function Name:
 *      _free
 *
 * \brief  Frees the memory. Same as free
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  pointer		The pointer
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void freeWrapper(void* pointer);

/**
 * \par Function Name:
 *      printMallocFreeStat
 *
 * \brief  Prints the stat about the number of mallocs and frees
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void printMallocFreeStat();

/**
 * \par Function Name:
 *      str_split
 *
 * \brief  Splits a string in a NULL terminated char* array
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return char**
 *
 * \retval  The new allocated NULL terminated array of string (char*) & the count (size)
 *
 * \param  a_str 			The string to split
 * \param  a_delim			The delimiter
 * \param  count			(OUT) The size of the NULL terminated array
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
char** str_split(char* a_str, const char a_delim, int* count);

void extract_hostname_and_port(char* str, char** hostname_str, size_t* hostname_length, char** port_str, size_t* port_length);

/**
 * \par Function Name:
 *      getMyNodeNumber
 *
 * \brief  Gets my node number
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return unsigned long long
 *
 * \retval  My node number
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
unsigned long long 	getMyNodeNumber();

#define FREE(ptr) do{freeWrapper(ptr);ptr=NULL;}while (0);//To avoid possible problems (Bisacchi docet)

#endif /* SRC_GENERIC_GENERIC_H_ */
