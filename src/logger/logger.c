/** \file logger.c
 *
 * \brief This file contains the implementations of the log functions
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include "logger.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "../LTPsegment/segment.h"
#include "../timer/timer.h"


/***** PRIVATE FUNCTIONS *****/
static void _logger_write(FILE* fp, const char *string, va_list argList);
/*end*/

static bool loggerEnabled = false;
static bool fileLoggerEnabled = false;
//Log on file added by CCaini
static FILE *fp;

void doLogFile(LTPSegment* segment) {
	if (fileLoggerEnabled) {
		long now=nowInMillis();

		switch (segment->header.typeFlag) {
		case LTP_Red_Data :
			fprintf(fp,"Time: %ld; SessOrig: %lld; Sess#: %d; Type: %u; RedData; Offset: %u; Length: %u \n", now, segment->header.sessionID.sessionOriginator,
					segment->header.sessionID.sessionNumber, segment->header.typeFlag, segment->dataSegment.offset, segment->dataSegment.length);
			fflush(fp);
			break;
		case LTP_Red_Data_CP :
		case LTP_Red_Data_CP_EORP :
		case LTP_Red_Data_CP_EORP_EOB :
			fprintf(fp,"Time: %ld; SessOrig: %lld; Sess#: %d; Type: %d; Ceckpoint; Offset: %u; Length: %u; CP: %u; RS: %u \n",
					now,segment->header.sessionID.sessionOriginator, segment->header.sessionID.sessionNumber, segment->header.typeFlag,
					segment->dataSegment.offset, segment->dataSegment.length, segment->dataSegment.checkpointSerialNumber, segment->dataSegment.reportSerialNumber);
			break;
		case LTP_Green_Data :
		case LTP_Orange_Data :
		case LTP_Orange_Data_EOB :
		case LTP_Green_Data_EOB :
			fprintf(fp,"Time: %ld; SessOrig: %lld; Sess#: %d; Type: %d; GreenOrangeData; Offset: %u; Length: %u \n", now, segment->header.sessionID.sessionOriginator,
					segment->header.sessionID.sessionNumber, segment->header.typeFlag, segment->dataSegment.offset, segment->dataSegment.length);
			break;
		case LTP_RS :
			fprintf(fp,"Time: %ld; SessOrig: %lld; Sess#: %d; Type: %d; RepSeg; :; :; CP: %u; RS: %u \n", now,
					segment->header.sessionID.sessionOriginator, segment->header.sessionID.sessionNumber, segment->header.typeFlag,
					segment->reportSegment.checkpointSerialNumber, segment->reportSegment.reportSerialNumber);
			break;
		case LTP_RA :
			fprintf(fp,"Time: %ld; SessOrig: %lld; Sess#: %d; Type: %d; RepAck; :; :; :; RS: %u \n", now, segment->header.sessionID.sessionOriginator, segment->header.sessionID.sessionNumber, segment->header.typeFlag, segment->reportAckSegment.reportSerialNumber);
			break;
		case LTP_Orange_PN :
			fprintf(fp,"Time: %ld; SessOrig: %lld; Sess#: %d; Type: %d; Orange_PN \n", now, segment->header.sessionID.sessionOriginator, segment->header.sessionID.sessionNumber, segment->header.typeFlag);
			break;
		case LTP_Orange_NN :
			fprintf(fp,"Time: %ld; SessOrig: %lld; Sess#: %d; Type: %d; Orange_NN \n", now, segment->header.sessionID.sessionOriginator, segment->header.sessionID.sessionNumber, segment->header.typeFlag);
			break;
		case LTP_CS :
			fprintf(fp,"Time: %ld; SessOrig: %lld; Sess#: %d; Type: %d; CS \n", now, segment->header.sessionID.sessionOriginator, segment->header.sessionID.sessionNumber, segment->header.typeFlag);
			break;
		case LTP_CAS :
			fprintf(fp,"Time: %ld; SessOrig: %lld; Sess#: %d; Type: %d; CAS \n", now, segment->header.sessionID.sessionOriginator, segment->header.sessionID.sessionNumber, segment->header.typeFlag);
			break;
		case LTP_CR :
			fprintf(fp,"Time: %ld; SessOrig: %lld; Sess#: %d; Type: %d; CR \n", now, segment->header.sessionID.sessionOriginator, segment->header.sessionID.sessionNumber, segment->header.typeFlag);
			break;
		case LTP_CAR :
			fprintf(fp,"Time: %ld; SessOrig: %lld; Sess#: %d; Type: %d; CAR \n", now, segment->header.sessionID.sessionOriginator, segment->header.sessionID.sessionNumber, segment->header.typeFlag);
			break;
		default : // When value of expression didn't match with any case
			fprintf (fp, "Time: %ld; Error: Wrong segment type \n", now);
			destroySegment(segment);
			break;
		}
	}
}

void enableFileLogger() {
	fileLoggerEnabled = true;
	if((fp=fopen("Unibo-LTP.csv", "wt"))==NULL) {
		printf("Error in file opening");
	exit(1);
	}
	fprintf(fp,":Time (ms); :SessOrig; :Sess#; :Type#; Type; :Data Offset; :Data Length; :CPserial#; :RSserial#; \n");
}


void disableFileLogger() {
	if (fileLoggerEnabled==true){
		fileLoggerEnabled = false;
		fclose(fp);
	}
}

void flushFileLogger() {
	if (fileLoggerEnabled==true){
		fileLoggerEnabled = false;
		fflush(fp);
	}

}

void enableLogger() {
	loggerEnabled = true;
}

void disableLogger() {
	loggerEnabled = false;
}



void doLog(const char *string, ...) {
	if ( loggerEnabled ) {
		va_list lp;
		va_start(lp, string);
		_logger_write(stdout, string, lp);
		va_end(lp);
	}
}

/***** PRIVATE FUNCTIONS *****/

static void _logger_write(FILE* fp, const char *string, va_list argList) {
	if (fp == NULL) {
		return;
	}

	vfprintf(fp, string, argList);
	fprintf(fp, "\n");
	fflush(fp);
}


