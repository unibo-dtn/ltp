/** \file timer.c
 *
 * \brief This file contains the Timer implementation
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 * \authors Davide Filoni, davide.filoni2@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include "timer.h"

#include <pthread.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <stddef.h>


#include "../list/list.h"
#include "../generic/generic.h"
#include "../config.h"
#include "../receiver/receiver.h"//CCaini added to pass the allThreadMutex


static int _findTimerID(void* _timerID, size_t _timerIDSize, void* _timer, size_t _timerSize);
static Timer* _getTimerFromTimerID(TimerID timerID);
static void* _timerController(void *_);
static void _handlerTimer(void* _timer, size_t size);
static bool _isStoppedTimer(void* _timer, size_t size);


static pthread_t timerThread;

static bool isInitialized = false;
static bool isTimerMainRunning = false;


static pthread_mutex_t lockAllTimers;
static List allTimers = empty_list;
static TimerID nextTimerID = 1;
static long timeElapsed=0;
static long now_global;



TimerID startTimer (int interval, int cycles, TimerHandler handler, void* data) {

	struct timeval tv;
	struct timezone tz;
	long now_ms;

	gettimeofday(&tv, &tz);
	now_ms = tv.tv_sec*1000+tv.tv_usec/1000;

	Timer timer;
	timer.timerID = nextTimerID;
	nextTimerID++; if (nextTimerID==0) nextTimerID++; // To avoid overflow

	if (cycles>0)
		timer.type=NORMAL;
	else if (cycles==0)
		timer.type=PERIODIC;

	timer.state=RUNNING;
	timer.interval = interval;
	timer.deadline=now_ms+interval;
	timer.cycles=cycles;
	timer.residualCycles=cycles;
	timer.handler = handler;
	timer.data = data;

	pthread_mutex_lock(&lockAllTimers);
	list_push_front(&allTimers, &timer, sizeof(Timer));
	pthread_mutex_unlock(&lockAllTimers);

	return timer.timerID;
}

TimerID startTimerSpecificFor_CP_RS_CS_CR (int interval, int cycles, TimerHandler handler, void* data, void* data2) {

	struct timeval tv;
	struct timezone tz;
	long now_ms;

	gettimeofday(&tv, &tz);
	now_ms = tv.tv_sec*1000+tv.tv_usec/1000;

	Timer timer;
	timer.timerID = nextTimerID;
	nextTimerID++; if (nextTimerID==0) nextTimerID++; // To avoid overflow
	timer.type=specificFor_CP_RS_CS_CR;
	timer.state=RUNNING;
	timer.interval = interval;
	timer.deadline=now_ms+interval;
	timer.cycles=cycles;
	timer.residualCycles=cycles;
	timer.handler = handler;
	timer.data = data;
	timer.data2 = data2;

	pthread_mutex_lock(&lockAllTimers);
	list_push_front(&allTimers, &timer, sizeof(Timer));
	pthread_mutex_unlock(&lockAllTimers);

	return timer.timerID;
}

//CCaini: o il controllo se timerID non serve, oppure serve per tutte le funzioni
bool stopTimer(TimerID timerID) {
	bool ST=false;
	if ( timerID != 0 ) {
		Timer* timer = _getTimerFromTimerID(timerID);
		if ( timer != NULL && timer->state != STOPPED) {
			timer->state = STOPPED;
			if (timer->type==specificFor_CP_RS_CS_CR){
				FREE(timer->data);
				FREE(timer->data2);
			}
//			printf("timer %d stopped",timerID);
			ST=true;
		}
	}
return ST;
}

void pauseTimer(TimerID timerID) {
	struct timeval tv;
	struct timezone tz;
	long now_ms;
	Timer* timer;

	timer = _getTimerFromTimerID(timerID);
	if ( timer != NULL  )
		gettimeofday(&tv, &tz);
		now_ms = tv.tv_sec*1000+tv.tv_usec/1000;
		timer->state = PAUSED;
		timer->pauseStartTime = now_ms;
}

void resumeTimer(TimerID timerID) {
	long now;
	Timer* timer;
	int time_elapsed_in_pause;

	timer= _getTimerFromTimerID(timerID);
	if ( timer != NULL )
		now = nowInMillis();
		time_elapsed_in_pause = now - timer->pauseStartTime;
		timer->deadline=timer->deadline+time_elapsed_in_pause;
		timer->state= RUNNING;
}


void restartTimer (TimerID timerID) {
	long now;
	Timer* timer;

	timer = _getTimerFromTimerID(timerID);
	if (timer != NULL ){
		now = nowInMillis();
		timer->deadline = now + timer->interval;
		timer->state = RUNNING;
	}
}

void initTimer(){

	pthread_mutexattr_t ma;
	pthread_mutexattr_init(&ma);
	pthread_mutexattr_settype(&ma, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&lockAllTimers, &ma);

	isInitialized = true;
	isTimerMainRunning = true;

	pthread_attr_t pthreadAttribute;
	pthread_attr_init(&pthreadAttribute);
	pthread_attr_setdetachstate(&pthreadAttribute, PTHREAD_CREATE_DETACHED);
	pthread_create(&timerThread, &pthreadAttribute, _timerController, NULL);

}


void destroyTimer(){

	isTimerMainRunning = false;
	pthread_mutex_lock(&lockAllTimers);
	list_destroy(&allTimers);
	pthread_cancel(timerThread);
	pthread_mutex_unlock(&lockAllTimers);
	pthread_mutex_destroy(&lockAllTimers);

}

long int nowInMillis (){
	struct timeval tv;
	struct timezone tz;
	long now;
	gettimeofday(&tv, &tz);
	now = tv.tv_sec*1000+tv.tv_usec/1000;
	return now;
}

/***** PRIVATE FUNCTIONS *****/


static Timer* _getTimerFromTimerID(TimerID timerID) {

	pthread_mutex_lock(&lockAllTimers);
	Timer* result = (Timer*) list_get_pointer_data(allTimers, &timerID, sizeof(timerID), _findTimerID);
	pthread_mutex_unlock(&lockAllTimers);
	return result;
}

static int _findTimerID(void* _timerID, size_t _timerIDSize, void* _timer, size_t _timerSize) {
	TimerID* timerID = (TimerID*) _timerID;
	Timer* timer = (Timer*) _timer;
	if ( timer->timerID == *timerID )
		return 0;
	else
		return 1;
}

static void* _timerController(void *_) {

		long last_update;

		while(isTimerMainRunning){
			last_update = nowInMillis();
			usleep(TIMER_TICK);
//			printf("lockThread bloccato da timer \n");
//			fflush (stdout);
			pthread_mutex_lock(&lockThreads);//added by CCaini
//			printf("lockThread sbloccato da timer \n");
//			fflush (stdout);
			if(allTimers != empty_list){
				pthread_mutex_lock(&lockAllTimers);
				now_global=nowInMillis();//CCaini Rimedio possibilmente temporaneo; per evitare di ripetere
				timeElapsed=now_global-last_update;//time elapsed global
				list_for_each(allTimers,_handlerTimer);
				list_remove_if(&allTimers,_isStoppedTimer);
				pthread_mutex_unlock(&lockAllTimers);
			}
			pthread_mutex_unlock(&lockThreads);//added by CCaini
		}

		return NULL;
}



static void _handlerTimer(void* _timer, size_t size){
	Timer *timer = (Timer*) _timer;
	TimerHandler handler;

	if (timer->state==RUNNING){
		if(timer->deadline <=now_global){ // timer expired
			handler = timer->handler;
			switch (timer->type){
			case NORMAL:
				timer->residualCycles-=1;
				if (timer->residualCycles>0){
					timer->deadline+=timer->interval;//This way errors do not cumulate
				}
				else {
					timer->state=STOPPED;
				}
				handler(timer->timerID, timer->data); // Go for handle
				break;
			case PERIODIC:
				timer->deadline+=timer->interval;//This way errors do not cumulate
				handler(timer->timerID, timer->data); // Go for handle
				break;
			case specificFor_CP_RS_CS_CR:
				timer->residualCycles-=1;
//				printf("timer %d expired at %li; residual cycles %d \n", timer->timerID, now_global, timer->residualCycles);
				if (timer->residualCycles>0){
					timer->deadline+=timer->interval;//This way errors do not cumulate
					handler(timer->timerID, timer->data); // //The handler inserts a copy of the data in the list;
					//data cannot be deallocated outside of the timer because they are reused in next cycles until the timer stops
				}
				else {
					//It uses the alternate data2 instead of data
					handler(timer->timerID, timer->data2); // Go for handle
					FREE(timer->data);//The handler inserts a copy in the list
					FREE(timer->data2);
					timer->state=STOPPED;
				}
				break;
			}
		}
//		else {//timer did not expire, do nothing added a print only for debug
//			printf("timer n %u ms rimanenti %ld \n",timer->timerID,
//					timer->deadline -now_global);
//		}
	}
	return;
}


static bool _isStoppedTimer(void* _timer, size_t size){
	Timer *timer =(Timer*) _timer;
	if(timer->state==STOPPED) return true;
	return false;
}

