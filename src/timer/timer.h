
/** \file timer.h
 *
 * \brief This file contains the headers of  the Timer functions
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 * \authors Davide Filoni, davide.filoni2@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef SRC_TIMER_TIMER_H_
#define SRC_TIMER_TIMER_H_

#include "timerStruct.h"

#include <stdbool.h>



/**
 * \par Function Name:
 *      startTimer
 *
 * \brief  Start a new timer
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return TimerID
 *
 * \retval The Timer ID
 *
 * \param  interval 		    The time after which the handler is called
 * \param  handler				The handler
 * \param  data					The data to be passed to the handler
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *  17/12/22 | C. Caini		   |  Added deadline
 *****************************************************************************/
TimerID 	startTimer		(int interval, int cycles, TimerHandler handler, void* data);
TimerID startTimerSpecificFor_CP_RS_CS_CR (int interval, int cycles, TimerHandler handler, void* data, void* data2);

/**
 * \par Function Name:
 *      stopTimer
 *
 * \brief  It stops the timer
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return bool
 *
 * \retval True if the timer is found (and stopped), false otherwise
 *
 * \param  timerID		The ID of the timer to stop
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
bool 	stopTimer		(TimerID timerID);

/**
 * \par Function Name:
 *      pauseTimer
 *
 * \brief It pauses the timer
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  timerID 		The ID of the timer to pause
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 		pauseTimer		(TimerID timerID);

/**
 * \par Function Name:
 *      resumeTimer
 *
 * \brief It resumes the timer
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  timerID		The ID of the timer to resume
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 		resumeTimer		(TimerID timerID);

/**
 * \par Function Name:
 *      restartTimer
 *
 * \brief It restarts the timer
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  timerID		The ID of the timer to restart
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 		restartTimer		(TimerID timerID);


/**
 * \par Function Name:
 *      initTimer
 *
 * \brief It starts the timer thread
 *
 *
 * \par Date Written:
 *      31/03/21
 *
 * \return void
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  31/03/21 | D. Filoni       |  Initial Implementation and documentation.
 *****************************************************************************/
void		initTimer();

/**
 * \par Function Name:
 *      initTimer
 *
 * \brief It stops the timer thread
 *
 *
 * \par Date Written:
 *      31/03/21
 *
 * \return void
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  31/03/21 | D.Filoni        |  Initial Implementation and documentation.
 *****************************************************************************/
void		destroyTimer();

long int nowInMillis ();

#endif /* SRC_TIMER_TIMER_H_ */
