/** \file spanFile.c
 *
 * \brief This file contains the implementation of the function used to load the "spans" from the configuration file (JSON format)
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include "../config.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <json-c/json.h>

#include "../adapters/protocol/lowerProtocol.h"
#include "../LTPspan/span.h"
#include "../LTPsession/session.h"
#include "spanFile.h"
#include "../generic/generic.h"

/* used locally */
#ifndef LOCALLYUSED
#define LOCALLYUSED
	int	parseSocketSpec2(char *ip, char * port, destination_struct *addr);
	bool getInternetAddress(char *hostname, destination_struct *addr);
#endif

/**
 * \brief parse the object and checks for errors
 */
#define json_parse_object_with_check(json, string, result) 	{ 	\
														json_object_object_get_ex(json, string, result);	\
														if ( result == NULL ) { \
															fprintf(stderr, "%s has an error.\n", CONFIG_FILE_NAME); \
															exit(-1); \
														}\
													}

bool loadSpansFromConfigFile() {
	int result;
	FILE *fp = fopen(CONFIG_FILE_NAME, "r");

	if ( fp == NULL ) {
		fprintf(stderr, "Configuration file \"%s\" not found.\n", CONFIG_FILE_NAME);
		exit(-1);
	}

	fseek(fp, 0L, SEEK_END); // Go to the end to discover the file size
	long fileSize = ftell(fp); // Get current position --> file size, i'm at the end of the file
	rewind(fp); // Rewind the file

	char* buffer = (char*) mallocWrapper(fileSize+1);
	if ( buffer == NULL ) {
		fprintf(stderr, "Not enough memory. Ending...\n");
		exit(-1);
	}

	// Read and close file
	result= fread(buffer, fileSize, 1, fp);
	if (result<0) {
		fprintf(stderr, "LoadSpans Error in function fread\n");
		exit(-1);
	}
	
	fclose(fp);

	// Set the end of the string properly
	buffer[fileSize] = '\0';

	struct json_object *parsed_json = json_tokener_parse(buffer);
	free(buffer);

	if ( parsed_json == NULL ) {
		fprintf(stderr, "%s has an error.\n", CONFIG_FILE_NAME);
		exit(-1);
	}

	struct json_object *ownqtime = NULL;
	json_parse_object_with_check(parsed_json, "ownqtime", &ownqtime);
	setOwnqTime(json_object_get_int(ownqtime));

	struct json_object *lowerProtocols = NULL;
	json_parse_object_with_check(parsed_json, "inLowerProtocols", &lowerProtocols);

	size_t n_lowerProtocols = json_object_array_length(lowerProtocols);
	bool atLeastOneLowerLevelInitialized = false;

	for (size_t i = 0; i < n_lowerProtocols; i++) {
		struct json_object *lowerProtocol = NULL;
		lowerProtocol = json_object_array_get_idx(lowerProtocols, i);

		struct json_object *lowerProtocolName = NULL;
		json_parse_object_with_check(lowerProtocol, "inLowerProtocol", &lowerProtocolName);
		struct json_object *lowerProtocolString = NULL;
		json_parse_object_with_check(lowerProtocol, "inLowerProtocolString", &lowerProtocolString);


		const char* lowerProtocolNameString = json_object_get_string(lowerProtocolName);
		const char* lowerProtocolStringStr = json_object_get_string(lowerProtocolString);

		if ( !initLowerLevelProtocol(lowerProtocolNameString, (char*) lowerProtocolStringStr) ) {
			fprintf(stderr, "Warning, not found adapter for protocol %s\n", lowerProtocolNameString);
		} else {
			atLeastOneLowerLevelInitialized = true;
		}
	}

	if ( !atLeastOneLowerLevelInitialized ) {
		fprintf(stderr, "Error, no adapter initialized for lower level\n");
		return false;
	}

	struct json_object *spans = NULL;
	json_parse_object_with_check(parsed_json, "spans", &spans);

	size_t n_spans = json_object_array_length(spans);

	for(size_t i = 0; i < n_spans; i++) {
		struct json_object *span = NULL;
		span = json_object_array_get_idx(spans, i);
		if ( span == NULL ) {
			fprintf(stderr, "%s has an error on span %ld.\n", CONFIG_FILE_NAME, i+1);
			exit(-1);
		}

		struct json_object *peerEngineNumber = NULL;
		struct json_object *maxExportSessions = NULL;
		struct json_object *maxImportSessions = NULL;
		struct json_object *maxPayloadSize = NULL;
		//struct json_object *destination = NULL;
		//struct json_object *port = NULL;
		struct json_object *remoteProcessingDelay = NULL;
		struct json_object *burstSize = NULL;
		struct json_object *color = NULL;
		struct json_object *lowerProtocol = NULL;
		struct json_object *lowerProtocolInit = NULL;

		json_parse_object_with_check(span, "peerEngineNumber", &peerEngineNumber);
		json_parse_object_with_check(span, "maxExportSessions", &maxExportSessions);
		json_parse_object_with_check(span, "maxImportSessions", &maxImportSessions);
		json_parse_object_with_check(span, "maxSegmentPayloadSize", &maxPayloadSize);
		//json_parse_object_with_check(span, "destinationIP", &destination);
		//json_parse_object_with_check(span, "destinationPort", &port);
		json_parse_object_with_check(span, "remoteProcessingDelay", &remoteProcessingDelay);
		json_parse_object_with_check(span, "TB_burstSize", &burstSize);
		json_parse_object_with_check(span, "color", &color);
		json_parse_object_with_check(span, "outLowerProtocol", &lowerProtocol);

		/* Added by ALB
		 * we need to select the right outLowerProtoclString (determined by outLowerProtocol param) */
		const char * lowerProtocol_string = json_object_get_string(lowerProtocol);
		char * realOutLowerProtocolStringParam = (char *) malloc(sizeof(char) * (strlen(lowerProtocol_string) + strlen("outLowerProtocolString") + 1));
		strcpy(realOutLowerProtocolStringParam, lowerProtocol_string);
		strcat(realOutLowerProtocolStringParam, "outLowerProtocolString");
		realOutLowerProtocolStringParam[strlen(lowerProtocol_string) + strlen("outLowerProtocolString")] = '\0';
		json_parse_object_with_check(span, realOutLowerProtocolStringParam, &lowerProtocolInit);

		if ( peerEngineNumber == NULL || maxExportSessions == NULL || maxImportSessions == NULL || maxPayloadSize == NULL || remoteProcessingDelay == NULL || burstSize == NULL || color == NULL ) {
			fprintf(stderr, "%s has an error on span %ld.\n", CONFIG_FILE_NAME, i+1);
			exit(-1);
		}

		unsigned long long nodeNumber = (unsigned long long) json_object_get_int(peerEngineNumber);
		unsigned int maxTxConcurrentSessions = (unsigned int) json_object_get_int(maxExportSessions);
		unsigned int maxRxConcurrentSessions = (unsigned int) json_object_get_int(maxImportSessions);
		unsigned int payloadSegmentSize = (unsigned int) json_object_get_int(maxPayloadSize);
		unsigned int remoteDelay = (unsigned int) json_object_get_int(remoteProcessingDelay);
		//unsigned int ipAddress;
		//unsigned short portNumber = (unsigned short) json_object_get_int(port);
		unsigned long int maxBurstSize = (unsigned long int) json_object_get_int(burstSize);
		//const char* destinationString = json_object_get_string(destination);

		const char* lowerProtocolString = json_object_get_string(lowerProtocol);
		const char* lowerProtocolInitString = json_object_get_string(lowerProtocolInit);

		/*struct hostent *he = gethostbyname(destinationString); // TODO: understand why this part is useful
		if (he == NULL) {
			fprintf(stderr, "%s not resolved\n", destinationString);
			exit(-1);
		}
		memcpy(&ipAddress, he->h_addr_list[0], he->h_length);*/

		LTPSessionColor sessionColor;
		const char* colorString = json_object_get_string(color);

		if ( color == NULL || colorString == NULL ) {
			fprintf(stderr, "Session color missing on span %lld\n", nodeNumber);
			exit(-1);
		}

		if ( strcmp(colorString, "red") == 0 ) {
			sessionColor = Red;
		} else if ( strcmp(colorString, "green") == 0 ) {
			sessionColor = Green;
		} else if ( strcmp(colorString, "orange") == 0 ) {
			sessionColor = Orange;
		}

		LTPSpan* LTPspan = addSpan(nodeNumber, maxTxConcurrentSessions, maxRxConcurrentSessions, payloadSegmentSize, remoteDelay, maxBurstSize, sessionColor, lowerProtocolString, lowerProtocolInitString);

		if ( LTPspan == NULL ) {
			fprintf(stderr, "Error on creating span toward %lld\n", nodeNumber);
			return false;
		}

		//TODO: check if this part is right
		/*char* ip = NULL;
		size_t ip_length = 0;
		char* port = NULL;
		size_t port_length = 0;
		extract_hostname_and_port((char *)lowerProtocolInitString, &ip, &ip_length, &port, &port_length);
		char * finalIP = (char *) malloc((ip_length + 1) * sizeof(char));
		for(int i=0; i < ip_length; i++) {
			finalIP[i] = ip[i];
		}
		finalIP[ip_length] = '\0';
		ECLSA_address_struct env;

		if(parseSocketSpec2(finalIP, port, &(env)) < 0) {
			fprintf(stderr, "Error while parsing ip=%s and port=%s\n", ip, port);
			return false;
		}
		LTPspan->address = env;*/

	}

	// Free the resources
	while (json_object_put(parsed_json) != 1);

	return true;
}
