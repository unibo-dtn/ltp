/** \file range.c
 *
 * \brief This file contains the implementations of the functions of a range
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include "range.h"

#include <stdlib.h>

void createRange(Range* range, unsigned long long fromNode, unsigned long long toNode, time_t fromTime, time_t toTime, long unsigned int seconds) {
	if (range == NULL ) return;

	range->fromNode = fromNode;
	range->toNode = toNode;
	range->fromTime = fromTime;
	range->toTime = toTime;
	range->seconds = seconds;
}

int compareRanges(Range *a, Range *b) {
	int result = -1;

	if (a == b) { //same address pointed
		result = 0;
	} else if (a != NULL && b != NULL) {
		if (a->fromNode < b->fromNode) {
			result = -1;
		} else if (a->fromNode > b->fromNode) {
			result = 1;
		} else if (a->toNode < b->toNode) {
			result = -1;
		} else if (a->toNode > b->toNode) {
			result = 1;
		} else if (a->fromTime < b->fromTime) {
			result = -1;
		} else if (a->fromTime > b->fromTime) {
			result = 1;
		} else {
			result = 0;
		}
	}
	return result;
}
