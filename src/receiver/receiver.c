/** \file receiver.c
 *
 * \brief This file contains the implementations of the LTP receiver functions
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/time.h>
#include <unistd.h>

#include "receiver.h"//contine la dichiarazione del mutex lockThreads

#include "rcvHandlers.h"
#include "../logger/logger.h"
#include "../adapters/protocol/lowerProtocol.h"
#include "../LTPspan/span.h"
#include "../generic/generic.h"

static bool isReceiverRunning = false;
pthread_mutex_t lockThreads;

void *mainReceiver(void *_) {
	LTPSegment receivedSegment;

/*	pthread_mutexattr_t ma;
	pthread_mutexattr_init(&ma);
	pthread_mutexattr_settype(&ma, PTHREAD_MUTEX_RECURSIVE);*/
	pthread_mutex_init(&lockThreads, NULL);
	isReceiverRunning = true;
	while (isReceiverRunning) {
		char buffer[LOWER_LEVEL_MAX_PACKET_SIZE];
		int bufferLenght;

		//memset(&(buffer[0]), 0, LOWER_LEVEL_MAX_PACKET_SIZE);

		if ( !getSerializedSegmentFromLowerProtocol(buffer, &bufferLenght) ) {
			continue; // Timeout
		}
		if (deserializeSegment(buffer, bufferLenght, &receivedSegment) <= 0) {
			doLog("Data received, but the LTP segment cannot be parsed.");
			continue;
		}

		pthread_mutex_lock(&lockThreads);
//		printf("blocked by da receiver \n");
//		fflush (stdout);
		handleNewSegmentReceived(&receivedSegment); //receiver thread
		pthread_mutex_unlock(&lockThreads);
//		printf("lockThread unblocked by receiver \n");
//		fflush (stdout);
	}

	doLog("Receiver ending...");
	return NULL;
}


void stopReceiver() {
	isReceiverRunning = false;
	doLog("Stopping receiver");
}
