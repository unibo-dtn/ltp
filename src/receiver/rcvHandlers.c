/*
 * rcvHandlers.c
 *
 *  Created on: 18 dic 2022
 *      Author: root
 */
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include "../logger/logger.h"
#include "rcvHandlers.h"
#include "../LTPspan/span.h"
#include "../timer/timer.h"
#include "../config.h"
#include "../generic/generic.h"
#include "../adapters/protocol/lowerProtocol.h"
#include "../adapters/protocol/upperProtocol.h"
#include "../adapters/protocol/upperProtocolReceivedData.h"
#include "../sender/sendNotif.h"

static void _checkAllDataReceived(LTPSession* session);
static void _handlerForwardingToSpanThreadNewReceivedSignal(LTPSpan* span, LTPSegment* receivedSegment);

void handleNewSegmentReceived(LTPSegment* receivedSegment) {
	int result;
	LTPSpan* span = NULL;

	doLogFile(receivedSegment);
	// Find the span. If the span is not found the segment is dropped
	// For signalling segments the knowledge of the span is necessary to know which span thread
	// the segment must be passed
	// For data segments to contribute to identify the RX_buffer
	if ( isRXSession(*receivedSegment)) { // If the segment is related to a local RX_SESSION
		span = getSpanFromNodeNumber(receivedSegment->header.sessionID.sessionOriginator); // find the span using sessionOriginator
		if ( span == NULL ) { // span NULL -> span not found
			doLog("Info:\tNot found span for %d-%d. Ignoring segment", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
			destroySegment(receivedSegment);
			return;
		}

	} else if ( isTXSession(*receivedSegment) && getMyNodeNumber() == receivedSegment->header.sessionID.sessionOriginator) { // If the segment is related to a local TX_SESSION

		span = (LTPSpan*) list_get_pointer_data(activeSpans, &receivedSegment->header.sessionID, sizeof(receivedSegment->header.sessionID), findSpanFromTXSessionID);

		if ( span == NULL ) { // If span is not found, search in recently terminated sessions.
			span = (LTPSpan*) list_get_pointer_data(activeSpans, &receivedSegment->header.sessionID, sizeof(receivedSegment->header.sessionID), findSpanFromTerminatedTXSessionID);
		}
		if ( span == NULL ) { // span NULL -> span not found
			doLog("Info:\tNot found span for %d-%d. Ignoring segment", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
			destroySegment(receivedSegment);
			return;
		}
	} else {
		doLog("Error:\t Unable to find the span of the received segment; received segment discarded");
		destroySegment(receivedSegment);
		return;
	}
	// Now we know the span

	LTPSession* session;
	bool goStep2=false, goStep3=false;
	LTPSegmentTypeCode segmentType;
	segmentType=receivedSegment->header.typeFlag;
	switch (segmentType) {
	case LTP_Red_Data :
		goStep2=handleNewDataSegmentReceived(span, receivedSegment, &session);
		if (!goStep2) destroySegment(receivedSegment);//CCaini altrimenti chi lo distrugge?
		break;
	case LTP_Red_Data_CP :
	case LTP_Red_Data_CP_EORP :
	case LTP_Red_Data_CP_EORP_EOB :
		goStep2=handleNewDataSegmentReceived(span, receivedSegment, &session);
		if (!goStep2) destroySegment(receivedSegment);//CCaini altrimenti chi lo distrugge?
		if (goStep2) goStep3=receiverHandler_CP(span, receivedSegment, session);
		if (goStep3) _handlerForwardingToSpanThreadNewReceivedSignal(span, receivedSegment);
		break;
	case LTP_Green_Data :
		goStep2=handleNewDataSegmentReceived(span, receivedSegment, &session);
		if (!goStep2) destroySegment(receivedSegment);//CCaini altrimenti chi lo distrugge?
		break;
	case LTP_Orange_Data :
		goStep2=handleNewDataSegmentReceived(span, receivedSegment, &session);
		if (!goStep2) destroySegment(receivedSegment);//CCaini altrimenti chi lo distrugge?
		break;
	case LTP_Orange_Data_EOB :
		goStep2=handleNewDataSegmentReceived(span, receivedSegment, &session);
		if (!goStep2) destroySegment(receivedSegment);//CCaini altrimenti chi lo distrugge?
		if (goStep2) {
			receiverHandler_OrangeEOB(span,receivedSegment, session);
			_handlerForwardingToSpanThreadNewReceivedSignal(span, receivedSegment);
		}
		break;
	case LTP_Green_Data_EOB :
		goStep2=handleNewDataSegmentReceived(span, receivedSegment, &session);
		if (!goStep2) destroySegment(receivedSegment);//CCaini altrimenti chi lo distrugge?
		if (goStep2) receiverHandler_GreenEOB(span,receivedSegment, session);
		break;
	case LTP_RS :
		receiverHandler_RS(span, receivedSegment); //At the end it will pass the RS to the span to send the RA and reTx segments
		break;
	case LTP_RA :
		receiverHandler_RA(span, receivedSegment);
		break;
	case LTP_Orange_PN :
		receiverHandler_OrangePN(span, receivedSegment);
		break;
	case LTP_Orange_NN :
		receiverHandler_OrangeNN(span, receivedSegment);
		break;
	case LTP_CS :
		receiverHandler_CS(span, receivedSegment);//At the end it will pass the CS to the span to send a CAS
		break;
	case LTP_CAS :
		receiverHandler_CAS(span, receivedSegment);
		break;
	case LTP_CR :
		receiverHandler_CR(span, receivedSegment);//At the end it will pass the CR to the span to send a CAR
		break;
	case LTP_CAR :
		receiverHandler_CAR(span, receivedSegment);
		break;
	default : // When value of expression didn't match with any case
		doLog("Error: Wrong segment type");
		destroySegment(receivedSegment);
		break;
	}
}



bool handleNewDataSegmentReceived (LTPSpan* span, LTPSegment* receivedSegment, LTPSession** psession) {
	/*This function is entered for all data segments (the first 8 types in RFC5326. Pure signalling segments (the last 8 types) are excluded
Some of the data segments are "pure", others, with flags, are "mixed" (CPs, EOBs).
return with false means that the segment does not need further processing by the caller (the segment will be immediately destroyed)
return with true the opposite.
span and receivedSegments are in input
session is in output;
	 */
	session_list_state_t list;
	LTPSession* session;
	LTPSessionColor segmentColor;
	//Discover the color of the segment
	if(isAnyKindRedData(*receivedSegment))
		segmentColor= Red;
	else if(isAnyKindOrangeData(*receivedSegment))
		segmentColor= Orange;
	else
		segmentColor=Green;

	list=findRxSession(span, receivedSegment, &session);
	switch (list) {
	case ACTIVE:
		// If the Rx segm. is in the active list but the session has been cancelled or all data have been received, skip it.
		if(session->RxSession.state == CR_SENT || session->RxSession.state == ALL_DATA_RECEIVED || session->RxSession.state == CS_RECEIVED){ // if session state is CR_SENT or ALL_DATA_RECEIVED or CS_RECEIVED-> skip the received segment
			doLog("Session (E%d,#%d):\tIgnoring segment data of a CR_SENT or ALL_DATA_RECEIVED or CS_RECEIVED session", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
			return false;
		}
		//If miscolored, do something and then skip the segment
		if(session->color != segmentColor){
			if(session->color == Red){ // red session -> send CR
				doLog("Session (E%d,#%d):\tReceived segment color different from session color... Send CR (miscolored)", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
				clearRXBuffer(session);
				Notification CR_notif=createNotif(CANCEL_RED_RX_SESSION, session->sessionID, 0, MISCOLORED, span->nodeNumber);
				forwardingNotifToSpanThread(&CR_notif);//Add a COPY of the notif to the list (this way the list element is decoupled from local notif
			}
			else { // green and orange session -> clean and close Session
				doLog("Session (E%d,#%d):\tReceived segment color different from session color... close session", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
				cleanAndCloseSession(session);
			}
			return false;
		}
		if(segmentColor == Green || segmentColor == Orange){
			restartTimer(session->RxSession.timerMaxIntersegment);
		}
		break;
	case RECENTLY_CLOSED:
		doLog("Session (E%d,#%d):\tIgnoring segment data of a recently-closed session", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
		return false;
		break;
	case UNKNOWN :
		//Try to start a new Rx session
		pthread_mutex_lock(&(span->lock)); // Lock the span
		if ( span->currentRxBufferUsed >= span->maxRxConcurrentSessions ) { // Too many Rx session -> discard segment
			fprintf(stderr, "Too many Rx sessions for span %lld\n", span->nodeNumber);
			pthread_mutex_unlock(&(span->lock)); // Unlock the span
			return false;
		}
		span->currentRxBufferUsed++; // Increase the number of concurrent Rx sessions
		LTPSession newSession;
		initializeSession(&newSession, span, receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber, segmentColor, RX_SESSION);
		session = list_append(&(span->activeRxSessions), &newSession, sizeof(LTPSession)); // Add the new session to the list of active sessions

		if(segmentColor == Red){
			// Set red session timer
			Notification* CR_notif;
			CR_notif=(Notification*) mallocWrapper(sizeof(Notification));
			*CR_notif=createNotif(CANCEL_RED_RX_SESSION, session->sessionID, 0, SESSION_TIMEOUT, session->span->nodeNumber);
			session->RxSession.timerCancelRXSession = startTimer(MAX_RX_RED_SESSION_TIME, 1, timerHandlerSessionTimeoutNotif , CR_notif);
			doLog("Session (E%d,#%d):\tCreated (red RX)", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		}
		else if (segmentColor == Orange){
			Notification* NN_notif;
			NN_notif=(Notification*) mallocWrapper(sizeof(Notification));
			*NN_notif=createNotif(CLOSE_ORANGE_RX_SESSION, session->sessionID, 0, SESSION_TIMEOUT, session->span->nodeNumber);
			session->RxSession.timerMaxIntersegment = startTimer(MAX_INTERSEGMENT_TIME, 1, timerHandlerSessionTimeoutNotif, NN_notif);
			doLog("Session (E%d,#%d):\tCreated (orange RX)", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		}
		else if (segmentColor == Green ){
			session->RxSession.timerMaxIntersegment = startTimer(MAX_INTERSEGMENT_TIME, 1, handlerRxSessionTimeout, session);
			doLog("Session (E%d,#%d):\tCreated (green RX)", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		}
		pthread_mutex_unlock(&(span->lock)); // Unlock the span
		break;
	}

	//Now the segment session in necessarily in the list of active Rx sessions; session is its pointer
	//CCaini The full segment is added to a list that works as Rx_buffer, not only the segment payload
	//the Rx buffer is a list of full segments; this list is a field of the session structure
	bool newSegmentAdded= addSegmentToSession(session, receivedSegment); // Try to add the received segment to the session RX-buffer (i.e. the Rx buffer for the session)

	/*CCaini: in case of disordered delivery, in a red data segment flagged as CP, it may happen that data
	 * are the same but CP is new. Thus it is always necessary to go on with processing (i.e. return true)
	 */
	*psession=session;
	if (isCP(*receivedSegment)) //LTP segment types 1-3
		return true; //go on
	else
		return newSegmentAdded; //go on only for segments with non-duplicate data
}


/*
 * receiveHandlers (For all segments types once received), in type order
*/

bool receiverHandler_CP(LTPSpan* span, LTPSegment* receivedSegment, LTPSession* session){//Types 1,2,3, response required
	TimerID timerID;
	int result;

	if ( isEOB(*receivedSegment) ) { // End of block -> save lastByteOfBlock & set EOB arrived
		session->block->blockLength = receivedSegment->dataSegment.offset + receivedSegment->dataSegment.length;
		session->RxSession.isEOBarrived = true;
	}

	if ( list_find(session->RxSession.CPReceived, &(receivedSegment->dataSegment.checkpointSerialNumber), sizeof(receivedSegment->dataSegment.checkpointSerialNumber), NULL /*default compare*/) > 0 ) { // Checkpoint is already received
		doLog("Session (E%d,#%d):\tCP %u duplicated: dropped", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->dataSegment.checkpointSerialNumber);
		destroySegment(receivedSegment);
		return false; // Already received -> skip it
	} else { // otherwise
		// The CP is fresh, add it to the list of CP received
		list_push_front(&(session->RxSession.CPReceived), &(receivedSegment->dataSegment.checkpointSerialNumber), sizeof(receivedSegment->dataSegment.checkpointSerialNumber));
	}


	if ( receivedSegment->dataSegment.reportSerialNumber > 0 ) { // If the CP is in response to an RS
		doLog("Session (E%d,#%d):\tCP %d received in response to RS %d", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->dataSegment.checkpointSerialNumber, receivedSegment->dataSegment.reportSerialNumber);
		//Check if the RS is still in the list of those waiting to be re-Tx, i.e. not confirmed by an RA yet (RA lost or out-of-order) if yes, the index will be >0
		int index = list_find(session->RxSession.RSWaitingForRA, &(receivedSegment->dataSegment.reportSerialNumber), sizeof(receivedSegment->dataSegment.reportSerialNumber), findReportSegmentFromReportSegmentSerialNumber);
		if ( index > 0 ) { // The RS is in the re-tx list: stop its timer, remove the RS from the list->  destroy segments (dealloc memory) -> free the segment (list_remove_index_get_pointer requires to free)
			doLog("Session (E%d,#%d):\tCP %d is the first to confirm RS %d (RA lost or out-of-order); remove RS form the re-tx list", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->dataSegment.checkpointSerialNumber, receivedSegment->dataSegment.reportSerialNumber);
			ResendStruct* RSstruct = (ResendStruct*) list_remove_index_get_pointer(&(session->RxSession.RSWaitingForRA), index, NULL);
			//Stop the associated RTO timer
			while (RSstruct->timerID==0); // wait for timerID is valid Chiedere Bisacchi
			timerID=RSstruct->timerID;
			stopTimer(timerID);
			doLog("Session (E%d,#%d):\t(RS) timer %d stopped\n", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, timerID);
			//remove the RS form the re-tx list
			pthread_mutex_lock(&(session->RTXTimersLock));
			list_remove_data(&(session->RTXTimers), &timerID, sizeof(TimerID), NULL);
			pthread_mutex_unlock(&(session->RTXTimersLock));
			destroyRSWaitingForRa(RSstruct, 0, session); // CCaini a me sembra che elimini il timner eliminato prima
			//Oltre ad altre cose. Chiedee a Bisacchi RSstruct
			FREE(RSstruct); // Free the sendStruct

			//Check if there are associated resendRS notifications waiting to be processed.
			int indexRS = list_find(span->notifToSpanThread, &(receivedSegment), sizeof(receivedSegment), findRSNotifFromCP);
			if(indexRS > 0){ //indexRS is the list index of the element to be removed
				// remove the element from the list, get its pointer, free the corresponding allocated memory
				void* pointer = list_remove_index_get_pointer(&(span->notifToSpanThread), indexRS, NULL);
				doLog("Session (E%d,#%d):\t resendRS notif found: removed", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
				FREE(pointer);
				//decrease the number of notifications waiting to be processed
				uint64_t dummy = -1; // -1 because I have to remove an element
				result=write(span->receivedNotifEventFD, &dummy, sizeof(dummy));
				if (result<0) {
					doLog("Session (E%d,#%d):\t CP received, error in function write", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
					exit (-1);
				}

			} //CCaini NOT treated ask Bisacchi Note that we could have multiple notifications in the notifToSpanThread list
		}//the RS is not in the list of those waiting to be retransmitted as expected (RA arrived before this CP)
	}
	else{ //The CP is not in response to an RS
		doLog("Session (E%d,#%d):\tCP %d received (not triggered by an RS)", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->dataSegment.checkpointSerialNumber);
	}

	_checkAllDataReceived(session); // Check block integrity & try to deliver to upper protocol
	return true;
}


void receiverHandler_OrangeEOB(LTPSpan* span, LTPSegment* receivedSegment, LTPSession* session){//Type 6
/*
	LTPSession* session = list_get_pointer_data(span->activeRxSessions, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), findSessionFromSessionID); // Get the session from the active sessions
	if ( session == NULL ) {	// If the EOB session does not exist -> ignore segment
		destroySegment(receivedSegment);
		return;
	}
*/
	session->block->blockLength = receivedSegment->dataSegment.offset + receivedSegment->dataSegment.length;
	session->RxSession.isEOBarrived = true;

	doLog("Session (E%d,#%d):\tOrange EOB received", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
	//CCaini added stop inter-segment timer
	stopTimer(session->RxSession.timerMaxIntersegment);

	_checkAllDataReceived(session); // Check the integrity of the block: if completed, deliver it to the upper protocol (e.g. BP)
	//the session status is changed by checkAllDataReceived if all data received
	if(session->RxSession.state != ALL_DATA_RECEIVED){ //only for doLog, because by contrast to greenEOB, orangeEOB triggers a notification
		//and the session will be closed only after this sending, by the span thread
		doLog("Session (E%d,#%d):\tBlock uncompleted", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
	}
}

void receiverHandler_GreenEOB(LTPSpan* span, LTPSegment* receivedSegment, LTPSession* session) {//Type 7
/*	LTPSession* session = list_get_pointer_data(span->activeRxSessions, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), findSessionFromSessionID); // Get the session from the active sessions
	if ( session == NULL ) {	// If the EOB session does not exist -> ignore the segment
		destroySegment(receivedSegment);
		return;
	}
*/
	session->block->blockLength = receivedSegment->dataSegment.offset + receivedSegment->dataSegment.length;
	session->RxSession.isEOBarrived = true;

	doLog("Session (E%d,#%d):\tGreen EOB received", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
	//CCaini added stop inter-segment timer
	stopTimer(session->RxSession.timerMaxIntersegment);

	_checkAllDataReceived(session); // Check block integrity & try to deliver the block to the upper protcol (e.g. BP)

	if (session->RxSession.state != ALL_DATA_RECEIVED ) { // If not all data received
		doLog("Session (E%d,#%d):\tGreen block uncompleted; block discarded", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
	}

	cleanAndCloseSession(session);//as green EOB does not trigger any notification, the session can be immediately closed
}

void receiverHandler_RS(LTPSpan* span, LTPSegment* receivedSegment) {//Type 8, response required

	session_list_state_t list;
	LTPSession* session;
	TimerID timerID;

	list=findTxSession(span, receivedSegment, &session);
	switch (list) {
	case ACTIVE :

		//3 initial exceptions
		if ( session->TxSession.state == CR_RECEIVED){ // if the session state is CR_RECEIVED -> ignore it
			doLog("Session (E%d,#%d), warning:\tRS received %d but the session is in CR_RECEIVED state. Dropped.", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->reportSegment.reportSerialNumber);
			destroySegment(receivedSegment);
			return;
		}
		if ( session->TxSession.state == CS_SENT ) {	// If the session state is CS_SENT -> send RA //CCaini non sicuro sia la cosa giusta
			doLog("Session (E%d,#%d), warning:\tRS %d received but the session is in CS_SENT state. RA will be sent.", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->reportSegment.reportSerialNumber);
			_handlerForwardingToSpanThreadNewReceivedSignal(span, receivedSegment);
			return;
		}
		//Check is the RS is a duplicate; if yes do not process it (implementation choice, not to retranmit missing segment twice for the same RS)
		if ( list_find(session->TxSession.RSReceived, &(receivedSegment->reportSegment.reportSerialNumber), sizeof(receivedSegment->reportSegment.reportSerialNumber), NULL /*default compare*/) > 0 ) { // RS is already received
			doLog("Session (E%d,#%d), warning:\tRS %u received but is a duplicate. Dropped as the session is still active", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->reportSegment.reportSerialNumber);
			destroySegment(receivedSegment); //CCaini era commentato: perché ? Avrei anche potuto forse anche mandare un RA
			return;
		}
		//End of initial exceptions for an active session

		doLog("Session (E%d,#%d):\tRS %d received in response to CP %d\tclaimCount = %d\t\tbounds %d-%d",
				session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->reportSegment.reportSerialNumber, receivedSegment->reportSegment.checkpointSerialNumber, receivedSegment->reportSegment.receptionClaimCount, receivedSegment->reportSegment.lowerBound, receivedSegment->reportSegment.upperBound);

		//The RS is fresh, add it to the list of RS received
		list_push_front(&(session->TxSession.RSReceived), &(receivedSegment->reportSegment.reportSerialNumber), sizeof(receivedSegment->reportSegment.reportSerialNumber));

		int result;
		if ( receivedSegment->reportSegment.checkpointSerialNumber > 0 ) {  // If the RS is in response to a CP
//Check if the CP is still in the list of those waiting to be re-Tx, i.e. not confirmed yet by a previous RS triggered by the same CP
			int index = list_find(session->TxSession.CPWaitingForRS, &(receivedSegment->reportSegment.checkpointSerialNumber), sizeof(receivedSegment->reportSegment.checkpointSerialNumber), findCheckpointFromCheckpointNumber);
			if (index > 0) {//the CP is still in the list (this is the expected case, as multiple RSs for the same CP are rare)
				doLog("Session (E%d,#%d):\tRS %d is the first to confirm CP %d; remove CP from the re-tx list", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->reportSegment.reportSerialNumber, receivedSegment->reportSegment.checkpointSerialNumber);
				//Remove the CP form the re-tx list
				ResendStruct* CPstruct = (ResendStruct*) list_remove_index_get_pointer(&(session->TxSession.CPWaitingForRS), index, NULL);
				//Stop the associated timer
				while (CPstruct->timerID==0); // wait for timerID is valid //CCaini perché?
				timerID=CPstruct->timerID;
				stopTimer(timerID);
				//remove the timer from the list of RTXTimers
				pthread_mutex_lock(&(session->RTXTimersLock));
				list_remove_data(&(session->RTXTimers), &timerID, sizeof(TimerID), NULL);
				pthread_mutex_unlock(&(session->RTXTimersLock));
				FREE(CPstruct->segment);
				FREE(CPstruct);

				//Check if there are associated resendCP notifications waiting to be processed.
				int indexCP = list_find(span->notifToSpanThread, &(receivedSegment), sizeof(receivedSegment), findCPNotifFromRS);
				if(indexCP > 0){//indexCP is the list index of the element to be removed
					// remove the element form the list, get its pointer, free the coresponding allocated memory
					void* pointer = list_remove_index_get_pointer(&(span->notifToSpanThread), indexCP, NULL);
					FREE(pointer);
					doLog("Session (E%d,#%d):\t resendCP notif found: removed", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
					//decrease the number of notifications waiting to be processed
					uint64_t dummy = -1; // -1 because I have remove an element
					result=write(span->receivedNotifEventFD, &dummy, sizeof(dummy));
					if (result<0) {
						doLog("Session (E%d,#%d):\t RS %d, error in function write", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->reportSegment.reportSerialNumber);
						exit (-1);
					}
				}//Not considered the case of multiple notifications waiting to be processed
			}//CP removal from CPWaitingForRS
		}
		_handlerForwardingToSpanThreadNewReceivedSignal(span, receivedSegment);	//send the RS to the span thread
		break;
	case RECENTLY_CLOSED :
		session = list_get_pointer_data(span->recentlyClosedTxSessionIDs, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), findSessionFromSessionID); // Get the session from the recently closed sessions
		doLog("Session (E%d,#%d), warning:\tRS %d received but the session is in the recently closed list. RA will be sent.",
				session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->reportSegment.reportSerialNumber);
		_handlerForwardingToSpanThreadNewReceivedSignal(span, receivedSegment);
		break;
	case UNKNOWN :
		doLog("Session (E%d,#%d), warning:\tRS %d received but the session is unknown. Dropped.",
				session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->reportSegment.reportSerialNumber);
		destroySegment(receivedSegment);
		break;
	}
	return;
}

void receiverHandler_RA (LTPSpan* span, LTPSegment* receivedSegment) {//Type 9

	session_list_state_t list;
	LTPSession* session;
	int result;
	bool finalRSConfirmed = false;

	session= list_get_pointer_data(span->activeRxSessions, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), findSessionFromSessionID); // Get the session from the active sessions
	if ( session != NULL ) list=ACTIVE;
	else list=UNKNOWN;//No need to consider the recently closed list as a case a part.

	switch (list) {
	case ACTIVE :
		doLog("Session (E%d,#%d):\tRA received confirming RS %d", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber, receivedSegment->reportAckSegment.reportSerialNumber);
		//Check if the RS is still in the list of those waiting to be re-Tx, i.e. not confirmed by a CP; if yes, the index will be >0
		int index = list_find(session->RxSession.RSWaitingForRA, &(receivedSegment->reportAckSegment.reportSerialNumber), sizeof(receivedSegment->reportAckSegment.reportSerialNumber), findReportSegmentFromReportSegmentSerialNumber);
		if ( index > 0 ) { //the RS is still in the list (expected case)
			doLog("Session (E%d,#%d):\tRS %d confirmed by RA; remove RS from the re-tx list", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->reportAckSegment.reportSerialNumber);
			//Remove the RS from the re-tx list
			ResendStruct* sendStruct = (ResendStruct*) list_remove_index_get_pointer(&(session->RxSession.RSWaitingForRA), index, NULL);
			//Stop the associated timer
			while (sendStruct->timerID==0); // wait for timerID is valid     XXX serve ancora coi nuovi timer?
			stopTimer(sendStruct->timerID);
			doLog("Session (E%d,#%d):\t(RS) timer %d stopped\n", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, sendStruct->timerID);
			//Remove the timer form the RTXTimers list
			pthread_mutex_lock(&(session->RTXTimersLock));
			list_remove_data(&(session->RTXTimers), &sendStruct->timerID, sizeof(sendStruct->timerID), NULL);
			pthread_mutex_unlock(&(session->RTXTimersLock));
			destroySegment(sendStruct->segment); //RAs are not processed by the span thread, by contrast to CPs and RSs.
			FREE(sendStruct->segment);
			FREE(sendStruct); // Free the sendStruct

			//Check if there are associated resendRS notifications waiting to be processed.
			int indexRS = list_find(span->notifToSpanThread, &(receivedSegment), sizeof(receivedSegment), findRSNotifFromRA);
			if(indexRS > 0){//indexRS is the list index of the element to be removed
				void* pointer = list_remove_index_get_pointer(&(span->notifToSpanThread), indexRS, NULL);
				doLog("Session (E%d,#%d):\tfound resendRS notif: removed\n", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
				FREE(pointer);
				//decrease the number of notifications waiting to be processed
				uint64_t dummy = -1; // -1 because I have to remove an element
				result=write(span->receivedNotifEventFD, &dummy, sizeof(dummy));
				if (result<0) {
					doLog("Session (E%d,#%d):\tRA received, error in function write", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
					exit (-1);
				}
			}

		}
		if	(receivedSegment->reportAckSegment.reportSerialNumber==session->RxSession.finalRSSentNumber) finalRSConfirmed = true;
		if ( session->RxSession.state == ALL_DATA_RECEIVED && finalRSConfirmed ) { // Block delivered && no more RS waiting for ack -> session is closed
			doLog("Session (E%d,#%d):\tCompleted", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
			cleanAndCloseSession(session);
		}//CCaini: questo if esterno è molto brutto, si potrebbe mettere dentro ma non sono sicuro delle _free
		break;
	case UNKNOWN :
		// If the segment session is not in the active list -> skip the segment
		doLog("Session (E%d,#%d) warning:\tRA %d received but the session is not in the active list", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber, receivedSegment->reportSegment.reportSerialNumber);
		break;
	}
	destroySegment(receivedSegment);
	return;
}


void receiverHandler_OrangePN(LTPSpan* span, LTPSegment* receivedSegment) { //type 10
	session_list_state_t list;
	LTPSession* session;

	session = list_get_pointer_data(span->activeTxSessions, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), findSessionFromSessionID); // Get the session from the active sessions
	if (session != NULL ) list=ACTIVE;
	else list=UNKNOWN; //There should be no Orange sessions in recently closed Tx session list; anyway, they are irrelevant.

	switch (list) {
	case ACTIVE :
		doLog("Session (E%d,#%d):\tOrange PN received, signaling SUCCESS to upper protocol", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		// Signal the upper protocol that this session is completed correctly (RFC 5326 7.4)
		if ( session->TxSession.handlerSessionTerminated != NULL ) session->TxSession.handlerSessionTerminated(session->TxSession.dataNumber, SessionResultSuccess);
		cleanAndCloseSession(session);
		break;
	case UNKNOWN:
		doLog("Session (E%d,#%d):\tOrange Positive Notification received of an unknown Session", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
		break;
	}
	destroySegment(receivedSegment);
	return;
}

void receiverHandler_OrangeNN(LTPSpan* span, LTPSegment* receivedSegment) { //type 11
	session_list_state_t list;
	LTPSession* session;

	session = list_get_pointer_data(span->activeTxSessions, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), findSessionFromSessionID); // Get the session from the active sessions
	if (session != NULL ) list=ACTIVE;
	else list=UNKNOWN; //There should be no Orange sessions in recently closed Tx session list; anyway, they are irrelevant.

	switch (list) {
	case ACTIVE :
		doLog("Session (E%d,#%d):\tOrange NN received, signaling FAILURE to upper protocol", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		// Signal the upper protocol that this session is completed correctly (RFC 5326 7.4)
		if ( session->TxSession.handlerSessionTerminated != NULL ) session->TxSession.handlerSessionTerminated(session->TxSession.dataNumber, SessionResultError);
		cleanAndCloseSession(session);
		break;
	case UNKNOWN:
		doLog("Session (E%d,#%d):\tOrange Negative Notification received of an unknown Session", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
		break;
	}
	destroySegment(receivedSegment);
	return;
}


void receiverHandler_CS(LTPSpan* span, LTPSegment* receivedSegment) {//type12, response required
	session_list_state_t list;
	LTPSession* session;
	list=findRxSession(span, receivedSegment, &session);
	switch (list) {
	case ACTIVE :
		session->RxSession.state = CS_RECEIVED;
		doLog("Session (E%d,#%d):\tCS received with reason code %d, cleaning the session...", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber, receivedSegment->cancelSegment.cancelCode);
		cleanSession(session);
		_handlerForwardingToSpanThreadNewReceivedSignal(span, receivedSegment); //It will pass the CR to the span thread to send the CAS
		break;
	case RECENTLY_CLOSED :
		doLog("Session (E%d,#%d):\tReceived CS of a recently closed Rx session. CAS will be sent", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
		_handlerForwardingToSpanThreadNewReceivedSignal(span, receivedSegment); //It will pass the CR to the span thread to send the CAS
		break;
	case UNKNOWN :
		doLog("Session (E%d,#%d):\tReceived CS of an unknown Rx session. Segment dropped.", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
		destroySegment(receivedSegment);
		break;
	}
	return;

}


void receiverHandler_CAS(LTPSpan* span, LTPSegment* receivedSegment) {//type 13
	session_list_state_t list;
	LTPSession* session;
	list=findTxSession(span, receivedSegment, &session);
	int result;

	switch (list) {
	case ACTIVE :
		doLog("Session (E%d,#%d):\tCAS received", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		if ( session->TxSession.state == CS_SENT) {
			doLog("Session (E%d,#%d):\tsession was in CS_SENT state, CS confirmed by CAS; remove CS from the re-tx list; signal FAILURE to upper protocol", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
			// stop retrasmission timer
			stopTimer(((ResendStruct*)session->TxSession.CSWaitingForCAS)->timerID);
			pthread_mutex_lock(&(session->RTXTimersLock));
			list_remove_data(&(session->RTXTimers), &(((ResendStruct*)session->TxSession.CSWaitingForCAS)->timerID), sizeof(((ResendStruct*)session->TxSession.CSWaitingForCAS)->timerID), NULL);
			pthread_mutex_unlock(&(session->RTXTimersLock));


//			int indexCS = list_find(span->structsToSendBySpanThread, &(receivedSegment), sizeof(receivedSegment), findAssociateCX);
			int indexCS = list_find(span->notifToSpanThread, &receivedSegment, sizeof(receivedSegment), findCXNotifFromCAX);

			if(indexCS > 0){ //Found CS with indexCS as index in list
				doLog("Session (E%d,#%d):\found resendCS notif: removed", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
				// extract the element pointed from the list, THEN to delete the element you need to free the memory
//				void* pointer = list_remove_index_get_pointer(&(span->structsToSendBySpanThread), indexCS, NULL);
				void* pointer = list_remove_index_get_pointer(&(span->notifToSpanThread), indexCS, NULL);
				FREE(pointer);
				uint64_t dummy = -1; // -1 because I have removed an element
				result=write(span->receivedNotifEventFD, &dummy, sizeof(dummy)); //To tell the span
													//that we have removbed an element from the queue
				if (result<0) {
					doLog("Session (E%d,#%d):\t CAS received, error in function write", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
					exit (-1);
				}
			}
			//Signal failure to upper protocol
			if ( session->TxSession.handlerSessionTerminated != NULL ) session->TxSession.handlerSessionTerminated(session->TxSession.dataNumber, SessionResultError);
			// free the memory
			FREE(((ResendStruct*)session->TxSession.CSWaitingForCAS)->segment);
			FREE(session->TxSession.CSWaitingForCAS);
			closeSession(session);  // Put the session into recently closed list //CCaini io la butterei...
		}
		break;
	case RECENTLY_CLOSED :
	case UNKNOWN :
		doLog("Session (E%d,#%d):\tCAS received of an unknown session", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
		break;
	}
	destroySegment(receivedSegment);
	return;
}

void receiverHandler_CR(LTPSpan* span, LTPSegment* receivedSegment) {//type 14, response required
	session_list_state_t list;
	LTPSession* session;
	list=findTxSession(span, receivedSegment, &session);
	switch (list) {
	case ACTIVE :
		session->TxSession.state = CR_RECEIVED;
		doLog("Session (E%d,#%d):\tCR received for session with code %d; session cleaning; CAR will be sent; .", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber, receivedSegment->cancelSegment.cancelCode);
		cleanSession(session);
		_handlerForwardingToSpanThreadNewReceivedSignal(span, receivedSegment); //It will pass the CR to the span thread to send the CAR
		break;
	case RECENTLY_CLOSED :
		doLog("Session (E%d,#%d):\tReceived CR of a recently closed Tx session. CAR will be sent", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
		_handlerForwardingToSpanThreadNewReceivedSignal(span, receivedSegment); //It will pass the CR to the span thread to send the CAR
		break;
	case UNKNOWN :
		doLog("Session (E%d,#%d):\tReceived CR of an unknown Rx session. Segment dropped.", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
		destroySegment(receivedSegment);
		break;
	}
	return;
}

void receiverHandler_CAR(LTPSpan* span, LTPSegment* receivedSegment) {//type 15
	session_list_state_t list;
	LTPSession* session;
	int result;

	list=findRxSession(span, receivedSegment, &session);
	switch (list) {
	case ACTIVE :
		doLog("Session (E%d,#%d):\tCAR received", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
		if ( session->RxSession.state == CR_SENT) {
			doLog("Session (E%d,#%d):\tsession was in CR_SENT state, CR confirmed by CAR; remove CR from the re-tx list", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
					// stop retransmission timer
			stopTimer(((ResendStruct*)session->RxSession.CRWaitingForCAR)->timerID);
			pthread_mutex_lock(&(session->RTXTimersLock));
			list_remove_data(&(session->RTXTimers), &(((ResendStruct*)session->RxSession.CRWaitingForCAR)->timerID), sizeof(((ResendStruct*)session->RxSession.CRWaitingForCAR)->timerID), NULL);
			pthread_mutex_unlock(&(session->RTXTimersLock));

//			int indexCR = list_find(span->structsToSendBySpanThread, &(receivedSegment), sizeof(receivedSegment), findAssociateCX);
			int indexCR = list_find(span->notifToSpanThread, &receivedSegment, sizeof(receivedSegment), findCXNotifFromCAX);
			if(indexCR > 0){
				// remove the pointer of CR from retransmission list and free the memory
//				void* pointer = list_remove_index_get_pointer(&(span->structsToSendBySpanThread), indexCR, NULL);
				doLog("Session (E%d,#%d):\found resendCR notif: removed", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
				void* pointer = list_remove_index_get_pointer(&(span->notifToSpanThread), indexCR, NULL);
				FREE(pointer);
				uint64_t dummy = -1; // -1 because I have removed an element
				result=write(span->receivedNotifEventFD, &dummy, sizeof(dummy));
				if (result<0) {
					doLog("Session (E%d,#%d):\t CAR received, error in function write", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
					exit (-1);
				}
			}


			// free the memory
			FREE(((ResendStruct*)session->RxSession.CRWaitingForCAR)->segment);
			FREE(session->RxSession.CRWaitingForCAR);
			closeSession(session);  // Put the session in recently close
		}
		break;
	case RECENTLY_CLOSED :
	case UNKNOWN :
		doLog("CAR received of an unknown Session (E%d,#%d)", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
		break;
	}
	destroySegment(receivedSegment);
	return;
}

/* To send the received segment to span thread for further processing */
static void _handlerForwardingToSpanThreadNewReceivedSignal(LTPSpan* span, LTPSegment* receivedSegment) {

	int result;

	pthread_mutex_lock(&span->lock);
	list_append(&(span->receivedSignalSegmentsForSpanThread), receivedSegment, sizeof(LTPSegment)); // Add the segment to the list
	pthread_mutex_unlock(&span->lock);
	// Signal the presence of a new segment in the list
	uint64_t dummy = 1; // 1 because I have 1 new segment to process
	result=write(span->receivedSignalSegmentEventFD, &dummy, sizeof(dummy));
	if (result<0) {
		doLog("Span %d:\tIn pause. New segment received error in function write", span->nodeNumber);
		exit (-1);
	}
}

static void _checkAllDataReceived(LTPSession* session) {
	LTPBlock*			block = session->block;
	List 				current;
	unsigned int 		nextOffset = 0;
	unsigned int 		oldOffset;
	bool				gapFound = false;
	bool				isAllDataReceived = false;

	pthread_mutex_lock(&(session->RxSession.segmentListLock));
	current = session->RxSession.segmentList;

	if ( session->RxSession.state == ALL_DATA_RECEIVED) { // Delivered yet -> Skip it
		pthread_mutex_unlock(&(session->RxSession.segmentListLock));
		return;
	}

	if ( !session->RxSession.isEOBarrived || session->RxSession.segmentList == empty_list ) { // EOB is not arrived OR there are no segments -> is not fully arived
		pthread_mutex_unlock(&(session->RxSession.segmentListLock));
		return;
	}

	// First row to set oldOffset
	if ( current != empty_list ) {
		LTPSegment* currentSegment = (LTPSegment*) current->data;
		if ( (currentSegment->dataSegment.offset + currentSegment->dataSegment.length) != block->blockLength ) { // Not the last segment
			gapFound = true;
		} else {
			oldOffset = currentSegment->dataSegment.offset;
		}
		current = current->next;
	}

	// Cycle looking for gaps
	for (; current != empty_list && !gapFound; current = current->next) {
		LTPSegment* currentSegment = (LTPSegment*) current->data;
		if ( currentSegment->dataSegment.offset + currentSegment->dataSegment.length < oldOffset ) {   // Gap found
			gapFound = true;
			break; // Gap found -> exit cycle
		}
		oldOffset = currentSegment->dataSegment.offset;
	}

	isAllDataReceived = ( (!gapFound) && (oldOffset == 0) ); // if (haven't found any gaps) AND (last segment offset is 0) ->  All data arrived

	if ( isAllDataReceived ) {
		// "compact" the segments into the block.data array
		session->block->data = mallocWrapper(session->block->blockLength);
		for (current = session->RxSession.segmentList; current != empty_list; current = current->next ) { // For each segment copy the data in the array
			LTPSegment* currentSegment = (LTPSegment*) current->data;
			memcpy((session->block->data+currentSegment->dataSegment.offset), currentSegment->dataSegment.data, currentSegment->dataSegment.length);
		}
		// Destroy the old segment list & decrement the currentRXBufferUsed
		doLog("Session (E%d,#%d):\tBlock completed; to be delivered to upper protocol", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		clearRXBuffer(session);


		deliverBlockToUpperProtocol(session->block); // TO DO: check result

		FREE(session->block->data); // Free the data delivered to upper protocol

		// it is delivered --> Change the state
		session->RxSession.state = ALL_DATA_RECEIVED;
		//session->span->currentRxBufferUsed--; // XXX Free one RX buffer
	}

	pthread_mutex_unlock(&(session->RxSession.segmentListLock));
}
