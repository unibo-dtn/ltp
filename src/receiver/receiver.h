/** \file receiver.h
 *
 * \brief This file contains the headers of the LTP receiver functions
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef RECEIVER_H_
#define RECEIVER_H_

/**
 * \par Function Name:
 *      mainReceiver
 *
 * \brief The main function of the receiver thread
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void*
 *
 * \retval Actually it always returns NULL
 *
 * \param  args 		Useless. Always NULL
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void*		mainReceiver(void *args);

/**
 * \par Function Name:
 *      stopReceiver
 *
 * \brief It stops the receiver thread
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void		stopReceiver();

extern pthread_mutex_t lockThreads;

#endif /* RECEIVER_H_ */
