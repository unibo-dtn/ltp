/*
 * rcvHandlers.h
 *
 *  Created on: 18 dic 2022
 *      Author: root
 */

#ifndef SRC_RECEIVER_RCVHANDLERS_H_
#define SRC_RECEIVER_RCVHANDLERS_H_
#include "../LTPspan/spanStruct.h"
#include "../LTPsegment/segment.h"
#include "../sender/sendStruct.h"
#include "../LTPsession/session.h"


/**
 * \par Function Name:
 *      signalSpanNewSegmentReceived
 *
 * \brief It signals a span that a new segment is received
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  receivedSegment 		The received segment
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 			handleNewSegmentReceived(LTPSegment* receivedSegment);



/**
 * \par Function Name:
 *      handleNewDataSegmentReceived
 *
 * \brief  Called when a new data segment is received
 *
 *
 * \par Date Written:
 *      20/04/21
 *
 * \return bool
 *
 * \retval  True if segment needs further processing, false otherwise
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  20/04/21 | D. Filoni       |  Initial Implementation and documentation.
 *  28/11/22 | C. Caini        |  Revised version; better handling of mixed segments
 *****************************************************************************/
bool 				handleNewDataSegmentReceived(LTPSpan* span, LTPSegment* receivedSegment, LTPSession** session);

/**
 * \par Function Name:
 *      receiverHandler_CP
 *
 * \brief  Called by receiver thread when a CP segment is received
 *
 *
 * \par Date Written:
 *      21/04/22
 *
 * \return bool True if further processing is required
 *
 * \retval  True if segment is processed well, false otherwise
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  21/04/22 | C. Cippitelli   |  Initial Implementation and documentation.
 *****************************************************************************/
bool				receiverHandler_CP(LTPSpan* span, LTPSegment* receivedSegment, LTPSession* session);

/**
 * \par Function Name:
 *      receiverHandler_OrangeEOB
 *
 * \brief  Called by receiver thread when a orange EOB data segment is received
 *
 *
 * \par Date Written:
 *      10/03/22
 *
 * \return void
 *
 * \retval  True if segment is processed well, false otherwise
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  10/03/22 | C. Cippitelli   |  Initial Implementation and documentation.
 *****************************************************************************/
void 				receiverHandler_OrangeEOB(LTPSpan* span, LTPSegment* receivedSegment, LTPSession* session);

/**
 * \par Function Name:
 *      receiverHandler_GreenEOB
 *
 * \brief  Called by receiver thread when a green EOB data segment is received
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \retval  True if segment is processed well, false otherwise
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				receiverHandler_GreenEOB(LTPSpan* span, LTPSegment* receivedSegment, LTPSession* session);

/**
 * \par Function Name:
 * 		receiverHandler_RS
 *
 *
 *
 * \brief  Called by receiver thread if a RS is received
 *
 *
 *
 * \par Date Written:
 *      07/04/22
 *
 * \return void
 *
 * \param  session			The pointer to the session
 * \param  receivedSegment  The segment received
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  07/04/22 | C. Cippitelli   |  Initial Implementation and documentation.
 *****************************************************************************/
void 				receiverHandler_RS(LTPSpan* span, LTPSegment* receivedSegment);

/**
 * \par Function Name:
 *      receiverHandler_RA
 *
 * \brief  Called by receiver thread when a new RA is received
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void				receiverHandler_RA(LTPSpan* span, LTPSegment* receivedSegment);

/**
 * \par Function Name: receiverHandler_OrangePN
 *
 *
 *
 * \brief  Called by receiver thread when a Positive Notification (PN) is received
 *
 *
 *
 * \par Date Written:
 *      16/03/22
 *
 * \return void
 *
 * \param  session			The pointer to the session
 * \param  receivedSegment  The segment received
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  16/03/21 | C. Cippitelli   |  Initial Implementation and documentation
 *  02/12/22 | C. Caini        |  PN and NN cases separated into two functions
 *****************************************************************************/
void 			receiverHandler_OrangePN(LTPSpan* span, LTPSegment* receivedSegment);

/**
 * \par Function Name: receiverHandler_OrangeNN
 *
 *
 *
 * \brief  Called by receiver thread when an Orange Negative Notification (NN) is received
 *
 *
 *
 * \par Date Written:
 *      16/03/23
 *
 * \return void
 *
 * \param  session			The pointer to the session
 * \param  receivedSegment  The segment received
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  16/03/21 | C. Cippitelli   |  Initial Implementation and documentation
 *  02/12/22 | C. Caini        |  PN and NN cases separated into two functions
 *****************************************************************************/
void 			receiverHandler_OrangeNN(LTPSpan* span, LTPSegment* receivedSegment);

/**
 * \par Function Name:
 *      receiverHandler_CS
 *
 * \brief  Called by receiver thread when a CS segment is received
 *
 *
 * \par Date Written:
 *      21/04/22
 *
 * \return void
 *
 * \retval  True if segment is processed well, false otherwise
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  21/04/22 | C. Cippitelli   |  Initial Implementation and documentation.
 *****************************************************************************/
void				receiverHandler_CS(LTPSpan* span, LTPSegment* receivedSegment);




/**
 * \par Function Name:
 *      receiverHandler_CAS
 *
 * \brief  Called by receiver thread when a new CAS is received
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				receiverHandler_CAS(LTPSpan* span, LTPSegment* receivedSegment);


/**
 * \par Function Name:
 *      receiverHandler_CR
 *
 * \brief  Called by receiver thread when a new CR is received
 *
 *
 * \par Date Written:
 *      04/05/22
 *
 * \return void
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  04/05/22 | C. Cippitelli   |  Initial Implementation and documentation.
 *****************************************************************************/
void				receiverHandler_CR(LTPSpan* span, LTPSegment* receivedSegment);

/**
 * \par Function Name:
 *      receiverHandler_CAR
 *
 * \brief  Called by receiver thread when a new CAR is received
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				receiverHandler_CAR(LTPSpan* span, LTPSegment* receivedSegment);


#endif /* SRC_RECEIVER_RCVHANDLERS_H_ */
