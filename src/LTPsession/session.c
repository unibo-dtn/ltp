/** \file session.c
 *
 * \brief This file contains the implementations of the LTPsession functions
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 * \authors Davide Filoni, davide.filoni2@studio.unibo.it
 * \authors Chiara Cippitelli, chiara.cippitelli@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>

#include "session.h"

#include "../list/list.h"
#include "../config.h"

#include "../logger/logger.h"
#include "../LTPsegment/segment.h"
#include "../LTPspan/span.h"
#include "../LTPspan/spanHandlers.h"
#include "../sender/sender.h"
#include "../adapters/protocol/upperProtocol.h"
#include "../generic/generic.h"
#include "../timer/timer.h"
#include "../sender/sendNotif.h"

/***** PRIVATE FUNCTIONS *****/
static bool _existsSegmentInSession(LTPSegment* segment, LTPSession *session);


bool haveGapInAckedRanges(LTPSession* session);
static LTPBlock* _newBlock();



/***** FOR EACH FUNCTION *****/
static void _forEachDestroySegment(void* data, size_t size);
/***** HANDLER FUNCTIONS *****/


// the signature of these methods is required by the implementation of the Bisacchi's list library

static void _destroyRSWaitingForCP(void* data, size_t size);
static void _destroyCPWaitingForRS(void* data, size_t size, void* session);
static void _freezeSessionTimer(void* data, size_t size);
static void _resumeSessionTimer(void* data, size_t size);
static void _stopSessionTimer(void* data, size_t size);
/***** PRIVATE VARIABLES *****/

static unsigned long long myNodeNumber = 0;

/*!
 * \brief Acked Ranges
 */
typedef struct {
	/**
	 * \brief Byte from
	 */
	unsigned int from;
	/**
	 * \brief Byte to
	 */
	unsigned int to;
} AckedRange;

/***** PUBLIC FUNCTIONS *****/

void setMyNodeNumber(unsigned long long _myNodeNumber) {
	myNodeNumber = _myNodeNumber;
}

unsigned long long getMyNodeNumber() {
	return myNodeNumber;
}

void freezeSession(LTPSession* session) {
	pthread_mutex_lock(&(session->RTXTimersLock));
	list_for_each(session->RTXTimers, _freezeSessionTimer);
	pthread_mutex_unlock(&(session->RTXTimersLock));
	pauseTimer(session->TxSession.timerOrangeNotification);
	doLog("Session (E%d,#%d):\tFreezed", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
}

void resumeSession(LTPSession* session) {
	pthread_mutex_lock(&(session->RTXTimersLock));
	list_for_each(session->RTXTimers, _resumeSessionTimer);
	pthread_mutex_unlock(&(session->RTXTimersLock));
	resumeTimer(session->TxSession.timerOrangeNotification);
	doLog("Session (E%d,#%d)\tResumed", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
}

void cleanAndCloseSession(LTPSession* session) {
	cleanSession(session);
	closeSession(session);
}

void initializeSession(LTPSession* session, LTPSpan* span, unsigned long long sessionOriginator, unsigned int sessionNumber, LTPSessionColor sessionColor, SessionType sessionType) {
	session->clientServiceID=getUpperProtocolClientServiceID();
	session->sessionID.sessionOriginator = sessionOriginator;
	session->sessionID.sessionNumber 	 = sessionNumber;

	session->color = sessionColor;

	session->RTXTimers = empty_list;

	session->block = _newBlock();
	session->span = span;

	session->type = sessionType;

	pthread_mutexattr_t ma;
	pthread_mutexattr_init(&ma);
	pthread_mutexattr_settype(&ma, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&(session->RTXTimersLock), &ma);

	if ( sessionType == RX_SESSION ) {
		session->RxSession.segmentList = empty_list;
		session->RxSession.RSWaitingForRA = empty_list;
		session->RxSession.RSWaitingForCP = empty_list;
		session->RxSession.isEOBarrived = false;
		session->RxSession.CPReceived = empty_list;
		session->RxSession.CRWaitingForCAR = NULL;
		session->RxSession.state = RX_NORMAL;
		session->RxSession.finalRSSentNumber = 0;
		session->RxSession.timerCancelRXSession = 0;
		session->RxSession.timerMaxIntersegment = 0;
		session->RxSession.lastRSnumber = rand() % 1024 + 1;

		pthread_mutexattr_t ma;
		pthread_mutexattr_init(&ma);
		pthread_mutexattr_settype(&ma, PTHREAD_MUTEX_RECURSIVE);
		pthread_mutex_init(&(session->RxSession.segmentListLock), &ma);

	} else if ( sessionType == TX_SESSION ) {
		session->TxSession.isFullySent = false;
		session->TxSession.destination = 0;
		session->TxSession.CPWaitingForRS = empty_list;
		session->TxSession.CSWaitingForCAS = NULL;
		session->TxSession.listRangeAcked = empty_list;
		session->TxSession.RSReceived = empty_list;
		session->TxSession.RSProcessed = empty_list;
		session->TxSession.dataNumber = 0;
		session->TxSession.handlerSessionTerminated = NULL;
		session->TxSession.state = TX_NORMAL;
		session->TxSession.timerCancelTXSession = 0;
		session->TxSession.timerOrangeNotification = 0;
		session->TxSession.lastCPnumber = 0;
	}
}

void cleanSession(LTPSession* session) {
	destroyBlock(session); // if not already destroyed

	pthread_mutex_lock(&(session->RTXTimersLock));
	list_for_each(session->RTXTimers, _stopSessionTimer);
	list_destroy(&(session->RTXTimers));
	pthread_mutex_unlock(&(session->RTXTimersLock));

	pthread_mutex_destroy(&(session->RTXTimersLock));

	if (session->type == RX_SESSION) {


		if (session->RxSession.CRWaitingForCAR != NULL) FREE(session->RxSession.CRWaitingForCAR->segment)
		if (session->RxSession.CRWaitingForCAR != NULL) FREE(session->RxSession.CRWaitingForCAR); //CCaini mancava
		// segmentList
		clearRXBuffer(session);
		// RSWaitingForRA
		list_for_each_fparam(session->RxSession.RSWaitingForRA, destroyRSWaitingForRa, session);
		list_destroy(&(session->RxSession.RSWaitingForRA));
		// RSWaitingForCP
		list_for_each(session->RxSession.RSWaitingForCP, _destroyRSWaitingForCP);
		list_destroy(&(session->RxSession.RSWaitingForCP));
		// CPReceived
		list_destroy(&(session->RxSession.CPReceived));
		// stop intersegment timer and cancel timer
		stopTimer(session->RxSession.timerMaxIntersegment);
		stopTimer(session->RxSession.timerCancelRXSession);

		pthread_mutex_destroy(&(session->RxSession.segmentListLock));

	} else if (session->type == TX_SESSION) {


		if (session->TxSession.CSWaitingForCAS != NULL) FREE(session->TxSession.CSWaitingForCAS->segment)
		if (session->TxSession.CSWaitingForCAS != NULL) FREE(session->TxSession.CSWaitingForCAS); //CCaini mancava
		// CPWaitingForRS
		list_for_each_fparam(session->TxSession.CPWaitingForRS, _destroyCPWaitingForRS, session);
		list_destroy(&(session->TxSession.CPWaitingForRS));
		// listRangeAcked
		list_destroy(&(session->TxSession.listRangeAcked));
		// RSReceived
		list_destroy(&(session->TxSession.RSReceived));
		// RSProcessed
		list_destroy(&(session->TxSession.RSProcessed));
		// stop cancel timer
		stopTimer(session->TxSession.timerCancelTXSession);
		// stop orange notification timer
		stopTimer(session->TxSession.timerOrangeNotification);
	}
}



session_list_state_t findRxSession(LTPSpan* span, LTPSegment* receivedSegment, LTPSession** psession) {

	session_list_state_t list;
	LTPSession* session = list_get_pointer_data(span->activeRxSessions, &(receivedSegment->header.sessionID),
			sizeof(LTPSessionID), findSessionFromSessionID); // Search the active sessions
	if ((session == NULL)){//If not found, search the recently closed sessions
		if(list_find(span->recentlyClosedRxSessionIDs, &(receivedSegment->header.sessionID), sizeof(LTPSessionID), findSessionFromSessionID) > 0)
			list=RECENTLY_CLOSED;
		else//The session segment is neither in the active nor in the recently closed sessions
			list=UNKNOWN;
	}
	else if ( session != NULL ) { // If session is active -> clean it
		list=ACTIVE;
	}
	*psession=session;
	return list;

}

session_list_state_t findTxSession(LTPSpan* span, LTPSegment* receivedSegment, LTPSession** psession) {

	session_list_state_t list;

	LTPSession* session = list_get_pointer_data(span->activeTxSessions, &(receivedSegment->header.sessionID),
			sizeof(LTPSessionID), findSessionFromSessionID); // Search the active sessions

	if ((session == NULL)){//If not found, search the recently closed sessions

		if(list_find(span->recentlyClosedTxSessionIDs, &(receivedSegment->header.sessionID), sizeof(LTPSessionID), findSessionFromSessionID) > 0){

			list=RECENTLY_CLOSED;
		}
		else //The session segment is neither in the active nor in the recently closed sessions
			list=UNKNOWN;
	}
	else if ( session != NULL ) { // If session is active -> clean it
		list=ACTIVE;
	}
	*psession=session;
	return list;
}


void closeSession(LTPSession* session){
	//Remove session from active session
	pthread_mutex_t* lock = &(session->span->lock); // Cache because it will be destroyed
	pthread_mutex_lock(lock); // Lock the span

	LTPSessionID sessionID = session->sessionID;


	EndedSession endedSession;
//	endedSession.sessionID = session->sessionID;
	endedSession.sessionID = sessionID;
	gettimeofday(&endedSession.endTime, NULL);
	endedSession.endTime.tv_sec += 2*(session->span->rtoTime * MAX_RETX_NUMBER);

	if ( session->type == RX_SESSION ) { // select the right list
		list_push_front(&session->span->recentlyClosedRxSessionIDs, &endedSession, sizeof(endedSession));
		/*if(session->RxSession.state != ALL_DATA_RECEIVED) // if all data received the buffer was freed at change of state
				session->span->currentRxBufferUsed--; // Free one RX buffer*/
		doLog("Session (E%d,#%d):\tInserted in recently closed Rx session list", sessionID.sessionOriginator, sessionID.sessionNumber);
	} else if ( session->type == TX_SESSION ) {
		list_push_front(&session->span->recentlyClosedTxSessionIDs, &endedSession, sizeof(endedSession));
		sem_post(&(session->span->freeTxConcurrentSessionsSemaphore));

		if(session->color != Orange){ //to avoid duplicate log
		doLog("Session (E%d,#%d):\tTX buffer freed", sessionID.sessionOriginator, sessionID.sessionNumber);
		}
		doLog("Session (E%d,#%d):\tInserted in recently closed Tx session list", sessionID.sessionOriginator, sessionID.sessionNumber);
	}

	if ( session->type == RX_SESSION ) { // select the right list
		// README session is no more allocated after the following function
		list_remove_data(&session->span->activeRxSessions, &session->sessionID, sizeof(session->sessionID), findSessionFromSessionID);
	} else if ( session->type == TX_SESSION ) {
		// README session is no more allocated after the following function
		list_remove_data(&session->span->activeTxSessions, &session->sessionID, sizeof(session->sessionID), findSessionFromSessionID);
	}

	doLog("Session (E%d,#%d):\tClosed", sessionID.sessionOriginator, sessionID.sessionNumber);
    fflush (stdout);
    flushFileLogger();
	pthread_mutex_unlock(lock); // Unlock the span
}



int findReportSegmentFromReportSegmentSerialNumber(void* _reportSerialNumber, size_t _reportSerialNumberSize, void* _sendStruct, size_t _sendStructSize) {
	unsigned int reportSerialNumber = *((int*) _reportSerialNumber);
	ResendStruct* sendStruct = (ResendStruct*) _sendStruct;
	if ( sendStruct->segment->reportSegment.reportSerialNumber == reportSerialNumber ) {
		return 0;
	} else {
		return 1;
	}
}


/***** PRIVATE FUNCTIONS *****/

bool haveGapInAckedRanges(LTPSession* session) {
	unsigned int lastTo 	= 0;

	for(List current = session->TxSession.listRangeAcked; current != empty_list; current = current->next) {
		AckedRange* currentackedRange = (AckedRange*) current->data;
		if ( currentackedRange->from > lastTo ) { // Gap found
			return true;
		}

		lastTo 		= (lastTo >= currentackedRange->to ? lastTo : currentackedRange->to); // Save the max of the 2
	}

	return ( lastTo != session->block->blockLength );
}

void saveAckedRanges(LTPSession* session, LTPSegment* reportSegment) {
	for (int i = 0; i < reportSegment->reportSegment.receptionClaimCount; i++) {
		AckedRange currentAckedRange;
		currentAckedRange.from 	= reportSegment->reportSegment.receptionClaims[i].offset;
		currentAckedRange.to 	= currentAckedRange.from + reportSegment->reportSegment.receptionClaims[i].length;
		list_push_ordered(&(session->TxSession.listRangeAcked), &currentAckedRange, sizeof(currentAckedRange), insertOrderedAckedRange);
	}
}




bool addSegmentToSession(LTPSession *session, LTPSegment* segment) {
	if ( session == NULL ) {
		doLog("Session (E%d,#%d):\tERROR in addSegmentToSession:Session=NULL i", segment->header.sessionID.sessionOriginator, segment->header.sessionID.sessionNumber);
		return false;//for safety reasons
	}
	if ( session->RxSession.state != RX_NORMAL){
		//TODO  unlock (appunti quaderno)
		doLog("Session (E%d,#%d):\t ERROR in addSegmentToSession: Rx Session state is not RX_Normal", segment->header.sessionID.sessionOriginator, segment->header.sessionID.sessionNumber);
		return false;
	}
	if ( _existsSegmentInSession(segment, session) ) {
		doLog("Session (E%d,#%d):\tWarning duplicated data segment arrived, dropped", segment->header.sessionID.sessionOriginator, segment->header.sessionID.sessionNumber);
		//TODO unlock dello stato!
		return false;
	} else { // the segment is fresh -> add to the list of segments working as Rx-buffer
		if (isCP(*segment)){
			doLog("Session (E%d,#%d):\tFresh data segment arrived, Offset=%d, length=%d, CP=%d, RS=%d", segment->header.sessionID.sessionOriginator, segment->header.sessionID.sessionNumber, segment->dataSegment.offset, segment->dataSegment.length, segment->dataSegment.checkpointSerialNumber, segment->dataSegment.reportSerialNumber);
			fflush(stdout);
		}
		pthread_mutex_lock(&(session->RxSession.segmentListLock));
		//Insert the segment into the segmentList in inverted order
		list_push_ordered(&(session->RxSession.segmentList), segment, sizeof(LTPSegment), insertOrderedSegment);
		pthread_mutex_unlock(&(session->RxSession.segmentListLock));
		//TODO unlock dello stato!
		return true;
	}
}

static bool _existsSegmentInSession(LTPSegment* newSegment, LTPSession *session) {
	List 			current;
	unsigned int 	oldOffset = 0;
	unsigned int 	oldEnd = 0;
	bool			isFirstRow = true;

	pthread_mutex_lock(&(session->RxSession.segmentListLock));

	if ( session->RxSession.segmentList == empty_list ) { // List empty -> not exists
		pthread_mutex_unlock(&(session->RxSession.segmentListLock));
		return false;
	}

	unsigned int 	newSegmentEnd = newSegment->dataSegment.offset+newSegment->dataSegment.length;

	for (current = session->RxSession.segmentList; current != empty_list; current = current->next) {
		LTPSegment* currentSegment = (LTPSegment*) current->data;
		const unsigned int currentSegmentEnd = currentSegment->dataSegment.offset+currentSegment->dataSegment.length;
		const unsigned int currentSegmentOffset = currentSegment->dataSegment.offset;

		if ( isFirstRow ) {
			if ( newSegment->dataSegment.offset >= currentSegmentEnd ) {
				pthread_mutex_unlock(&(session->RxSession.segmentListLock));
				return false; // Segment doesn't exist
			}
			oldOffset = currentSegmentOffset;
			oldEnd = currentSegmentEnd;
			isFirstRow = false;
			continue;
		}

		if ( currentSegmentEnd < oldOffset ) { // Gap found
			if ( ( 			newSegment->dataSegment.offset >= currentSegmentEnd			// Segment is in the gap
					&&	newSegmentEnd <= oldOffset									// Example: received is 0-100 and 200-300 and segment is 100-200
			) || (
					newSegment->dataSegment.offset < currentSegmentEnd			// Segment is half in gap and half in received
					&&	newSegmentEnd > currentSegmentEnd							// Example: received 0-100 and segment is 50-150
			) || (
					newSegmentEnd > oldOffset									// Segment is half in gap and half in received
					&&	newSegment->dataSegment.offset < oldOffset					// Example: received 0-100 and 400-500 and segment is 350-450
			)
			) {
				pthread_mutex_unlock(&(session->RxSession.segmentListLock));
				return false; // Segment doesn't exist
			}
			oldEnd = currentSegmentEnd;
		}

		if ( newSegment->dataSegment.offset >= currentSegmentOffset ) {
			pthread_mutex_unlock(&(session->RxSession.segmentListLock));
			return true; // Segment exists
		}

		oldOffset = currentSegmentOffset;
	}

	if ( current == empty_list && newSegment->dataSegment.offset < oldOffset ) {
		pthread_mutex_unlock(&(session->RxSession.segmentListLock));
		return false; // Segment doesn't exist
	}

	pthread_mutex_unlock(&(session->RxSession.segmentListLock));

	return true;
}

List getClaimsFromSession(LTPSession* session, unsigned int lowerByteToAck, const unsigned int upperByteToAck, bool* isLastRS) {
	List 				current = empty_list;
	List 				claims  = empty_list;
	LTPReportSegmentClaim 	currentClaim;

	if ( session->RxSession.state == ALL_DATA_RECEIVED ) { // Block is delivered to UP -> all is arrived, generated a single claim to ACK everything
		currentClaim.offset = 0;
		currentClaim.length = session->block->blockLength;
		list_push_front(&claims, &currentClaim, sizeof(LTPReportSegmentClaim));
		if ( isLastRS != NULL )
			*isLastRS = true;
		return claims;
	} else { // Block is not delivered to UP
		if ( isLastRS != NULL )
			*isLastRS = false;
	}

	pthread_mutex_lock(&(session->RxSession.segmentListLock));

	if ( session->RxSession.segmentList == empty_list ) {
		pthread_mutex_unlock(&(session->RxSession.segmentListLock));
		return empty_list;
	}

	current = session->RxSession.segmentList;

	currentClaim.offset = 0;
	currentClaim.length = 0;

	while (current != empty_list) {
		LTPSegment* currentSegment = (LTPSegment*) current->data;
		if ( currentSegment->dataSegment.offset + currentSegment->dataSegment.length <= upperByteToAck ) {
			currentClaim.length = currentSegment->dataSegment.length;
			currentClaim.offset = currentSegment->dataSegment.offset;
			current = current->next;
			break;
		}
		current = current->next;
	}
	for (; current != empty_list; current = current->next) {
		LTPSegment* currentSegment = (LTPSegment*) current->data;

		if ( (currentSegment->dataSegment.offset + currentSegment->dataSegment.length) <= lowerByteToAck ) {
			break; // Going over the lowerByteToAck
		}

		if ( currentClaim.offset > (currentSegment->dataSegment.offset + currentSegment->dataSegment.length) ) {  	// Gap found
			list_push_front(&claims, &currentClaim, sizeof(LTPReportSegmentClaim));
			currentClaim.length = currentSegment->dataSegment.length;
		} else { 																									// No gap
			currentClaim.length += currentSegment->dataSegment.length; // Increase the claim length
		}

		currentClaim.offset = currentSegment->dataSegment.offset;
	}

	list_push_front(&claims, &currentClaim, sizeof(LTPReportSegmentClaim));

	pthread_mutex_unlock(&(session->RxSession.segmentListLock));
	return claims;
}


void clearRXBuffer(LTPSession* session) {
	// TODO lock anche sullo stato
	pthread_mutex_lock(&(session->RxSession.segmentListLock));


	if ( session == NULL || session->type != RX_SESSION || session->RxSession.segmentList == empty_list) {
		pthread_mutex_unlock(&(session->RxSession.segmentListLock));
		return;
	}

	list_for_each(session->RxSession.segmentList, _forEachDestroySegment);
	list_destroy(&(session->RxSession.segmentList));

	session->span->currentRxBufferUsed--;
//	session->RxSession.state= RX_BUFFER_FLUSHED; //TODO

	doLog("Session (E%d,#%d):\tRX buffer freed", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);

	pthread_mutex_unlock(&(session->RxSession.segmentListLock));
	// TODO unlock state
	}

static LTPBlock* _newBlock() {
	LTPBlock* result = (LTPBlock*) mallocWrapper(sizeof(LTPBlock));

	result->data = NULL;
	result->blockLength = 0;

	return result;
}

void destroyBlock(LTPSession* session) {
	if (session == NULL || session->block == NULL)
		return;

	FREE(session->block->data);

	FREE(session->block);
	session->block = NULL;
}


/***** FOR EACH FUNCTION *****/

static void _forEachDestroySegment(void* data, size_t size) {
	destroySegment((LTPSegment*) data);
}

/***** HANDLER FUNCTIONS *****/


void handlerTxSessionTimeout(TimerID timerID, void* _session) {
	LTPSession* session = (LTPSession*) _session;

	if(session->color== Orange){ //At present it should be called only for Orange Tx
		doLog("Session (E%d,#%d):\tExpired, signaling FAILURE to upper protocol", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		if ( session->TxSession.handlerSessionTerminated != NULL ){
			session->TxSession.handlerSessionTerminated(session->TxSession.dataNumber, SessionResultError); // Signal the upper protocol this session is not completed correctly
		}
		cleanAndCloseSession(session);
	}

}


void handlerRxSessionTimeout(TimerID timerID, void* _session) {
	LTPSession* session = (LTPSession*) _session;
	Notification* CR_notif;
	Notification NN_notif;

	doLog("Session (E%d,#%d):\tExpired", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
//At present it should be called only for Green
/*
	if ( session->color == Red ) {
		clearRXBuffer(session);

		CR_notif=(Notification*) mallocWrapper(sizeof(Notification));
		*CR_notif=createNotif(CANCEL_RED_RX_SESSION, session->sessionID, 0, SESSION_TIMEOUT, session->span->nodeNumber);//Non passo il puntatore a session
		forwardingNotifToSpanThread(CR_notif);//Add a COPY of the notif pointer to the list

	}
	else if(session->color== Orange){
		clearRXBuffer(session);
		NN_notif=createNotif(CLOSE_ORANGE_RX_SESSION, session->sessionID, 0, SESSION_TIMEOUT, session->span->nodeNumber);
		forwardingNotifToSpanThread(&NN_notif);//Add a COPY of the notif pointer to the list
	} */
	if (session->color == Green){
		cleanAndCloseSession(session);
	}
}

/*
//Chiamata da cleanSession e receiverHandler_CP
void destroyRSWaitingForRetransmissionOld(void* data, size_t size) {
	ResendStruct* sendStruct = (ResendStruct*) data;
	stopTimer(sendStruct->timerID);
	pthread_mutex_lock(&(sendStruct->session->RTXTimersLock));
	list_remove_data(&(sendStruct->session->RTXTimers), &sendStruct->timerID, sizeof(sendStruct->timerID), NULL);
	pthread_mutex_unlock(&(sendStruct->session->RTXTimersLock));
	destroySegment(sendStruct->segment);
	FREE(sendStruct->segment);
}
*/


//The
void destroyRSWaitingForRa(void* data, size_t size, void* _session) {
	LTPSession* session;
	ResendStruct* sendStruct;

	sendStruct= (ResendStruct*) data;
	session=(LTPSession*) _session;

	stopTimer(sendStruct->timerID);
	pthread_mutex_lock(&(session->RTXTimersLock));
	list_remove_data(&(session->RTXTimers), &sendStruct->timerID, sizeof(sendStruct->timerID), NULL);
	pthread_mutex_unlock(&(session->RTXTimersLock));
	destroySegment(sendStruct->segment);
	FREE(sendStruct->segment);
}


static void _destroyRSWaitingForCP(void* data, size_t size) {
//It is simpler than destroyRSwaitingForRA because RSWaitingForCP list consists of segments not send_struct.
//Moreover there is not any timer associated.
	LTPSegment* segment = (LTPSegment*) data;
	destroySegment(segment);
}

//Chiamata da cleanSession //Chiedere a Bisacchi se la mancanza della destroy segment è voluta e perché.
static void _destroyCPWaitingForRS(void* data, size_t size, void* _session) {
	LTPSession* session;
	ResendStruct* sendStruct;

	sendStruct= (ResendStruct*) data;
	session=(LTPSession*) _session;

	stopTimer(sendStruct->timerID);
	pthread_mutex_lock(&(session->RTXTimersLock));
	list_remove_data(&(session->RTXTimers), &sendStruct->timerID, sizeof(sendStruct->timerID), NULL);
	pthread_mutex_unlock(&(session->RTXTimersLock));
	FREE(sendStruct->segment);
}
/*

static void _destroyCPWaitingForRSOld(void* data, size_t size) {
	ResendStruct* sendStruct = (ResendStruct*) data;
	stopTimer(sendStruct->timerID);
	pthread_mutex_lock(&(sendStruct->session->RTXTimersLock));
	list_remove_data(&(sendStruct->session->RTXTimers), &sendStruct->timerID, sizeof(sendStruct->timerID), NULL);
	pthread_mutex_unlock(&(sendStruct->session->RTXTimersLock));
	FREE(sendStruct->segment);
}
*/


//CCaini: non capito perche' non uso direttamente la pauseTimer. Forse per estrarre da Data l'ID del timer.
//Idem per le due sotto.
static void _freezeSessionTimer(void* data, size_t size) {
	TimerID* timerID = (TimerID*) data;
	pauseTimer(*timerID);
}

static void _resumeSessionTimer(void* data, size_t size) {
	TimerID* timerID = (TimerID*) data;
	resumeTimer(*timerID);
}

static void _stopSessionTimer(void* data, size_t size) {
	TimerID* timerID = (TimerID*) data;
	stopTimer(*timerID);
}

/***** COMPARE FUNCTIONS *****/

/*
 * Descending order of offset value. The offset will be descending
 */
int insertOrderedSegment(void* newSegment, size_t newSegmentSize, void* segment, size_t segmentSize) {
	return ((LTPSegment*) segment)->dataSegment.offset - ((LTPSegment*) newSegment)->dataSegment.offset;
}

int insertOrderedAckedRange(void* _newAckedRange, size_t newAckedRangeSize, void* _currentAckedRange, size_t currentAckedSize) {
	AckedRange* newAckedRange = (AckedRange*) _newAckedRange;
	AckedRange* currentAckedRange = (AckedRange*) _currentAckedRange;
	return (newAckedRange->from - currentAckedRange->from);
}


//Used by receiverHandler_RS & resend_CP_RS_CS_CR
int findCheckpointFromCheckpointNumber(void* _checkpointNumber, size_t checkpointNumberSize, void* _sendStruct, size_t sendStructSize) {
	unsigned int checkpointNumber = *((unsigned int*) _checkpointNumber);
	ResendStruct* sendStruct = (ResendStruct*) _sendStruct;
	if ( sendStruct->segment->dataSegment.checkpointSerialNumber == checkpointNumber ) {
		return 0;
	} else {
		return 1; // not equals
	}
}

//Used by resend_CP_RS_CS_CR
int findReportFromReportNumber(void* _reportNumber, size_t reportNumberSize, void* _sendStruct, size_t sendStructSize) {
	unsigned int reportNumber = *((unsigned int*) _reportNumber);
	ResendStruct* sendStruct = (ResendStruct*) _sendStruct;
	if ( sendStruct->segment->reportSegment.reportSerialNumber == reportNumber ) {
		return 0;
	} else {
		return 1; // not equals
	}
}


//CCaini E' diversa da quella sopra perche' gli RS nalla lista RSWaitiungforCP sono segmenti e non sendStruct!
//Used by spanHandler_CP per trovare nalla RSWaitiungforCP (lista di LTPSegment non sendStruct!), l'RS corrispondente al CP, per i claim
int findReportSegmentFromCheckpoint(void* _reportSerialNumber, size_t _reportSerialNumberSize, void* _reportSegment, size_t _reportSegmentSize) {
	unsigned int reportSerialNumber = *((int*) _reportSerialNumber);
	LTPSegment* reportSegment = (LTPSegment*) _reportSegment;
	if ( reportSegment->reportSegment.reportSerialNumber == reportSerialNumber ) {
		return 0;
	} else {
		return 1;
	}
}

/*
 * Finds in the list the session from a LTPSessionID
 */
//Used by receiverHandlers, findTxSession, findRXsession, closeSession, handleNotification, spanHandler_OrangeEOB.
int findSessionFromSessionID(void* _sessionID, size_t _sessionID_size, void* _session, size_t _sessionSize) {
	LTPSessionID* 	sessionID 	= (LTPSessionID*) _sessionID;
	LTPSession*		session 	= (LTPSession*)	_session;
	if (	sessionID->sessionOriginator == session->sessionID.sessionOriginator
			&& 	sessionID->sessionNumber == session->sessionID.sessionNumber) {
		return 0;
	} else {
		return 1; // not equals
	}
}

/*
 * The following four "find" functions are used to eliminate resending segment notifications waiting to
 * be processed by the span thread, whenever the retransmission is made unnecessary by the arrival of a
 * confirmation segment. All of them follow the same template.
 */

//Used by receiverHandler_RS
int findCPNotifFromRS (void* _Segment, size_t SegmentSize, void* _data, size_t dataSize){
	LTPSegment* RSSegment;
	Notification* notif;

	if(sizeof(_data) == sizeof(Notification*)){
		notif =(Notification*) _data;//data is the generic element of the list
		RSSegment= (LTPSegment*) _Segment;//To cast _Segment (void*) to LTPsegment* and then to assign
		// search RESEND_RS notifications with the same SessionID and Report Serial Number
		if(notif->type==RESEND_CP &&
				notif->sessionID.sessionNumber == RSSegment->header.sessionID.sessionNumber &&
				notif->sessionID.sessionOriginator == RSSegment->header.sessionID.sessionOriginator &&
				notif->sequenceNumber == RSSegment->dataSegment.checkpointSerialNumber)
			return 0;//Found (in analogy with C "compare" 0 means equal, 1 means different)
	}
	return 1;
}

//Used by receiverHandlerRA
//Note that RS retransmissions are made redundant by both the arrival of response CPs or confirmning RAs
int findRSNotifFromRA (void* _Segment, size_t SegmentSize, void* _data, size_t dataSize){
	LTPSegment* RASegment;
	Notification* notif;

	if(sizeof(_data) == sizeof(Notification*)){
		notif =(Notification*) _data;//data is the generic element of the list
		RASegment= (LTPSegment*) _Segment;//To cast _Segment (void*) to LTPsegment* and then to assign
		// search RESEND_RS notifiucations with the same SessionID and Report Serial Number
		if(notif->type==RESEND_RS &&
				notif->sessionID.sessionNumber == RASegment->header.sessionID.sessionNumber &&
				notif->sessionID.sessionOriginator == RASegment->header.sessionID.sessionOriginator &&
				notif->sequenceNumber == RASegment->dataSegment.reportSerialNumber)
			return 0;//Found (in analogy with C "compare" 0 means equal, 1 means different)
	}
	return 1;
}


//Used by receiverHandlerCP
//Note that RS retransmissions are made redundant by both the arrival of response CPs or confirmning RAs
int findRSNotifFromCP (void* _Segment, size_t SegmentSize, void* _data, size_t dataSize){
	LTPSegment* CPSegment;
	Notification* notif;

	if(sizeof(_data) == sizeof(Notification*)){
		notif =(Notification*) _data;//data is the generic element of the list
		CPSegment= (LTPSegment*) _Segment;//To cast _Segment (void*) to LTPsegment* and then to assign
		// search RESEND_RS with the same SessionID and Report Serial Number
		if(notif->type==RESEND_RS &&
				notif->sessionID.sessionNumber == CPSegment->header.sessionID.sessionNumber &&
				notif->sessionID.sessionOriginator == CPSegment->header.sessionID.sessionOriginator &&
				notif->sequenceNumber == CPSegment->dataSegment.reportSerialNumber)
			return 0;//Found (in analogy with C "compare" 0 means equal, 1 means different)
	}
	return 1;
}

//Used by receiverHandler_CAR e receiverHandler_CAS
int findCXNotifFromCAX(void* _Segment, size_t SegmentSize, void* _data, size_t dataSize){
	LTPSegment* CAXSegment;
	Notification* notif;

	if(sizeof(_data) == sizeof(Notification*)){//If the dimension is different, the two elements are necessarily different (return 1)

		notif =(Notification*) _data;//data is the generic element of the list
		CAXSegment= (LTPSegment*) _Segment;//To cast _Segment (void*) to LTPsegment* and then to assign

		// search CX with the same SessionID
		if(notif->type==RESEND_CS &&
				notif->sessionID.sessionNumber == CAXSegment->header.sessionID.sessionNumber &&
				notif->sessionID.sessionOriginator == CAXSegment->header.sessionID.sessionOriginator )
			return 0;//Found (in analogy with C "compare" 0 means equal, 1 means different)
	}
	return 1;//Not Found
}

