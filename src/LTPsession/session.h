/** \file session.h
 *
 * \brief This file contains the headers of the LTPsession functions and related structures
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 * \authors Chiara Cippitelli, chiara.cippitelli@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef SRC_LTPSESSION_SESSION_H_
#define SRC_LTPSESSION_SESSION_H_


#include "sessionStruct.h"

#include "../sender/sendStruct.h"
#include "../list/list.h"
#include "../LTPsegment/segment.h"


#include <stdbool.h>

//structure
typedef enum session_list_t
{
	/**
		* \The session is in the active list
	*/
	ACTIVE=0,
	/**
		* \The session is in the recently closed list
	*/
	RECENTLY_CLOSED=1,
	/**
		* \brief The session is unknown
	*/
	UNKNOWN=2,
} session_list_state_t;

session_list_state_t findTxSession(LTPSpan* span, LTPSegment* receivedSegment, LTPSession** psession);
session_list_state_t findRxSession(LTPSpan* span, LTPSegment* receivedSegment, LTPSession** psession);

/**
 * \par Function Name:
 *      setMyNodeNumber
 *
 * \brief  Sets the node number
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  _myNodeNumber 		The nodeNumber
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				setMyNodeNumber(unsigned long long _myNodeNumber);

/**
 * \par Function Name:
 *      getMyNodeNumber
 *
 * \brief  Gets my node number
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return unsigned long long
 *
 * \retval  My node number
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
unsigned long long 	getMyNodeNumber();

/**
 * \par Function Name:
 *      initializeSession (ex createSesion)
 *
 * \brief  It initializes most of either Tx or Rx session paramaters
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  session 					The session to create
 * \param  span						The span
 * \param  sessionOriginator		The session originator
 * \param  sessionNumber 			The session number
 * \param  sessionColor				The color of the session
 * \param  sessionType 				The session type (RX / TX)
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				initializeSession(LTPSession* session, LTPSpan* span, unsigned long long sessionOriginator, unsigned int sessionNumber, LTPSessionColor sessionColor, SessionType sessionType);

/**
 * \par Function Name:
 *      freezeSession
 *
 * \brief  Freezes all timer in this session
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  session 		The session to freeze
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void		 		freezeSession(LTPSession* session);

/**
 * \par Function Name:
 *      resumeSession
 *
 * \brief  Resumes the session
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  session 		The session to resume
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void				resumeSession(LTPSession* session);

/**
 * \par Function Name:
 *     cleanAndCloseSession
 *
 * \brief  Destroys the session
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  session 		The session to destroy
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void				cleanAndCloseSession(LTPSession* session);


/**
 * \par Function Name:
 *      cleanSession
 *
 * \brief  Cleans the session
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  session 		The session
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				cleanSession(LTPSession* session);

/**
 * \par Function Name:
 *      createNewSendSession
 *
 * \brief  It creates a new Tx session (wrapper of initializeSession)
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 			The span
 * \param  data				The data to send
 * \param  dataLength		The length of the data
 * \param  dataNumber		The dataNumber
 * \param  handlerSessionTerminated The handler to call the session terminates
 * \param  sessionColor		The session color
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				createTxSession(LTPSpan* span, void* data, int dataLength, unsigned int dataNumber, void (*handlerSessionTerminated)(unsigned int, LTPSessionResultCode), LTPSessionColor sessionColor, int redSessionTime);




/**
 * \par Function Name:
 *      closeSession
 *
 * \brief  Remove session from the Active Session and put session in the Recently Closed Session
 *
 *
 * \par Date Written:
 *      10/04/21
 *
 * \return void
 *
 * \param  session			The pointer to the session
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  10/04/21 | D. Filoni       |  Initial Implementation and documentation.
 *****************************************************************************/
void 		closeSession(LTPSession* session);


/**
 * \par Function Name:
 *      generateCR
 *
 * \brief  Generete CR segment, change session state to CR_SENT and clean session
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  session			The pointer to the session
 * \param  cancelCode		The cancel code to insert into the segment
 * \param  ipAddress	    The recipient IP address
 * \param  portNumber		The recipient port number
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void generateCR(LTPSession* session, LTPCancelReasonCode cancelCode);


/**
 * \par Function Name:
 *      generateCS
 *
 * \brief  Generete CS segment, change session state to CS_SENT and clean session
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  session			The pointer to the session
 * \param  cancelCode		The cancel code to insert into the segment
 * \param  ipAddress	    The recipient IP address
 * \param  portNumber		The recipient port number
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void generateCS(LTPSession* session, LTPCancelReasonCode cancelCode);

/**
 * \par Function Name:
 *      _generateNN
 *
 * \brief  Generete NN segment
 *
 *
 * \par Date Written:
 *      30/04/22
 *
 * \return void
 *
 * \param  session			The pointer to the session
 * 		   span 			The pointer to the span
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  30/04/22 | C. Cippitelli   |  Initial Implementation and documentation.
 *****************************************************************************/
void generateNN(LTPSession*session,LTPSpan* span);

/**
 * \par Function Name:
 *      findReportSegmentFromReportSegmentSerialNumber
 *
 * \brief  Find the report segment from the report segment serial number
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return int
 *
 * \retval  The report segment
 *
 * \param  _reportSerialNumber			The report segment serial number
 * \param  _reportSerialNumberSize		The size of the report segment serial number
 * \param  _reportSegment	    		The pointer of the report segment
 * \param  _reportSegmentSize			The size of the report segment
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
int findReportSegmentFromReportSegmentSerialNumber(void* _reportSerialNumber, size_t _reportSerialNumberSize, void* _reportSegment, size_t _reportSegmentSize);


void clearRXBuffer(LTPSession* session);
void handlerTxSessionTimeout(TimerID timerID, void* _session);
void handlerRxSessionTimeout(TimerID timerID, void* _session);
bool	addSegmentToSession(LTPSession *session, LTPSegment* segment);
void destroyRSWaitingForRa(void* data, size_t size, void* session);
void destroyBlock(LTPSession* session);
/***** COMPARE FUNCTIONS *****/
int insertOrderedSegment(void* newSegment, size_t newSegmentSize, void* segment, size_t segmentSize);
int insertOrderedAckedRange(void* _newAckedRange, size_t newAckedRangeSize, void* _currentAckedRange, size_t currentAckedSize);
int findCheckpointFromCheckpointNumber(void* _checkpointNumber, size_t checkpointNumberSize, void* _sendStruct, size_t sendStructSize);
int findReportFromReportNumber(void* _reportNumber, size_t reportNumberSize, void* _sendStruct, size_t sendStructSize);
int findReportSegmentFromCheckpoint(void* _reportSerialNumber, size_t _reportSerialNumberSize, void* _reportSegment, size_t _reportSegmentSize);
int findSessionFromSessionID(void* _sessionID, size_t _sessionID_size, void* _session, size_t _sessionSize);
int findCPNotifFromRS (void* _RSSegment, size_t RSSize, void* _data, size_t dataSize);
int findRSNotifFromRA (void* _RASegment, size_t RASize, void* _data, size_t dataSize);
int findRSNotifFromCP (void* _CPSegment, size_t CPSize, void* _data, size_t dataSize);
int findCXNotifFromCAX (void* _CAXSegment, size_t CAXSize, void* _data, size_t dataSize);

List getClaimsFromSession(LTPSession* session, unsigned int lowerByteToAck, const unsigned int upperByteToAck, bool* isLastRS);
void saveAckedRanges(LTPSession* session, LTPSegment* reportSegment);
bool reportSegmentResendNotReceivedData(LTPSession* session, LTPSegment* reportSegment);
bool haveGapInAckedRanges(LTPSession* session);
#endif /* SRC_LTPSESSION_SESSION_H_ */
