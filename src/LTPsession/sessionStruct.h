/** \file sessionStruct.h
 *
 * \brief This file contains the defininition of the Session structure
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 * \authors Davide Filoni, davide.filoni2@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *
 *
 ***********************************************/

#ifndef SRC_LTPSESSION_SESSIONSTRUCT_H_
#define SRC_LTPSESSION_SESSIONSTRUCT_H_

#include <stdbool.h>


#include "sessionID.h"
#include "sessionColor.h"
#include "../LTPspan/spanStruct.h"
#include "../list/list.h"
#include "../sender/sendStruct.h"


/*!
 * \brief LTP session result code
 */
typedef enum {
	SessionNoResult, SessionResultSuccess, SessionResultCancel, SessionResultError
} LTPSessionResultCode;

/*!
 * \brief LTP RX Session state
 */
typedef enum {
	RX_NORMAL, RX_BUFFER_FLUSHED, ALL_DATA_RECEIVED, CR_SENT, CS_RECEIVED
}LTPRXSessionState;

/*!
 * \brief LTP TX Session state
 */
typedef enum {
	TX_NORMAL, CS_SENT, CR_RECEIVED
}LTPTXSessionState;

/*!
 * \brief LTP block
 */
typedef struct {
	/**
	 * \brief The array of data (the effective payload)
	 */
	void*						data;
	/**
	 * \brief The block length
	 */
	unsigned int 				blockLength;
} LTPBlock;

/*!
 * \brief RX session specific fields
 */
typedef struct {
	/**
	 * \brief The list of segments received
	 */
	List						segmentList;

	/**
	 * \brief The mutex to access to segmentList
	 */
	pthread_mutex_t				segmentListLock;

	/**
	 * \brief The list of the RS sent and waiting for RA
	 */
	List 						RSWaitingForRA;
	/**
	 * \brief The list of RS waiting for CP
	 */
	List 						RSWaitingForCP;

	/**
	 * \brief The pointer of CR waiting for CAR
	 */
	ResendStruct* 						CRWaitingForCAR;

	/**
	 * \brief The list of CP received
	 */
	List 						CPReceived;

	/**
	 * \brief True if the EOB is arrived
	 */
	bool 						isEOBarrived;

	/**
	* \brief The state of the session
	*/
	LTPRXSessionState 			state;

	/**
	 * \brief The reportSerialNumber of the RS which confirms the whole session
	 */
	unsigned int				finalRSSentNumber;

	/**
	* \brief The timer to cancel a too long RX session
	*/
	TimerID						timerCancelRXSession;

	/**
	* \brief The timer to cancel a session eith a too long intersegment time
	*/
	TimerID						timerMaxIntersegment;

	/**
	* \brief The number of the last RS sent (numbering is unique inside one session)
	*/
	unsigned int 						lastRSnumber;
} LTPRXSession;

/*!
 * \brief TX Session specific fields
 */

typedef struct {
	/**
	 * \brief The destination node number
	 */
	unsigned long long			destination;

	/**
	 * \brief The list of range acked by RS
	 */
	List 						listRangeAcked;
	/**
	 * \brief The list of CP waiting for RS
	 */
	List 						CPWaitingForRS;

	/**
	 * \brief The pointer of CS waiting for CAS
	 */
	ResendStruct*					CSWaitingForCAS;

	/**
	 * \brief The list of RS received
	 */
	List 						RSReceived;
	/**
	 * \brief The list of RS processed by span thread
	 */
	List 						RSProcessed;
	/**
	 * \brief The data number provided from upper protocol which is used when handler is called
	 */
	unsigned int 				dataNumber;
	/**
	 * \brief Tells if is fully sent
	 */
	bool						isFullySent;
	/**
	 * \brief The handler to call when the session is completed with a success or a failure
	 */
	void						(*handlerSessionTerminated)(unsigned int, LTPSessionResultCode);
	/**
	* \brief The state of the session
	*/
	LTPTXSessionState 			state;

	/**
	* \brief The timer to cancel a too long TX session
	*/
	TimerID						timerCancelTXSession;

	/**
	* \brief The timer to cancel a TX Orange session waiting for too long NN or PN
	*/
	TimerID 					timerOrangeNotification;

	/**
	* \brief The number of the last CP sent (numbering is unique inside one session)
	*/
	unsigned int 						lastCPnumber;

} LTPTXSession;

/*!
 * \brief LTP session
 */
typedef struct {
	/**
	 * \brief The session identifier
	 */
	LTPSessionID				sessionID;

	/**
	 * \brief The pointer to the block
	 */
	LTPBlock*					block;

	/**
	 * \brief The list of re-trasmission timers.
	 */
	List 						RTXTimers;

	/**
	 * \brief The mutex to access to re-trasmission timers.
	 */
	pthread_mutex_t				RTXTimersLock;

	/**
	 * \brief The pointer to the span
	 */
	LTPSpan*					span;

	/**
	 * \brief The color of this session
	 */
	LTPSessionColor				color;


	/**
	 * \brief The session type
	 */
	SessionType					type;


	unsigned int  		clientServiceID;

	union {
		/**
		 * \brief The specific RX values
		 */
		LTPRXSession			RxSession;
		/**
		 * \brief The specific TX values
		 */
		LTPTXSession			TxSession;
	};

} LTPSession;

/*!
 * \brief Ended session
 */
typedef struct {
	/**
	 * \brief The session ID of the ended session
	 */
	LTPSessionID	sessionID;
	/**
	 * \brief The time after which this session can be flushed from recently terminated sessions
	 */
	struct timeval 	endTime;
} EndedSession;


#endif /* SRC_LTPSESSION_SESSIONSTRUCT_H_ */
