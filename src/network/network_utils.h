/*
 * net_utils.h
 *
 *  Created on: 20 ott 2024
 *      Author: carlo
 */

#ifndef SRC_NETWORK_NET_UTILS_H_
#define SRC_NETWORK_NET_UTILS_H_

/*
    network_utils.h

     Author: Andrea Lo Buglio (andrea.lobuglio@studio.unibo.it)
     Project Supervisor: Carlo Caini (carlo.caini@unibo.it)

    Copyright (c) 2024, Alma Mater Studiorum, University of Bologna
    All rights reserved.

*/

#include <netdb.h>

   typedef struct {
        int ai_family; // if == AF_INET => IPv4, if == AF_INET6 => IPv6
        union {
            struct sockaddr_in ipv4;
            struct sockaddr_in6 ipv6;
        } address;
    } INET_struct;


    int parseHostnameAndPort(char* hostnameAndPort, INET_struct *addr, uint16_t defaultPort);

#endif /* SRC_NETWORK_NET_UTILS_H_ */
