/** \file segment.h
 *
 * \brief This file contains the defines of the functions and the structs of an LTPsegment
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef SRC_LTPSEGMENT_SEGMENT_H_
#define SRC_LTPSEGMENT_SEGMENT_H_

#include <stdbool.h>

#include "../LTPsession/sessionID.h"

/**
 * \brief The LTP version
 *
 * \hideinitializer
 */
#define LTP_VERSION 0

/*!
 * \brief LTP Segment type codes according to RFC 5326
 */
typedef enum {
	LTP_Red_Data 				= 0,		/* 	Red data segment 							*/
	LTP_Red_Data_CP 			= 1,		/* 	Red data segment + checkpoint 				*/
	LTP_Red_Data_CP_EORP 		= 2,		/* 	Red data segment + checkpoint + EORP 		*/
	LTP_Red_Data_CP_EORP_EOB 	= 3,		/* 	Red data segment + checkpoint + EORP + EOB 	*/

	LTP_Green_Data 				= 4,		/* 	Green data segment 							*/
	LTP_Green_Data_EOB 			= 7,		/* 	Green data segment + EOB 					*/

	LTP_Orange_Data				= 5,		/*	Orange data segment							*/
	LTP_Orange_Data_EOB			= 6,		/*	Orange data segment + EOB				*/

	LTP_RS 						= 8,		/* 	Report Segment.								*/
	LTP_RA 						= 9,		/* 	Report Acknowledgment.						*/

	LTP_Orange_PN				= 10,		/* 	Orange Positive Notification (PN)			*/
	LTP_Orange_NN				= 11,		/* 	Orange Negative Notification (NN)			*/

	LTP_CS 						= 12,		/* 	Cancel from Sender.					*/
	LTP_CAS 					= 13,		/* 	CS acknowledgment.							*/

	LTP_CR 						= 14,		/* 	Cancel from Receiver.		*/
	LTP_CAR 					= 15		/* 	CR acknowledgment.							*/
} LTPSegmentTypeCode;

/*!
 * \brief LTP Cancel code reasons according to RFC 5326 + 1 New
 */
typedef enum {
	USR_CNCLD					= 0,		/* 	Client service canceled session 			*/
	UNREACH						= 1,		/* 	Unreachable client service					*/
	RLEXC						= 2,		/* 	Retransmission limit exceeded				*/
	MISCOLORED					= 3,		/* 	Received red data BEFORE green data			*/
	SYS_CNCLD					= 4,		/* 	System error 								*/
	RXMTCYCEXC					= 5,		/* 	Retransmission-Cycles limit 				*/
	SESSION_TIMEOUT				= 6,			/*  Session timeout expired						*/
	DUMMY						= 7
} LTPCancelReasonCode;


/*!
 * \brief LTP Segment header
 */
typedef struct { //All according to RFC5326
	/**
	 * \brief The type of segment header
	 */
	LTPSegmentTypeCode							typeFlag;

	/**
	 * \brief The session ID (originator and number)
	 */
	LTPSessionID								sessionID;	
	/**
	 * \brief The number of header extensions
	 */
	unsigned char 								headerExtensionsCount;
	/**
	 * \brief The number of trailer extensions
	 */
	unsigned char 								trailerExtensionsCount;

} LTPSegmentHeader;

/*!
 * \brief LTP Data segment
 */
typedef struct {
	/**
	 * \brief The client service ID
	 */
	unsigned int 								clientServiceID;
	/**
	 * \brief The offset of the data
	 */
	unsigned int 								offset;
	/**
	 * \brief The length of data present in this segment
	 */
	unsigned int								length;

	/**
	 * \brief The checkpoint serial number. If the segment is not a CP this values is 0 
	 * RFC5326 states that this field should not be present unless the data segment is flagged
	 * as CP. CP=0 is forbidden for CPs.
	 * Its value is irrelevant in internal use unless the data segment is flagged as CP.
	 */
	unsigned int 								checkpointSerialNumber;
	/**
	 * \brief The report serial number. If the segment is not a CP this values is 0.
	 * RFC5326 states that this field should not be present unless the data segment is flagged
	 * as CP. RS=0 is not forbidden for CPs.
	 * Its value is irrelevant in internal use unless the data segment is flagged as CP.
	 */
	unsigned int								reportSerialNumber;

	/**
	 * \brief The paylaod data
	 */
	void*										data;
} LTPDataSegmentSpecificPart;

/*!
 * \brief  Report Segment Claim
 */
typedef struct {
	/**
	 * \brief The claim offset
	 */
	unsigned int 								offset;
	/**
	 * \brief The claim length
	 */
	unsigned int								length;
} LTPReportSegmentClaim;

/*!
 * \brief  Report Segment specific fields
 */
typedef struct {
	/**
	 * \brief The report serial number
	 */
	unsigned int								reportSerialNumber;
	/**
	 * \brief The checkpoint serial number
	 */
	unsigned int								checkpointSerialNumber;
	/**
	 * \brief The RS upper bound
	 */
	unsigned int								upperBound;
	/**
	 * \brief The RS lower bound
	 */
	unsigned int								lowerBound;

	/**
	 * \brief The number of claims
	 */
	unsigned int								receptionClaimCount;
	/**
	 * \brief The array of claims
	 */
	LTPReportSegmentClaim*						receptionClaims;
} LTPReportSegmentSpecificPart;

/*!
 * \brief Report Ack specific fields
 */
typedef struct {
	/**
	 * \brief The report serial number of the RS which is confirmed by this RA
	 */
	unsigned int 								reportSerialNumber;
} LTPReportAckSegmentSpecificPart;

/*!
 * \brief Cancel segment specific fields
 */

typedef struct {
	LTPCancelReasonCode 								cancelCode;
} LTPCancelSegmentSpecificPart;


/*!
 * \brief LTP Segment
 */
typedef struct {
	/**
	 * \brief The LTP segment header
	 */
	LTPSegmentHeader		 					header;

	/**
	 * \brief Adds the additional values (based on the type of segment)
	 */
	union {
		/**
		 * \brief Only if segment is a data segment
		 */
		LTPDataSegmentSpecificPart 					dataSegment;
		/**
		 * \brief Only if the segment is a RS
		 */
		LTPReportSegmentSpecificPart 				reportSegment;
		/**
		 * \brief Only if the segment is a RA
		 */
		LTPReportAckSegmentSpecificPart				reportAckSegment;
		/**
		 * \brief Only if the segment is a CS or CR
		 */
		LTPCancelSegmentSpecificPart				cancelSegment
		;
	};
} LTPSegment;


/**
 * \par Function Name:
 *      parsePacketIntoSegment
 *
 * \brief  Transforms a buffer into a segment
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return int
 *
 * \retval  1 = success. 0 if fails. Negative if an error occurred
 *
 * \param  cursor 			The data to parse
 * \param  bytesRemaining	The amount of data to parse
 * \param  result			The LTP segment which will be filled
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
int 			deserializeSegment (char *cursor, int bytesRemaining, LTPSegment *result);

/**
 * \par Function Name:
 *      putSegmentInBuffer
 *
 * \brief  Inserts a segment into a buffer
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return int
 *
 * \retval  Number of bytes inserted
 *
 * \param  segment 		The segment to inset into the buffer
 * \param  buffer		The buffer to fill
 * \param  bufferSize	The buffer size
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
unsigned int	serializeSegment(LTPSegment* segment, char* buffer, unsigned int bufferSize);

/**
 * \par Function Name:
 *      destroySegment
 *
 * \brief  Destroys a segment
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  segment 		The segment to destroy
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 			destroySegment(LTPSegment *segment);



/**
 * \brief Tells if the segment is an End Of Block
 *
 * \hideinitializer
 */
#define 		isEOB(segment)				( (segment).header.typeFlag == LTP_Red_Data_CP_EORP_EOB || (segment).header.typeFlag == LTP_Green_Data_EOB || (segment).header.typeFlag == LTP_Orange_Data_EOB )

/**
 * \brief Tells if the segment is a CheckPoint
 *
 * \hideinitializer
 */
#define 		isCP(segment) 		( (segment).header.typeFlag==LTP_Red_Data_CP || (segment).header.typeFlag==LTP_Red_Data_CP_EORP || (segment).header.typeFlag==LTP_Red_Data_CP_EORP_EOB )

/**
 * \brief Tells if the segment is one of the 4 red data segments (including CP EORP EOB)
 *
 * \hideinitializer
 */
#define 		isAnyKindRedData(segment) 			( (segment).header.typeFlag == LTP_Red_Data || (segment).header.typeFlag == LTP_Red_Data_CP || (segment).header.typeFlag == LTP_Red_Data_CP_EORP || (segment).header.typeFlag == LTP_Red_Data_CP_EORP_EOB )

/**
 * \brief Tells if the segment is a Report Segment
 *
 * \hideinitializer
 */
#define			isRS(segment)			( (segment).header.typeFlag == LTP_RS )

/**
 * \brief Tells if the segment is an Ack for RS
 *
 * \hideinitializer
 */
#define 		isRA(segment)			( (segment).header.typeFlag == LTP_RA )

/**
 * \brief Tells if the segment is a Cancel from Sender segment
 *
 * \hideinitializer
 */
#define	 		isCS(segment)			( (segment).header.typeFlag == LTP_CS )

/**
 * \brief Tells if the segment is an Ack for CS
 *
 * \hideinitializer
 */
#define 		isCAS(segment)			( (segment).header.typeFlag == LTP_CAS )

/**
 * \brief Tells if the segment is a Cancel from Receiver segment
 *
 * \hideinitializer
 */
#define 		isCR(segment)	( (segment).header.typeFlag == LTP_CR )

/**
 * \brief Tells if the segment is an Ack for CR segment
 *
 * \hideinitializer
 */
#define 		isCAR(segment)	( (segment).header.typeFlag == LTP_CAR )

/**
 * \brief Tells if the segment is a green segment
 *
 * \hideinitializer
 */
#define 		isAnyKindGreenData(segment)			( (segment).header.typeFlag == LTP_Green_Data || (segment).header.typeFlag == LTP_Green_Data_EOB )

/**
 * \brief Tells if the segment is a green EOB data segment
 *
 * \hideinitializer
 */
#define 		isGreenDataEOB(segment)			( (segment).header.typeFlag == LTP_Green_Data_EOB )

/**
 * \brief Tells if the segment is an orange segment
 *
 * \hideinitializer
 */
#define 		isAnyKindOrangeData(segment)			( (segment).header.typeFlag == LTP_Orange_Data || (segment).header.typeFlag == LTP_Orange_Data_EOB )

/**
 * \brief Tells if the segment is an orange EOB data segment
 *
 * \hideinitializer
 */
#define 		isOrangeDataEOB(segment)			( (segment).header.typeFlag == LTP_Orange_Data_EOB )

/**
 * \brief Tells if the segment is an orange notification (PN or NN)
 *
 * \hideinitializer
 */
#define 		isOrangeNotification(segment)			( (segment).header.typeFlag == LTP_Orange_PN ||  (segment).header.typeFlag == LTP_Orange_NN)

/**
 * \brief Tells if the segment is a data segment (green or red)
 *
 * \hideinitializer
 */
#define			isDataSegment(segment)				( isAnyKindRedData(segment) || isAnyKindGreenData(segment) || isAnyKindOrangeData(segment) )

/**
 * \brief Tells if the segment is a signal segment
 *
 * \hideinitializer
 */
//CCaini C'era CAS spurio tolto
#define 		isSignalSegmentToAck(segment)			( isCP(segment) || isRS(segment) || isCS(segment) || isCR(segment) || isOrangeDataEOB(segment) )
/**
 * \brief Tells if the segment belongs to a RX session
 *
 * \hideinitializer
 */
#define 		isRXSession(segment)			( isAnyKindRedData(segment) || isRA(segment) || isCS(segment)|| isCAR(segment) ||	isAnyKindGreenData(segment) || isAnyKindOrangeData(segment) )

/**
 * \brief Tells if the segment belongs to a TX session
 *
 * \hideinitializer
 */
#define 		isTXSession(segment)			( isRS(segment) || isCAS(segment) || isCR(segment) || isOrangeNotification(segment) )

/**
 * \brief Tells if the segment is a CX
 *
 * \hideinitializer
 */
#define 		isCXSegment(segment)			( isCR(segment)|| isCS(segment) )

#endif /* SRC_LTPSEGMENT_SEGMENT_H_ */
