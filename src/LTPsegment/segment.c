/** \file segment.c
 *
 * \brief This file contains the implementations of the LTP segment functions
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include "segment.h"

#include "../sdnv/sdnv.h"

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "../generic/generic.h"

/***** PRIVATE FUNCTIONS *****/

static bool _putGreenSegment (LTPSegment *segment, char **cursor, int *bytesRemaining);
static bool _parseGreenSegment (char **cursor, int *bytesRemaining, LTPSegment *result);

static bool _putOrangeSegment (LTPSegment *segment, char **cursor, int *bytesRemaining);
static bool _parseOrangeSegment (char **cursor, int *bytesRemaining, LTPSegment *result);

static bool _putRedSegment (LTPSegment *segment, char **cursor, int *bytesRemaining);
static bool _parseRedSegment (char **cursor, int *bytesRemaining, LTPSegment *result);

static bool _putReportSegment (LTPSegment *segment, char **cursor, int *bytesRemaining);
static bool _parseReportSegment (char **cursor, int *bytesRemaining, LTPSegment *result);

static bool _putReportAckSegment (LTPSegment *segment, char **cursor, int *bytesRemaining);
static bool _parseReportAckSegment (char **cursor, int *bytesRemaining, LTPSegment *result);

static bool _putCancelSegment (LTPSegment *segment, char **cursor, int *bytesRemaining);
static bool _parseCancelSegment (char **cursor, int *bytesRemaining, LTPSegment *result);

void destroySegment(LTPSegment* segment) {
	if ( isAnyKindRedData(*segment) || isAnyKindGreenData(*segment) || isAnyKindOrangeData(*segment)) {
		//For data segments, if data are allocated, free them
		if ( segment->dataSegment.data != NULL ) FREE(segment->dataSegment.data);
	} else if ( isRS(*segment) ) {
		if ( segment->reportSegment.receptionClaims != NULL ) FREE(segment->reportSegment.receptionClaims);
	}
	// ReportAckSegment 		-> NOTHING to free
	// Cancel Sender 			-> NOTHING to free
	// Cancel Sender ACK (CAS)		-> NOTHING to free
	// Cancel Receiver			-> NOTHING to free
	// Cancel Receiver ACK (CAR)		-> NOTHING to free
	// Positive Notification    -> NOTHING to free
	// Negative Notification    -> NOTHING to free

	memset(segment, 0, sizeof(LTPSegment)); // Sets all to 0, and so also pointers to NULL
}

int deserializeSegment (char *cursor, int bytesRemaining, LTPSegment *result) {
	if (cursor == NULL || bytesRemaining <= 0)
		return -1; // Skip

	{
		unsigned char version = ((*cursor) >> 4) & 0x0F;
		if (version != LTP_VERSION) {
			return 0; // Skip
		}
	}

	memset(result, 0, sizeof(LTPSegment));//result sarà alla fine la struttura contenenete i campi del segmento

	// Version + type flag
	result->header.typeFlag = (*cursor) & 0x0F;
	cursor++; bytesRemaining--;

	// Session ID
	extractSdnv(&(result->header.sessionID.sessionOriginator), (unsigned char **) &cursor, &bytesRemaining);
	extractSmallSdnv(&(result->header.sessionID.sessionNumber), (unsigned char **) &cursor, &bytesRemaining);

	// Header & trailer extension count
	result->header.headerExtensionsCount = (*cursor >> 4) & 0x0f;
	result->header.trailerExtensionsCount = *cursor & 0x0f;
	cursor++; bytesRemaining--;

	// Header extensions
	for (int i = 0; i < result->header.headerExtensionsCount; i++) {
		// TODO: Parse a single header extension
		return 0; // It cannot handle it
	}

	// Parse specific fields
	bool parsed;
	if ( isAnyKindRedData(*result) ) {
		parsed = _parseRedSegment(&cursor, &bytesRemaining, result);

	} else if ( isRS(*result) ) {
		parsed = _parseReportSegment(&cursor, &bytesRemaining, result);

	} else if ( isRA(*result) ) {
		parsed = _parseReportAckSegment(&cursor, &bytesRemaining, result);

	} else if ( isCS(*result) || isCR(*result) ) {
		parsed = _parseCancelSegment(&cursor, &bytesRemaining, result);

	} else if ( isCAS(*result) || isCAR(*result) || isOrangeNotification(*result) ) {
		// Nothing to do, no spiecific fields
		parsed = true;
	} else if ( isAnyKindGreenData(*result) ) {
		parsed = _parseGreenSegment(&cursor, &bytesRemaining, result);

	} else if ( isAnyKindOrangeData(*result) ) {
		parsed = _parseOrangeSegment(&cursor, &bytesRemaining, result);

	} else {
		parsed = false;
		fprintf(stderr, "Arrived segment with not recognized code %d\n", result->header.typeFlag);
		return 0;
	}
	if ( !parsed ) {
		destroySegment(result);
		return 0; // Skip
	}

	// Trailer extensions
	for (int i = 0; i < result->header.trailerExtensionsCount; i++) {
		// TODO: Parse a single trailer extension
		return 0; // It canot handle it
	}

	return 1;
}

//unsigned int serializeSegment(LTPSegment* segment, char* buffer, unsigned int bufferSize) {
unsigned int serializeSegment(LTPSegment* segment, char* buffer, unsigned int bufferSize) {
	char* cursor = buffer;
	unsigned int bytesRemaining = bufferSize;

	// Version + type flag
	*cursor = LTP_VERSION;
	*cursor <<= 4;
	*cursor += segment->header.typeFlag;
	cursor++; bytesRemaining--;

	// Session ID
	insertSdnv(segment->header.sessionID.sessionOriginator, &cursor, &bytesRemaining);
	insertSdnv(segment->header.sessionID.sessionNumber, &cursor, &bytesRemaining);

	// Header & trailer extension count
	*cursor = segment->header.headerExtensionsCount;
	*cursor <<= 4;
	*cursor += segment->header.trailerExtensionsCount;
	cursor++; bytesRemaining--;

	// Header extensions
	for(int i = 0; i < segment->header.headerExtensionsCount; i++) {
		// TODO: insert a single header extension
		return 0; // Can't handle it
	}

	// Specific fields
	bool put;
	if ( isAnyKindRedData(*segment) ) {
		put = _putRedSegment(segment, &cursor, &bytesRemaining);

	} else if ( isRS(*segment) ) {
		put = _putReportSegment(segment, &cursor, &bytesRemaining);

	} else if ( isRA(*segment) ) {
		put = _putReportAckSegment(segment, &cursor, &bytesRemaining);

	} else if ( isCS(*segment) || isCR(*segment) ) {
		put = _putCancelSegment(segment, &cursor, &bytesRemaining);

	} else if ( isCAS(*segment) || isCAR(*segment) || isOrangeNotification(*segment) ) {
		// Nothing to do, no specific fields
		put = true;

	} else if ( isAnyKindGreenData(*segment) ) {
		put = _putGreenSegment(segment, &cursor, &bytesRemaining);

	} else if ( isAnyKindOrangeData(*segment) ) {
		put = _putOrangeSegment(segment, &cursor, &bytesRemaining);

	} else {
		put = false;
		fprintf(stderr, "Segment with not recognized code %d\n", segment->header.typeFlag);
		return 0;
	}

	if ( !put ) {
		return 0; // Error occurred -> Skip
	}

	// Trailer extensions
	for(int i = 0; i < segment->header.trailerExtensionsCount; i++) {
		// TODO: insert a single trailer  extension
		return 0; // It cannot handle it
	}

	return bufferSize - bytesRemaining;
}

/***** PRIVATE FUNCTIONS *****/

static bool _putGreenSegment(LTPSegment *segment, char **cursor, int *bytesRemaining) {
	insertSdnv(segment->dataSegment.clientServiceID, cursor, bytesRemaining);
	insertSdnv(segment->dataSegment.offset, cursor, bytesRemaining);
	insertSdnv(segment->dataSegment.length, cursor, bytesRemaining);

	memcpy(*cursor, segment->dataSegment.data, segment->dataSegment.length);
	(*cursor) += segment->dataSegment.length;
	*bytesRemaining -= segment->dataSegment.length;

	return true;
}

static bool _parseGreenSegment (char **cursor, int *bytesRemaining, LTPSegment *result) {
	if (!extractSmallSdnv(&(result->dataSegment.clientServiceID), (unsigned char **) cursor, bytesRemaining)) 				return false;
	if (!extractSmallSdnv(&(result->dataSegment.offset), (unsigned char **) cursor, bytesRemaining)) 						return false;
	if (!extractSmallSdnv(&(result->dataSegment.length), (unsigned char **) cursor, bytesRemaining)) 						return false;

	result->dataSegment.data = (char*) mallocWrapper( result->dataSegment.length );
	if (result->dataSegment.data == NULL)																					return false;

	memcpy(result->dataSegment.data, *cursor, result->dataSegment.length);
	(*cursor) += result->dataSegment.length;
	*bytesRemaining -= result->dataSegment.length;

	return true;
}

static bool _putOrangeSegment(LTPSegment *segment, char **cursor, int *bytesRemaining) {
	insertSdnv(segment->dataSegment.clientServiceID, cursor, bytesRemaining);
	insertSdnv(segment->dataSegment.offset, cursor, bytesRemaining);
	insertSdnv(segment->dataSegment.length, cursor, bytesRemaining);

	if ( isOrangeDataEOB(*segment) ) {
		insertSdnv(segment->dataSegment.checkpointSerialNumber, cursor, bytesRemaining);
	}

	memcpy(*cursor, segment->dataSegment.data, segment->dataSegment.length);
	(*cursor) += segment->dataSegment.length;
	*bytesRemaining -= segment->dataSegment.length;

	return true;
}

static bool _parseOrangeSegment (char **cursor, int *bytesRemaining, LTPSegment *result) {

	if (!extractSmallSdnv(&(result->dataSegment.clientServiceID), (unsigned char **) cursor, bytesRemaining)) 				return false;
	if (!extractSmallSdnv(&(result->dataSegment.offset), (unsigned char **) cursor, bytesRemaining)) 						return false;
	if (!extractSmallSdnv(&(result->dataSegment.length), (unsigned char **) cursor, bytesRemaining)) 						return false;

	// If checkpoint -> have to parse checkpointSerialNumber & reportSerialNumber
	if ( isOrangeDataEOB(*result) ) {
		if (!extractSmallSdnv(&(result->dataSegment.checkpointSerialNumber), (unsigned char **) cursor, bytesRemaining)) 	return false;
	}

	result->dataSegment.data = (char*) mallocWrapper( result->dataSegment.length );
	if (result->dataSegment.data == NULL)																					return false;

	memcpy(result->dataSegment.data, *cursor, result->dataSegment.length);
	(*cursor) += result->dataSegment.length;
	*bytesRemaining -= result->dataSegment.length;

	return true;
}

static bool _putRedSegment(LTPSegment *segment, char **cursor, int *bytesRemaining) {
	insertSdnv(segment->dataSegment.clientServiceID, cursor, bytesRemaining);
	insertSdnv(segment->dataSegment.offset, cursor, bytesRemaining);
	insertSdnv(segment->dataSegment.length, cursor, bytesRemaining);

	if ( isCP(*segment) ) {
		insertSdnv(segment->dataSegment.checkpointSerialNumber, cursor, bytesRemaining);
		insertSdnv(segment->dataSegment.reportSerialNumber, cursor, bytesRemaining);
	}

	memcpy(*cursor, segment->dataSegment.data, segment->dataSegment.length);
	(*cursor) += segment->dataSegment.length;
	*bytesRemaining -= segment->dataSegment.length;

	return true;
}

static bool _parseRedSegment (char **cursor, int *bytesRemaining, LTPSegment *result) {

	if (!extractSmallSdnv(&(result->dataSegment.clientServiceID), (unsigned char **) cursor, bytesRemaining)) 				return false;
	if (!extractSmallSdnv(&(result->dataSegment.offset), (unsigned char **) cursor, bytesRemaining)) 						return false;
	if (!extractSmallSdnv(&(result->dataSegment.length), (unsigned char **) cursor, bytesRemaining)) 						return false;

	// If checkpoint -> have to parse checkpointSerialNumber & reportSerialNumber
	if ( isCP(*result) ) {
		if (!extractSmallSdnv(&(result->dataSegment.checkpointSerialNumber), (unsigned char **) cursor, bytesRemaining)) 	return false;
		if (!extractSmallSdnv(&(result->dataSegment.reportSerialNumber), (unsigned char **) cursor, bytesRemaining)) 		return false;
	}

	result->dataSegment.data = (char*) mallocWrapper( result->dataSegment.length );
	if (result->dataSegment.data == NULL)																					return false;

	memcpy(result->dataSegment.data, *cursor, result->dataSegment.length);
	(*cursor) += result->dataSegment.length;
	*bytesRemaining -= result->dataSegment.length;

	return true;
}

static bool _putReportSegment (LTPSegment *segment, char **cursor, int *bytesRemaining) {
	insertSdnv(segment->reportSegment.reportSerialNumber, cursor, bytesRemaining);
	insertSdnv(segment->reportSegment.checkpointSerialNumber, cursor, bytesRemaining);
	insertSdnv(segment->reportSegment.upperBound, cursor, bytesRemaining);
	insertSdnv(segment->reportSegment.lowerBound, cursor, bytesRemaining);

	insertSdnv(segment->reportSegment.receptionClaimCount, cursor, bytesRemaining);
	for(int i = 0; i < segment->reportSegment.receptionClaimCount; i++) {
		insertSdnv(segment->reportSegment.receptionClaims[i].offset, cursor, bytesRemaining);
		insertSdnv(segment->reportSegment.receptionClaims[i].length, cursor, bytesRemaining);
	}

	return true;
}

static bool _parseReportSegment (char **cursor, int *bytesRemaining, LTPSegment *result) {
	if (!extractSmallSdnv(&(result->reportSegment.reportSerialNumber), (unsigned char **) cursor, bytesRemaining)) 				return false;
	if (!extractSmallSdnv(&(result->reportSegment.checkpointSerialNumber), (unsigned char **) cursor, bytesRemaining)) 			return false;
	if (!extractSmallSdnv(&(result->reportSegment.upperBound), (unsigned char **) cursor, bytesRemaining)) 						return false;
	if (!extractSmallSdnv(&(result->reportSegment.lowerBound), (unsigned char **) cursor, bytesRemaining)) 						return false;

	if (!extractSmallSdnv(&(result->reportSegment.receptionClaimCount), (unsigned char **) cursor, bytesRemaining)) 			return false;

	result->reportSegment.receptionClaims = mallocWrapper(sizeof(LTPReportSegmentClaim) * result->reportSegment.receptionClaimCount);

	for(int i = 0; i < result->reportSegment.receptionClaimCount; i++) {
		if (!extractSmallSdnv(&(result->reportSegment.receptionClaims[i].offset), (unsigned char **) cursor, bytesRemaining)) 	return false;
		if (!extractSmallSdnv(&(result->reportSegment.receptionClaims[i].length), (unsigned char **) cursor, bytesRemaining)) 	return false;
	}

	return true;
}

static bool _putReportAckSegment (LTPSegment *segment, char **cursor, int *bytesRemaining) {
	insertSdnv(segment->reportAckSegment.reportSerialNumber, cursor, bytesRemaining);
	return true;
}

static bool _parseReportAckSegment (char **cursor, int *bytesRemaining, LTPSegment *result) {
	if (!extractSmallSdnv(&(result->reportAckSegment.reportSerialNumber), (unsigned char**) cursor, bytesRemaining))			return false;
	return true;
}

static bool _putCancelSegment (LTPSegment *segment, char **cursor, int *bytesRemaining) {
	**cursor = segment->cancelSegment.cancelCode;
	(*cursor)++;	(*bytesRemaining)--;
	return true;
}

static bool _parseCancelSegment (char **cursor, int *bytesRemaining, LTPSegment *result) {
	result->cancelSegment.cancelCode = **cursor;
	(*cursor)++;	(*bytesRemaining)--;
	return true;
}
