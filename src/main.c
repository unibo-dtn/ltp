/** \file main.c
 *
 * \brief This file contains the main of the LTP engine
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 * \authors Davide Filoni, davide.filoni2@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include <stdio.h>
#include <pthread.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>
#include <math.h>
#include <errno.h>
#include <limits.h>
#include <netdb.h>

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#ifndef __USE_GNU
#define __USE_GNU
#endif
#include <ucontext.h>
#include <execinfo.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>

#include "logger/logger.h"
#include "adapters/protocol/lowerProtocol.h"
#include "adapters/protocol/upperProtocol.h"
#include "receiver/receiver.h"
#include "sender/sender.h"
#include "sdnv/sdnv.h"
#include "timer/timer.h"

#include "generic/generic.h"
#include "config.h"

#include "list/list.h"
#include "spanFile/spanFile.h"


/**
 * \brief Handler for INT signal
 *
 * \param code 		Signal code number
 */
void intHandler(int code) {
	signal(SIGINT, SIG_DFL);

	printf("CTRL+C received\n");

	closeAllSpans();
	stopReceiver();

	for (int prints = 3; prints > 0; prints--) {
		printf("Closing... waiting %d seconds\n", prints);
		sleep(1);
	}
    printf("Closing!\n");
}

static void sigHandler(int sig, siginfo_t *si, void* unused)
{
	fprintf(stderr, "Segmentation fault!\n\nStack trace:\n");
    char **strings;
    size_t i, size;
    void *array[1024];
    size = backtrace(array, 1024);
    backtrace_symbols_fd(array, size, STDERR_FILENO);
    exit(EXIT_FAILURE);
}


void printHelp()
{
	printf("\nSyntax:\n");
	printf("Unibo-LTP my_node_number [options]\n");
	printf ("-n, --my_node_number [int]	The local node number \n");
	printf ("--log 						Print logs on standard output \n");
    printf ("--fixed_rto  [ms] 			Set a fixed RTO (in ms) to be used instead of the dynamic one\n");
    printf ("--csv			 			Print logs in Unibo-LTP.csv (one line for each received or transmitted segment)\n");
    printf (" \n");
    printf (" -h, --help                This help.\n");
}

typedef struct {
	unsigned long long my_node_number;
	bool log;
	bool csv;
	unsigned int fixed_rto;
} ltp_options_t;

typedef enum {
	HELP = 50,
	MY_NODE = 51,
	LOG = 52,
	CSV = 53,
	FRTO= 54,
} parser_options_val;

bool parse(int argc, char **argv, ltp_options_t *ltp_options) {
   
	int optionIndex=0;
	
	static struct option long_options[] =
	{
			{"help", no_argument, NULL, 'h'},
			{"my_node_number", required_argument, NULL, 'n'},
			{"log", no_argument, NULL, LOG},
			{"csv", no_argument, NULL, CSV},
			{"fixed_rto", required_argument, NULL, FRTO},
			{0, 0, 0, 0}	// The last element of the array has to be filled with zeros.
			};
			optind=1;

	//set defaults
	ltp_options->my_node_number=0;
	ltp_options->log=false;
	ltp_options->csv=false;
	ltp_options->fixed_rto=0;//0 means disabled

	while ((optionIndex = getopt_long(argc,argv, "hn:", long_options, NULL)) != -1) {
		switch (optionIndex) {
		case 'h':
			printHelp();
			return false;
		case 'n':
			ltp_options->my_node_number=atoi(optarg);
			break;
		case LOG:
			ltp_options->log=true;
			break;
		case CSV:
			ltp_options->csv=true;
			break;
		case FRTO:
			ltp_options->fixed_rto=atoi(optarg);
			if (ltp_options->fixed_rto<TIMER_TICK/1000) {
				printf("fLTP parser error: fixed_rto must be >= TIMER_TICK\n");
				printHelp();
				return false;
			}
			break;
		case '?':
			printf("LTP parser error: option not recognized\n");
			printHelp();
			return false;
			break;
		default:
			break;
		}
	}
	if (ltp_options->my_node_number==0) {
		printf("LTP parser error: my_node_number parameter not set\n");
		printHelp();
		return false;
	}
return true;
}


/**
 * \brief Main function
 *
 * \param argc		Number of arguments
 * \param argv		Arguments
 */
int main(int argc, char **argv) {
	srand(time(NULL));
	signal(SIGINT, intHandler);

	char* buffer;

	#ifdef HAVE_UNIBO_BP
	printf("Unibo-LTP version 2.1.0 compiled for Unibo-BP\n");
	#else
	printf("Unibo-LTP Version 2.1.0 compiled for ION\n");
	#endif

	if( (buffer=getcwd(NULL, 0)) == NULL) {
		perror("failed to get current directory\n");
	} else {
		printf("Current directory:%s\n", buffer);
		free(buffer);
	}

	// Set the SEGFAULT handler
	{
		struct sigaction sa;
		sa.sa_flags = SA_SIGINFO;
		sigemptyset(&sa.sa_mask);
		sa.sa_sigaction = sigHandler;
		sigaction(SIGSEGV, &sa, NULL);
	}
	// start timer
	initTimer();

	unsigned long long myNodeNumber = 0;
	disableLogger();
	bool result;
	ltp_options_t ltp_options;
	
//Parse options
	result=parse(argc, argv, &ltp_options);
    if (result==false) exit(EXIT_FAILURE);

    //Set global variables    
    setMyNodeNumber(ltp_options.my_node_number); //to set a global variable in sessions.c
    setFixedRto(ltp_options.fixed_rto); //to set a global variable in span.c

    //Enable loggers if requested    
    if (ltp_options.log) enableLogger();
    if (ltp_options.csv) enableFileLogger();
    
	// Init components
	if ( !initUpperProtocol() ) {
		fprintf(stderr, "Error on initUpperProtocol\n");
		exit(-1);
	}

	if ( !initLowerLevel() ) {
		fprintf(stderr, "Error on initlowerProtocol\n");
		exit(-2);
	}

	if ( !loadSpansFromConfigFile() ) {
		fprintf(stderr, "Error on parsing json file\n");
		exit(-1);
	}

	// Start threads

	pthread_t receiverThread;
	pthread_create(&receiverThread, NULL, mainReceiver, NULL);

	loadContactPlanUpperProtocol();	//it also starts a periodic timer for periodic updates

	// Wait untill everything is done
	pthread_join(receiverThread,  NULL);

	closeAllSpans();
	stopReceiver();

	//NEW
	destroyTimer();

	// Dispose everything
	disposeUpperProtocol();
	disposeLowerProtocol();

	sleep(1);

	printMallocFreeStat();
	doLog("End");
	disableFileLogger();//Added by CCaini
	return 0;
}

