/*
 * spanHandlers.h
 *
 *  Created on: 18 dic 2022
 *      Author: root
 */

#ifndef SRC_LTPSPAN_SPANHANDLERS_H_
#define SRC_LTPSPAN_SPANHANDLERS_H_

#include "../LTPspan/spanStruct.h"
#include "../LTPsegment/segment.h"
#include "../sender/sendStruct.h"
#include "../LTPsession/session.h"
/**
 * \par Function Name:
 *      spanHandler_CP
 *
 * \brief  Called when a new CP is received
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				spanHandler_CP(LTPSpan* span, LTPSegment* receivedSegment);

/**
 * \par Function Name:
 *      spanHandler_OrangeEOB
 *
 * \brief  Called by Span thread when a orange EOB data segment is received
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \retval  True if segment is processed well, false otherwise
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				spanHandler_OrangeEOB(LTPSpan* span, LTPSegment* receivedSegment);

/**
 * \par Function Name:
 *      spanHandler_RS
 *
 * \brief  Called when a new RS is received
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				spanHandler_RS(LTPSpan* span, LTPSegment* receivedSegment);

/**
 * \par Function Name:
 *      spanHandler_CS
 *
 * \brief  Called when a new CS is received
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				spanHandler_CS(LTPSpan* span, LTPSegment* receivedSegment);

/**
 * \par Function Name:
 *      spanHandler_CR
 *
 * \brief  Called by span thread when a new CR is received
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				spanHandler_CR(LTPSpan* span, LTPSegment* receivedSegment);
void generateDataSegment(LTPSession* session, unsigned int offset, unsigned int length, unsigned int reportSerialNumber, LTPSegmentTypeCode type);
void handleLocalSignalSegmentToSend(LTPSpan* span);

void generateDataSegmentB(LTPSession* session, LTPSpan* span, unsigned int offset, unsigned int length, unsigned int reportSerialNumber, LTPSegmentTypeCode type);
void generateRSB(LTPSession* session, LTPSpan* span, List claims, unsigned int checkpointSerialNumber,
			const unsigned int lowerByteToAck);
void generate_PN_NN_RA_CAS_CAR(LTPSegmentTypeCode type, LTPSpan* span, LTPSegment* receivedSegment);

void handleSignalSegmentToRespond(LTPSpan* span);

#endif /* SRC_LTPSPAN_SPANHANDLERS_H_ */
