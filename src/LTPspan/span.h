/** \file span.h
 *
 * \brief This file contains the headers of the LTP span functions (and related structures)
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 * \authors Davide Filoni, davide.filoni2@studio.unibo.it
 * \authors Chiara Cippitelli, chiara.cippitelli@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef SRC_LTPSPAN_SPAN_H_
#define SRC_LTPSPAN_SPAN_H_

#include "spanStruct.h"

#include "../list/list.h"
#include "../LTPsegment/segment.h"
#include "../LTPsession/sessionColor.h"
#include "../LTPsession/session.h"
#include "../timer/timerStruct.h"
#include "../sender/sendStruct.h"

#include "../contact/contact.h"
#include "../range/range.h"
#include "../adapters/protocol/upperProtocolReceivedData.h"

extern List activeSpans; //In use to handleNewSegmentReceived in receiverHandlers.c

/**
 * \par Function Name:
 *      setOwnqTime
 *
 * \brief Sets the ownqtime
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  _ownqtime 		The ownqtime
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 			setOwnqTime(unsigned int _ownqtime);

/**
 * \par Function Name:
 *      getOwnqTime
 *
 * \brief Gets the ownqtime
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return int
 *
 * \retval	The qwnqtime setted before
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
unsigned int 	getOwnqTime();

/**
 * \par Function Name:
 *      closeAllSpans
 *
 * \brief Closes all spans
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 			closeAllSpans();

/**
 * \par Function Name:
 *      closeSpan
 *
 * \brief Closes the span
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 		The span
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 			closeSpan(LTPSpan* span);

/**
 * \par Function Name:
 *      deleteSpan
 *
 * \brief Clsoes and removes the span
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  nodeNumber 		The node number
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 			deleteSpan(unsigned long long nodeNumber);

/**
 * \par Function Name:
 *      addSpan
 *
 * \brief Creates the span
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return LTPSpan*
 *
 * \retval The new span created
 *
 * \param  nodeNumber 				The node number
 * \param  maxTxConcurrentSessions	The max number of TX concurrent sessions
 * \param  maxRxConcurrentSessions	The max number of RX concurrent sessions
 * \param  payloadSegmentSize		The payload segment size
 * \param  remoteDelay				The remote delay
 * \param  ipAddress				The destination IP address
 * \param  portNumber				The destination port number
 * \param  maxBurstSize 			The max size of burst
 * \param  sessionColor				The session color
 * \param  lowerProtocol			The lower protocol to use
 * \param  lowerProtocolInitString	The lower protocol init string
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
LTPSpan*		addSpan(unsigned long long nodeNumber, unsigned int maxTxConcurrentSessions, unsigned int maxRxConcurrentSessions, unsigned int payloadSegmentSize, unsigned int remoteDelay, long unsigned int maxBurstSize, LTPSessionColor sessionColor, const char* lowerProtocol, const char* lowerProtocolInitString);

/**
 * \par Function Name:
 *      getSpanFromNodeNumber
 *
 * \brief Gets the span from a node number
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return LTPSpan*
 *
 * \retval The span or NULL if not found
 *
 * \param  nodeNumber 		The node number
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
LTPSpan* 		getSpanFromNodeNumber(unsigned long long nodeNumber);

/**
 * \par Function Name:
 *      waitUntillSpanIsActive
 *
 * \brief Waits untill span is active
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 		The span
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void			waitUntilSpanOn(LTPSpan* span);

/**
 * \par Function Name:
 *      waitUntillCanSend
 *
 * \brief Waits untill can send
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 		The span
 * \param  bytesToSend	The amount of data to send
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 			waitUntilAllowedByCongCtrl(LTPSpan* span, unsigned int bytesToSend);

/**
 * \par Function Name:
 *      addContactToSpan
 *
 * \brief Adds the contact to the span
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  contact 		The contact
 * \param  span 		The span
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 			addContactToSpan(Contact* contact, LTPSpan* span);

/**
 * \par Function Name:
 *      removeContactFromSpan
 *
 * \brief Removes the contact from the span
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  contact 		The contact
 * \param  span			The span
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 			removeContactFromSpan(Contact* contact, LTPSpan* span);

/**
 * \par Function Name:
 *      removeAllContactsFromSpan
 *
 * \brief Removes all contacts from the span
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 		The span
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 			removeAllContactsFromSpan(LTPSpan* span);

/**
 * \par Function Name:
 *      addRangeToSpan
 *
 * \brief Adds a range to the span
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  range 		The range
 * \param  span			The span
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 			addRangeToSpan(Range* range, LTPSpan* span);

/**
 * \par Function Name:
 *      removeRangeFromSpan
 *
 * \brief Removes range from span
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  range 		The range
 * \param  span 		The span
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 			removeRangeFromSpan(Range* range, LTPSpan* span);

/**
 * \par Function Name:
 *      removeAllRangesFromSpan
 *
 * \brief Removes all ranges from span
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 	The span
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 			removeAllRangesFromSpan(LTPSpan* span);

int findSpanFromTXSessionID(void* _sessionID, size_t sessionIDSize, void* _span, size_t spanSize);
int findSpanFromTerminatedTXSessionID(void* _sessionID, size_t sessionIDSize, void* _span, size_t spanSize);
void setFixedRto(unsigned int _fixed_rto);
unsigned int getFixedRto();

#endif /* SRC_LTPSPAN_SPAN_H_ */
