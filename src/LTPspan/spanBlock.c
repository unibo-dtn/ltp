/*
 * spanBlock.c
 *
 *  Created on: 27 dic 2022
 *      Author: carlo
 */

#include <stdio.h>
#include "spanBlock.h"
#include "../adapters/protocol/upperProtocol.h"
#include "../logger/logger.h"
#include "../timer/timer.h"
#include "../config.h"
#include "span.h"
#include "spanHandlers.h"
#include "../sender/sendNotif.h"
#include "../generic/generic.h"
#include "../sender/sender.h"
#include "../receiver/receiver.h" //for mutex lockThread

void handleNewBlockToSend(LTPSpan* span) {
// qua deve essere fatta l'associazione tra QoS BP e QoS LTP. Ora l'unico QoS ltp che abbiamo è colore.
	UpperProtocolDataToSend dataToSend;
	dataToSend.buffer = NULL;
	dataToSend.bufferLength = 0;
	dataToSend.dataNumber = 0;
	dataToSend.handlerSessionTerminated = NULL;
	//dataToSend.isUnreliableBundle = false;
//Nota che "isUnreliable" è duplicato; è sia in dataToSend che dataPassedFromUpperProtcol
	passedDataFromUpperProtocol(span->nodeNumber, &dataToSend, &span->dataPassedFromUpperProtocol);
	LTPSessionColor sessionColor;
	int msLeft;
	//Calculate the remaining bundle lifetime in ms;
	msLeft = 1000*dataToSend.metadata.deadline.tv_sec-nowInMillis();
	if (msLeft<0) msLeft=0;

	if ( dataToSend.buffer != NULL){// && (msLeft > span->rtoTime)) {
		if (span->color == Green) {
			/*If span->color is Green, we could have a unidirectional link;
				for safety, we do not want to override this setting */
			sessionColor = span->color;
		}

		else { //Otherwise sessionColor can be overridden by metadata settings
			if(dataToSend.metadata.ecos.fields.bestEffort && !dataToSend.metadata.ecos.fields.reliable)
			{
				doLog("Span %d: Unreliable bundle on non-green span",span->nodeNumber);
				sessionColor = Green;
			}
			else if(!dataToSend.metadata.ecos.fields.bestEffort && dataToSend.metadata.ecos.fields.reliable)
			{
				sessionColor = Red;
			}
			else if(dataToSend.metadata.ecos.fields.bestEffort && dataToSend.metadata.ecos.fields.reliable
					&& dataToSend.metadata.reTx_counter==0){
				doLog("Span %d: Orange on first bundle Tx attempt",span->nodeNumber);
				sessionColor = Orange;
			}
			else if(dataToSend.metadata.ecos.fields.bestEffort && dataToSend.metadata.ecos.fields.reliable
					&& dataToSend.metadata.reTx_counter>0){
				doLog("Span %d: Red on bundle reTx number %d:",span->nodeNumber, dataToSend.metadata.reTx_counter);
				sessionColor = Red;
			}
			else { //default case: keep the span color
				sessionColor = span->color;
			}
		}
		createTxSession(span, dataToSend.buffer, dataToSend.bufferLength, dataToSend.dataNumber, dataToSend.handlerSessionTerminated, sessionColor, msLeft );
	}
}

void createTxSession(LTPSpan* span, void* data, int dataLength, unsigned int dataNumber, void (*handlerSessionTerminated)(unsigned int, LTPSessionResultCode), LTPSessionColor sessionColor, int redSessionTime) {
//CCaini: it is a wrapper of initializeSession used only for Tx sessions
//initializeSession initializes variables of either Tx or RX sessions

	static unsigned int currentSessionNumber = 1;
	LTPSession newSession;
	unsigned long long myNodeNumber=getMyNodeNumber();

	initializeSession(&newSession, span, myNodeNumber, currentSessionNumber, sessionColor, TX_SESSION);
	currentSessionNumber++; if (currentSessionNumber == 0) currentSessionNumber++; // To avoid using 0

	// Set block data
	newSession.block->data = data;
	newSession.block->blockLength = dataLength;

	newSession.TxSession.dataNumber = dataNumber;
	newSession.TxSession.handlerSessionTerminated = handlerSessionTerminated;

	// Set destination
	newSession.TxSession.destination = span->nodeNumber;

	LTPSegmentTypeCode firstSegments;
	LTPSegmentTypeCode lastSegment;
	char* colorString;

	switch( sessionColor ) {
	case Red:
		firstSegments = LTP_Red_Data;
		lastSegment = LTP_Red_Data_CP_EORP_EOB;
		colorString = "red";
		break;

	case Green:
		firstSegments = LTP_Green_Data;
		lastSegment = LTP_Green_Data_EOB;
		colorString = "green";
		break;

	case Orange:
		firstSegments = LTP_Orange_Data;
		lastSegment = LTP_Orange_Data_EOB;
		colorString = "orange";
		break;

	default:
		fprintf(stderr, "Color not found in creating new send session\n");
		return;
	}

	LTPSession* session = (LTPSession*) list_append(&(span->activeTxSessions), &newSession, sizeof(LTPSession));

	doLog("Session (E%d,#%d):\tCreated (%s TX)", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, colorString);

	if(sessionColor == Red){ //Start red Tx session timer and save timerID in session->TxSession.timerCancelTxSession
		if (MAX_TX_RED_SESSION_TIME<redSessionTime) redSessionTime=MAX_TX_RED_SESSION_TIME;
		Notification* CS_notif;
		CS_notif=(Notification*) mallocWrapper(sizeof(Notification));
		*CS_notif=createNotif(CANCEL_RED_TX_SESSION, session->sessionID, 0, SESSION_TIMEOUT, session->span->nodeNumber);//Non passo il puntatore a session
		session->TxSession.timerCancelTXSession = startTimer(redSessionTime, 1, timerHandlerSessionTimeoutNotif , CS_notif);
	}
	pthread_mutex_unlock(&lockThreads);
//	printf("lockThread unblocked by create New Tx session\n");
	fflush (stdout);

	sendSegmentsInRange(session, span, 0, session->block->blockLength, 0, firstSegments, lastSegment);
    pthread_mutex_lock(&lockThreads);
//	printf("lockThread blocked by create New Tx session\n");
	fflush (stdout);

	doLog("Session (E%d,#%d):\tData block sent", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);

	if ( sessionColor == Orange ) {

		destroyBlock(session);
		doLog("Session (E%d,#%d):\tTx buffer freed", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		//Start Tx session timeout ( max 1 RTO to receive the PN or NN notification)
		session->TxSession.timerOrangeNotification = startTimer(session->span->rtoTime, 1, handlerTxSessionTimeout, session); //start notification timer ( max 1 rto)
		// To do for other BP implementation: Initial Transmission Completion (ORANGE) (RFC 5326 7.7)
	}
	else if ( sessionColor == Green ) {
		doLog("Session (E%d,#%d):\tSignaling SUCCESS to upper protocol (green session)", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		if ( session->TxSession.handlerSessionTerminated != NULL )
			session->TxSession.handlerSessionTerminated(session->TxSession.dataNumber, SessionResultSuccess); // Signal the upper protocol this session is completed correctly (RFC 5326 7.4)
		cleanAndCloseSession(session);
	}
}

void sendSegmentsInRange(LTPSession* session, LTPSpan* span, unsigned int rangeFrom, unsigned int rangeTo, unsigned int reportSerialNumber, LTPSegmentTypeCode normalType, LTPSegmentTypeCode lastType) {
	unsigned int oldOffset = rangeFrom;
	unsigned int currentSegmentPayloadSize;
	int numSegmentSent=0;
//	pthread_mutex_unlock(&lockThreads);
	for (unsigned int toSend = (rangeTo-rangeFrom); toSend > 0; toSend -= currentSegmentPayloadSize, oldOffset += currentSegmentPayloadSize) {

		//CCaini la condizione sullo span diverso da NULL non ha senso scritta così. Se è NULL vado avanti?
		//Chiaramente devo rilasciare il mutex generale prima di questo check.

		if ( session->span != NULL ) { // If the span is stopped (last contact stopped) wait until it becomes available again (new contact starts)
			waitUntilSpanOn(session->span); //CCaini Molto brutto che si prenda lo span dalla session
		}

		bool isLastSegment = false;
		if ( toSend <= span->payloadSegmentSize ) { // residual data to send fit in one segment
			currentSegmentPayloadSize = toSend; //the payload dimension of the last segment is equal to the residual data
			isLastSegment = true;
		}
		else {
			currentSegmentPayloadSize = session->span->payloadSegmentSize; //the dimension of segments before the last is always
			//the same (it is specified in span.json)
		}

		LTPSegmentTypeCode segmentType = isLastSegment ? lastType : normalType;

		generateDataSegmentB(session, span, oldOffset, currentSegmentPayloadSize, reportSerialNumber, segmentType);
		numSegmentSent+=1;
	}//end for
	doLog("Session (E%d,#%d):\tNum. segments sent %d", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, numSegmentSent);
	flushFileLogger();
	//	pthread_mutex_lock(&lockThreads);
}

//ex _generateSegmentToSend
void generateDataSegmentB(LTPSession* session, LTPSpan* span, unsigned int offset, unsigned int length, unsigned int reportSerialNumber, LTPSegmentTypeCode type) {
	//It generates a data segment of whatever color (types 0-7). If red, it can be flagged as a CP (types 1,2,3) and it requires special treatment.
	LTPSegment segmentToSend;
	ResendStruct* resendStruct;
	ResendStruct* addedResendStruct;
	Notification* notif1;
	Notification* notif2;
	TimerID timerID;
    unsigned int checkpointNumber=session->TxSession.lastCPnumber;

	//segmentToSend building: header
	segmentToSend.header.typeFlag = type;
	segmentToSend.header.headerExtensionsCount = segmentToSend.header.trailerExtensionsCount = 0;
	segmentToSend.header.sessionID = session->sessionID; //Qui passa un valore. Deve avere session.

	//segmentToSend building: dataSegment specific fields CP and RS sequence numbers
	segmentToSend.dataSegment.clientServiceID =	session->clientServiceID;
	if (type==LTP_Red_Data_CP || type==LTP_Red_Data_CP_EORP || type== LTP_Red_Data_CP_EORP_EOB) {
		checkpointNumber++;
		if (checkpointNumber==0) checkpointNumber++; // To avoid using 0
		segmentToSend.dataSegment.checkpointSerialNumber = checkpointNumber;
		segmentToSend.dataSegment.reportSerialNumber = reportSerialNumber;//CCaini. It should present only if CP is in response to an RS.
	}
	//segmentToSend building: dataSegment specific fields: data offset, length and data
	segmentToSend.dataSegment.offset = offset;
	segmentToSend.dataSegment.length = length;
	segmentToSend.dataSegment.data = session->block->data + offset;//CCaini qui penso passi un puntatore; la variabile puntatata da session deve esistere


	//CP further processing
	if ( isCP(segmentToSend)) {
		pthread_mutex_lock(&lockThreads);
//		printf("lockThread blocked by span generateDataSegmentB\n");
		fflush (stdout);
		/*CCaini the resendStruct has been resumed because we need the timerID to stop the reTx timer (timerID)
		 * when a CP,RS,CS,CR is confirmed
		 * The only field to be used should be the timerID and maybe the number of sendings but only for prints.
		 */
		resendStruct=(ResendStruct*) mallocWrapper(sizeof(ResendStruct));
//		resendStruct->session = session;
		resendStruct->numberOfSendings = 0;
		resendStruct->timerID = 0;
		resendStruct->segment=mallocWrapper(sizeof(LTPSegment));
		*resendStruct->segment=segmentToSend;

		//add a copy of resendStruct to the list //CCaini devo fare una free?
		addedResendStruct = list_push_front(&(session->TxSession.CPWaitingForRS), resendStruct, sizeof(ResendStruct));
		FREE(resendStruct);//CCaini to_be_verified

		//Create a notification and start a timer that will send, when fires, this notif in input to span thread

		notif1=(Notification*) mallocWrapper(sizeof(Notification));
		*notif1=createNotif(RESEND_CP, session->sessionID, checkpointNumber, DUMMY, span->nodeNumber);
		int dummy=0;
		notif2=(Notification*) mallocWrapper(sizeof(Notification));
		*notif2=createNotif(CANCEL_RED_TX_SESSION, session->sessionID, dummy, RXMTCYCEXC, span->nodeNumber);

		int interval=session->span->rtoTime;//now in ms
		int max_retx=MAX_RETX_NUMBER;

		if (DISTRIBUTED_COPIES) {
			interval=interval/CP_COPIES;
			max_retx=max_retx*CP_COPIES;
		}

		timerID=startTimerSpecificFor_CP_RS_CS_CR (interval, max_retx, timerHandlerForwardNotif2, notif1, notif2);

		//Add Timer ID to copy in the list
		addedResendStruct->timerID=timerID;
		//Add the timer to the session timers

		pthread_mutex_lock(&(session->RTXTimersLock));
		list_append(&(session->RTXTimers), &timerID, sizeof(timerID));
		pthread_mutex_unlock(&(session->RTXTimersLock));

		session->TxSession.lastCPnumber=checkpointNumber;

		int burstLength=(DISTRIBUTED_COPIES ? 1: CP_COPIES);
		for (int i = 1; i <= burstLength; i++) {
			new_sendSegment(&segmentToSend, span, false);//false means no pacing
		}

		//Log
		if (segmentToSend.dataSegment.reportSerialNumber > 0 ) {
			doLog("Session (E%d,#%d):\tCP %u sent in response to RS %d. Waiting for RS", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, segmentToSend.dataSegment.checkpointSerialNumber, segmentToSend.dataSegment.reportSerialNumber);
		} else {
			doLog("Session (E%d,#%d):\tCP %u sent, waiting for RS", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, segmentToSend.dataSegment.checkpointSerialNumber);
		}
		pthread_mutex_unlock(&lockThreads);
//		printf("lockThread unblocked by span generateDataSegmentB\n");
		fflush (stdout);

	}
	else if (type==LTP_Orange_Data_EOB) {
		for (int i = 1; i <= ORANGE_EOB_COPIES; i++) {
			new_sendSegment(&segmentToSend, span, false);//false means no pacing
		}
	}
	else if (type==LTP_Green_Data_EOB) {
		for (int i = 1; i <= GREEN_EOB_COPIES; i++) {
			new_sendSegment(&segmentToSend, span, false);//false means no pacing
		}
	}
	else {//Pure data segment
//		new_sendSegment(&segmentToSend, span, true);//Pure data segments are paced

		new_sendSegment(&segmentToSend, span, true);//Pure data segments are paced
	}
	return;
}

