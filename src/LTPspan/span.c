/** \file span.c
 *
 * \brief This file contains the implementations of the LTP span functions
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 * \authors Davide Filoni, davide.filoni2@studio.unibo.it
 * \authors Chiara Cippitelli, chiara.cippitelli@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <stdint.h>
#include <pthread.h>
#include <sys/eventfd.h>
#include <sys/select.h>

#include "span.h"

#include "../logger/logger.h"
#include "../list/list.h"
#include "../LTPsegment/segment.h"
#include "../LTPsession/sessionStruct.h"
#include "../LTPsession/session.h"
#include "../generic/generic.h"
#include "../adapters/protocol/lowerProtocol.h"
#include "../adapters/protocol/upperProtocol.h"
#include "../adapters/protocol/upperProtocolReceivedData.h"
#include "../config.h"
#include "../timer/timer.h"
#include "../timer/timerStruct.h"
#include "../contact/contact.h"
#include "../range/range.h"
#include "../sender/sender.h"
#include "../receiver/rcvHandlers.h"
#include "spanHandlers.h"
#include "../sender/sendNotif.h"
#include "spanBlock.h"
#include "spanNotifications.h"
#include "../receiver/receiver.h" //CCaini to share pthread_mutex_t lockThreads;

List activeSpans = empty_list; //made external by CCaini

static unsigned int fixed_rto;

static unsigned int ownqtime;


/***** PRIVATE FUNCTIONS *****/
static void _checkSpanStatus(TimerID timerID, void* _span);
static bool _contactIsInThePast(void* data, size_t size);
static bool _rangeIsInThePast(void* data, size_t size);
static void _freezeAllSessions(void* span);
static void _resumeAllSessions(void* span);
static void _destroyAllSessions(void* _span);
static void* _mainCongestionControl(void* _span);
static void* _mainGetterDataFromUpperProtocol(void* _span);
static void* _mainSpanThread(void* _span);


/***** HANDLER FUNCTIONS *****/
static int _compareContacts(void* _newContact, size_t newContactSize, void* _contact, size_t contactSize);
static int _compareRanges(void* _newRange, size_t newRangeSize, void* _range, size_t rangeSize);
static void _handlerCheckForTerminatedSession(TimerID timerID, void* _span);
static bool _checkIsToRemoveTerminatedSession(void* data, size_t size);
static void _cleanSession(void *session, size_t size);
static void _closeSpan(void *span, size_t size);
/***** COMPARE FUNCTIONS *****/
static int _findActiveSpanFromNodeNumber(void* _nodeNumber, size_t nodeNumberSize, void* _span, size_t spanSize);
static int _findActiveTXSessionFromSessionID(void* _sessionID, size_t sessionIDSize, void* _session, size_t sessionSize);
static int _findTerminatedTXSessionFromSessionID(void* _sessionID1, size_t sessionID1Size, void* _sessionID2, size_t sessionID2Size);


static void* _mainSpanThread(void* _span) {
	int result;
	LTPSpan* span = (LTPSpan*) _span;
	uint64_t numberOfRead;
	fd_set fdset;
	fd_set fdstruct; // cache the value to avoid to re-calculate every time

	struct timeval timeout = {1,0}; // seconds of timeout to close with ctrl+c

	// Prepare the select struct
	FD_ZERO(&fdstruct);
	FD_SET(span->receivedSignalSegmentEventFD, &fdstruct);
	FD_SET(span->receivedNotifEventFD, &fdstruct);
	FD_SET(span->newBlockEventFD, &fdstruct);

	// da mettere const
	int max = max(span->receivedSignalSegmentEventFD, max(span->receivedNotifEventFD,span->newBlockEventFD));
		while ( !span->isRemoved ) {

		waitUntilSpanOn(span); //Wait until a contact to the destination node of this span is on. It will be
		//to be checked again for non "atomic" processing
		if ( span->isRemoved  ) break;

		fdset = fdstruct; //CCaini Perché dentro al ciclo questa e quella sotto?
		struct timeval timeout = {1,0}; // seconds of timeout to close with ctrl+c
		select(max+1, &fdset, NULL, NULL, &timeout);


		if ( FD_ISSET(span->receivedNotifEventFD, &fdset) ) { //
			//NOTE: this will be executed first. Each notification handling is atomic and fast
//			sleep(10);//CCaini temp to debug
			result=read(span->receivedNotifEventFD, &numberOfRead, sizeof(numberOfRead)); // read to remove the element from the queue. The number read is the number of queued elements
			if (result<0) {
				doLog("Span %d:\t error in function read", span->nodeNumber);
				exit (-1);
			}
			for (int i = 1; i <= numberOfRead; i++) {
				//				handleLocalSignalSegmentToSend(span);//notifications: locally generated signalingSegments (RS,CP,CS,CR,NN), plus session closing.
				pthread_mutex_lock(&lockThreads);
				handleNotification(span);//session is checked before notification processing; "atomic" functions: the mutex is never unlocked.
				pthread_mutex_unlock(&lockThreads);
			}
		}
		else if ( FD_ISSET(span->receivedSignalSegmentEventFD, &fdset) ) { // signaling segments that need a response (passed by receiver thread)
			//NOTE: this will be executed only if there are not notifications available!
			//handling of received signal segments are generally atomic and fast but that of RS, which may imply the reTx of data segments.
			result=read(span->receivedSignalSegmentEventFD, &numberOfRead, sizeof(numberOfRead)); // read to remove the element from the queue. The number read is the number of queued elements
			if (result<0) {
				doLog("Span %d:\t error in function read", span->nodeNumber);
				exit (-1);
			}
			for (int i = 1; i <= numberOfRead; i++) {
				printf("lockThread blocked by mainSpanThread receivedSignalSegment \n");
				fflush (stdout);
				pthread_mutex_lock(&lockThreads);
				handleSignalSegmentToRespond(span); // signaling segments that need a response (CPs, OEOB, RS, CS, CR);
				//Session is checked before processing. All atomic functions but spanHandlerRS, in which the mutex is
				//unlocked and locked to cope with span stopping and/or congestion control delay.
				pthread_mutex_unlock(&lockThreads);
//				printf("lockThread unblocked by mainSpanThread receivedSignalSegment \n");
				fflush (stdout);
			}
		}
		else if ( FD_ISSET(span->newBlockEventFD, &fdset) ) { // dataFromUpperProtocol available.
			//NOTE: this will be executed only if there are neither notifications nor signal segments available!
			//The Tx of new blocks is always time consuming. CCaini We must avoid tranmitting more than one new block at a time, not to postpone processing of
			//notifications and signal segments.
			result=read(span->newBlockEventFD, &numberOfRead, sizeof(numberOfRead)); // read to remove the element from the queue. The number read is the number of queued elements
			if (result<0) {
				doLog("Span %d:\t error in function read", span->nodeNumber);
				exit (-1);
			}
			for (int i = 1; i <= numberOfRead; i++) {
				pthread_mutex_lock(&lockThreads);
				handleNewBlockToSend(span);// Non atomic because of possible span stopping and/or congestion control delay:
				//the mutex is unlocked and locked inside
				pthread_mutex_unlock(&lockThreads);
			}
		}

}

	doLog("Span %d:\tspanThread ended", span->nodeNumber);
	return NULL;
}


void setOwnqTime(unsigned int _ownqtime) {
	ownqtime = _ownqtime;
}

unsigned int getOwnqTime() {
	return ownqtime;
}

LTPSpan* getSpanFromNodeNumber(unsigned long long nodeNumber) {
	LTPSpan* span = (LTPSpan*) list_get_pointer_data(activeSpans, &nodeNumber, sizeof(nodeNumber), _findActiveSpanFromNodeNumber);
	return span;
}

void closeAllSpans() {
	list_for_each(activeSpans, _closeSpan);
	list_destroy(&activeSpans);
}

void closeSpan(LTPSpan* span) {
	if ( span == NULL ) return;

	doLog("Span %d:\tClosing...", span->nodeNumber);

	pthread_mutex_lock(&(span->lock));


	span->isRemoved = true; // Span removed

	stopTimer(span->timerCleanerTerminatedSessions); // Stop timer for cleaning

	// Cancel the threads
	pthread_cancel(span->mainThread);
	pthread_cancel(span->upperProtocolThread);
	pthread_cancel(span->congestionControlThread);

    destroyUpperProtocolReceivedData(&span->dataPassedFromUpperProtocol);

	// Close the FD used for signaling
	close(span->newBlockEventFD);
	close(span->receivedSignalSegmentEventFD);
	close(span->receivedNotifEventFD);

	//destroy the list
	list_destroy(&(span->structsToSendBySpanThread));
	list_destroy(&(span->receivedSignalSegmentsForSpanThread));
	list_destroy(&(span->notifToSpanThread));

	// Destroy the condition variable
	pthread_cond_destroy(&(span->stoppedCondition));
	pthread_cond_destroy(&(span->waitingForCongestionControlCondition));

	sem_destroy(&span->freeTxConcurrentSessionsSemaphore);

	stopTimer(span->contactPlanUpdateTimerID); // Stop timer for changeStatus event on contacts & ranges
	list_destroy(&(span->contactsList)); // Destroy contact list
	list_destroy(&(span->rangesListOut)); // Destroy range list
	list_destroy(&(span->rangesListIn)); // Destroy range list

	// Remove active RX sessions
	list_for_each(span->activeRxSessions, _cleanSession);
	list_destroy(&(span->activeRxSessions));

	// Remove active TX sessions
	list_for_each(span->activeTxSessions, _cleanSession);
	list_destroy(&(span->activeTxSessions));

	// Remove terminated RX sessions
	list_destroy(&(span->recentlyClosedRxSessionIDs));

	// Remove terminated TX sessions
	list_destroy(&(span->recentlyClosedTxSessionIDs));

	FREE(span->lowerProtocol);

	pthread_mutex_destroy(&(span->lock));
}

void deleteSpan(unsigned long long nodeNumber) {
	LTPSpan* span = getSpanFromNodeNumber(nodeNumber);
	closeSpan(span);
	list_remove_data(&activeSpans, &nodeNumber, sizeof(nodeNumber), _findActiveSpanFromNodeNumber);
}

LTPSpan* addSpan(unsigned long long nodeNumber, unsigned int maxTxConcurrentSessions, unsigned int maxRxConcurrentSessions, unsigned int payloadSegmentSize, unsigned int remoteDelay, long unsigned int maxBurstSize, LTPSessionColor sessionColor, const char* lowerProtocol, const char* lowerProtocolInitString) {
	LTPSpan span;
	// Set new span data
	span.nodeNumber = nodeNumber;
	span.maxTxConcurrentSessions = maxTxConcurrentSessions;
	span.maxRxConcurrentSessions = maxRxConcurrentSessions;
	span.payloadSegmentSize = payloadSegmentSize;
	span.currentMaxBurstSize = span.maxBurstSize = maxBurstSize;//maxBurstSize is the value read by the Json file for the current span
	span.color = sessionColor;

	span.lowerProtocol = mallocWrapper(strlen(lowerProtocol) + 1);
	memcpy(span.lowerProtocol, lowerProtocol, strlen(lowerProtocol) + 1);

	memset(&span.address, 0, sizeof(destination_struct));
	if ( !initOutLowerLevel(lowerProtocol, nodeNumber, lowerProtocolInitString, &span.address) ) {
		FREE(span.lowerProtocol);
		return NULL;
	}

	span.currentRxBufferUsed = 0;
	sem_init(&span.freeTxConcurrentSessionsSemaphore, 0, maxTxConcurrentSessions);

	span.remoteDelay = remoteDelay;
	if (fixed_rto>0) span.rtoTime = fixed_rto;
	else span.rtoTime = 1000*(ownqtime + remoteDelay);//CCaini rtoTime now in ms to enable RTO<1s

	span.xmitRate = 0;
	span.bytesAllowed = 0;
	pthread_cond_init(&span.waitingForCongestionControlCondition, NULL);

    bool isReliableLink = span.color != Green;

	initUpperProtocolReceivedData(&span.dataPassedFromUpperProtocol, nodeNumber, isReliableLink);//Added nodeNumber and isReliableLink by Persampieri

	// Set defaults & init components

	span.contactsList = empty_list;
	span.rangesListOut = empty_list;
	span.rangesListIn = empty_list;
	span.contactPlanUpdateTimerID = 0;

	span.activeRxSessions = empty_list;
	span.activeTxSessions = empty_list;

	span.recentlyClosedRxSessionIDs = empty_list;
	span.recentlyClosedTxSessionIDs = empty_list;

	span.receivedSignalSegmentsForSpanThread = empty_list;
	span.newBlockEventFD = eventfd(0, 0);

	span.notifToSpanThread = empty_list;
	span.structsToSendBySpanThread = empty_list;
	span.receivedNotifEventFD= eventfd(0, 0);

	span.receivedSignalSegmentEventFD = eventfd(0, 0);

	pthread_mutexattr_t ma;
	pthread_mutexattr_init(&ma);
	pthread_mutexattr_settype(&ma, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&(span.lock), &ma);


	span.isRemoved = false;

	span.isStopped = true;
	pthread_cond_init(&span.stoppedCondition, NULL);

	//TODO _destroyAllSession: check if only tx session should be destroyed instead -> purgeTXSession (see ltprpc man, add span quein latency)

	#if DELETE_SESSION_WHEN_SPAN_PAUSED
		span.Functions.handlerSpanPaused = _destroyAllSessions;
		span.Functions.handlerSpanResumed = NULL;
 	#else
		span.Functions.handlerSpanPaused = _freezeAllSessions;
		span.Functions.handlerSpanResumed = _resumeAllSessions;
	#endif

	// Append span to activeSpan list & save new pointer
	LTPSpan* newSpan = (LTPSpan*) list_append(&activeSpans, &span, sizeof(span));

	// Start span threads

	pthread_attr_t pthreadAttribute;
	pthread_attr_init(&pthreadAttribute);
	pthread_attr_setdetachstate(&pthreadAttribute, PTHREAD_CREATE_DETACHED);

	doLog("Span %d:\tCreating...", nodeNumber);

	pthread_create(&(newSpan->mainThread), &pthreadAttribute, _mainSpanThread, newSpan);
	pthread_create(&(newSpan->upperProtocolThread), &pthreadAttribute, _mainGetterDataFromUpperProtocol, newSpan);
	pthread_create(&(newSpan->congestionControlThread), &pthreadAttribute, _mainCongestionControl, newSpan);

	// Start timer for cleaning terminated sessions
	int cycles=0; //0 means periodic timer
	newSpan->timerCleanerTerminatedSessions = startTimer(1000*RECENTLY_CLOSED_LIST_UPDATE_TIME, cycles, _handlerCheckForTerminatedSession, newSpan);

	return newSpan;
}

void addContactToSpan(Contact* contact, LTPSpan* span) { // It adds only contacts to the wanted node (span). Example: contact 1 -> 2 is added only if the current node is 1 and the span is to 2
	if ( contact == NULL || span == NULL ) return;

	if ( contact->fromNode == getMyNodeNumber() && contact->toNode == span->nodeNumber ) {
		pthread_mutex_lock(&span->lock);
		list_push_ordered(&(span->contactsList), contact, sizeof(Contact), _compareContacts);
		pthread_mutex_unlock(&span->lock);
		_checkSpanStatus(0, span);
	}
}

void removeContactFromSpan(Contact* contact, LTPSpan* span) {
	if ( contact == NULL || span == NULL ) return;

	pthread_mutex_lock(&span->lock);
	list_remove_data(&(span->contactsList), contact, sizeof(Contact), _compareContacts);
	pthread_mutex_unlock(&span->lock);
	_checkSpanStatus(0, span);
}

void removeAllContactsFromSpan(LTPSpan* span) {
	if ( span == NULL ) return;

	pthread_mutex_lock(&span->lock);
	list_destroy(&(span->contactsList));
	pthread_mutex_unlock(&span->lock);
	_checkSpanStatus(0, span);
}

void addRangeToSpan(Range* range, LTPSpan* span) {
	if ( range == NULL || span == NULL ) return;

	pthread_mutex_lock(&span->lock);
	if ( range->fromNode == span->nodeNumber && range->toNode == getMyNodeNumber() )
		list_push_ordered(&(span->rangesListOut), range, sizeof(Range), _compareRanges);
	if ( range->fromNode == getMyNodeNumber() && range->toNode == span->nodeNumber )
		list_push_ordered(&(span->rangesListIn), range, sizeof(Range), _compareRanges);
	pthread_mutex_unlock(&span->lock);
	_checkSpanStatus(0, span);
}

void removeRangeFromSpan(Range* range, LTPSpan* span) {
	if ( range == NULL || span == NULL ) return;

	pthread_mutex_lock(&span->lock);
	if ( range->fromNode == span->nodeNumber )
		list_remove_data(&(span->rangesListOut), range, sizeof(Range), _compareRanges);
	if ( range->toNode == span->nodeNumber )
		list_remove_data(&(span->rangesListIn), range, sizeof(Range), _compareRanges);
	pthread_mutex_unlock(&span->lock);
	_checkSpanStatus(0, span);
}

void removeAllRangesFromSpan(LTPSpan* span) {
	if ( span == NULL ) return;

	pthread_mutex_lock(&span->lock);
	list_destroy(&(span->rangesListOut));
	list_destroy(&(span->rangesListIn));
	pthread_mutex_unlock(&span->lock);
	_checkSpanStatus(0, span);
}

void waitUntilSpanOn(LTPSpan* span) {
	pthread_mutex_lock(&(span->lock));
	while ( span->isStopped ) {
		pthread_cond_wait(&(span->stoppedCondition), &(span->lock));
	}
	pthread_cond_signal(&(span->stoppedCondition));
	pthread_mutex_unlock(&(span->lock));
}

void waitUntilAllowedByCongCtrl(LTPSpan* span, unsigned int bytesToSend) {
	if ( span==NULL ) return;

	pthread_mutex_lock(&(span->lock));
	while ( bytesToSend > span->bytesAllowed ) {
	doLog("Span X:\t residual Bytes allowed by CC are less than bytesToSend %lu", span->bytesAllowed);
		pthread_cond_wait(&(span->waitingForCongestionControlCondition), &(span->lock));
	}
	span->bytesAllowed -= bytesToSend;


	pthread_cond_signal(&(span->waitingForCongestionControlCondition));
	pthread_mutex_unlock(&(span->lock));
}

void setFixedRto(unsigned int _fixed_rto) {
	fixed_rto = _fixed_rto;
}

unsigned int getFixedRto() {
	return fixed_rto;
}

/***** PRIVATE FUNCTIONS *****/

static inline int _compareContacts(void* _newContact, size_t newContactSize, void* _contact, size_t contactSize) {
	return compareContacts((Contact*) _newContact, (Contact*)_contact);
}

static inline int _compareRanges(void* _newRange, size_t newRangeSize, void* _range, size_t rangeSize) {
	return compareRanges((Range*) _newRange, (Range*)_range);
}

static void _checkSpanStatus(TimerID timerID, void* _span) {
	LTPSpan* span = (LTPSpan*) _span;
	if ( span == NULL ) return;

	pthread_mutex_lock(&(span->lock));

	stopTimer(span->contactPlanUpdateTimerID); // Stop the timer

	// Remove all old contacts
	list_remove_if(&(span->contactsList), _contactIsInThePast);

	// Remove all old ranges
	list_remove_if(&(span->rangesListOut), _rangeIsInThePast);
	list_remove_if(&(span->rangesListIn), _rangeIsInThePast);

	// No more contacts or ranges
	if ( span->contactsList == empty_list || span->rangesListOut == empty_list || span->rangesListIn == empty_list ) {
		span->isStopped = true;
		_freezeAllSessions(span);
		doLog("Span %d:\tInfinite pause", span->nodeNumber);
		pthread_cond_signal(&(span->stoppedCondition)); // Signal waiting threads
		pthread_mutex_unlock(&(span->lock));
		return;
	}

	// Looking for current contact & current range
	const time_t currentTime = time(NULL);

	// Now the first contact is the first to be scheduled (contacts are ordered)
	Contact* firstContact = (Contact*) span->contactsList->data;
	time_t nextEventContact;
	bool contactIsNow;
	if ( firstContact->fromTime <= currentTime && currentTime < firstContact->toTime ) { // The contact is now (currently active)
		nextEventContact = firstContact->toTime - currentTime;
		contactIsNow = true;
	} else { // The contact is in the future
		nextEventContact = firstContact->fromTime - currentTime;
		contactIsNow = false;
	}

	// Now the first range "out" is the first to be scheduled (ranges are ordered)
	Range* firstRangeOut = (Range*) span->rangesListOut->data;
	time_t nextEventRangeOut;
	bool rangeOutIsNow;
	if ( firstRangeOut->fromTime <= currentTime && currentTime < firstRangeOut->toTime ) { // The range "out" is now (currently valid)
		nextEventRangeOut = firstRangeOut->toTime - currentTime;
		rangeOutIsNow = true;
	} else { // The validity interval of the range "out" is in the future
		nextEventRangeOut = firstRangeOut->fromTime - currentTime;
		rangeOutIsNow = false;
	}

	// Now the first range "in" is the first to be scheduled (ranges are ordered)
	Range* firstRangeIn = (Range*) span->rangesListIn->data;
	time_t nextEventRangeIn;
	bool rangeInIsNow;
	if ( firstRangeIn->fromTime <= currentTime && currentTime < firstRangeIn->toTime ) { // The range "in" is now (currently valid)
		nextEventRangeIn = firstRangeIn->toTime - currentTime;
		rangeInIsNow = true;
	} else { // The validity interval of the range "in" is in the future
		nextEventRangeIn = firstRangeIn->fromTime - currentTime;
		rangeInIsNow = false;
	}

	const time_t nextEvent = min( min(nextEventRangeOut, nextEventRangeIn) , nextEventContact );

	if ( contactIsNow && rangeOutIsNow && rangeInIsNow ) { // Contact, range "in" and range "out" are all now
		if (fixed_rto==0) span->rtoTime = 1000* (ownqtime + firstRangeOut->seconds + firstRangeIn->seconds + span->remoteDelay); //now rto in ms
		span->xmitRate = firstContact->xmitRate;
		span->isStopped = false;

		span->currentMaxBurstSize = max(span->maxBurstSize, span->xmitRate * ( TOKEN_BUCKET_UPDATE_TIME / 1000.0 ));

		if ( span->currentMaxBurstSize != span->maxBurstSize ) {
			doLog("Warning:\tMax burst size increased to ul% to meet the current contact nominal Tx rate", span->currentMaxBurstSize);
		}

		if(span->Functions.handlerSpanResumed != NULL) { // It depends on the option chosen
			span->Functions.handlerSpanResumed(span);
		}
		pthread_cond_signal(&(span->stoppedCondition)); // Signal waiting threads

		doLog("Span %d:\tEnabled. Next event in %d seconds", span->nodeNumber, nextEvent);
	} else { // At least one of the 2 is in the future
		span->isStopped = true;
		span->Functions.handlerSpanPaused(span);

		doLog("Span %d:\tIn pause. Next event in %d seconds", span->nodeNumber, nextEvent);
	}

	if ( nextEvent > 0 ) // CCaini chiedere Bisacchi Restart the timer
		span->contactPlanUpdateTimerID = startTimer(1000 * nextEvent, 1, _checkSpanStatus, span);

	pthread_mutex_unlock(&(span->lock));
}

static void _freezeAllSessions(void* _span) {
	LTPSpan* span = (LTPSpan*) _span;
	// Freeze all RX sessions
	List current = span->activeRxSessions;
	while ( current != empty_list ) {
		freezeSession((LTPSession*)current->data);
		current = current->next;
	}

	// Freeze all TX sessions
	current = span->activeTxSessions;
	while ( current != empty_list ) {
		freezeSession((LTPSession*)current->data);
		current = current->next;
	}
}

static void _resumeAllSessions(void* _span) {
	LTPSpan* span = (LTPSpan*) _span;
	// Resume all RX sessions
	List current = span->activeRxSessions;
	while ( current != empty_list ) {
		resumeSession((LTPSession*)current->data);
		current = current->next;
	}

	// Resume all TX sessions
	current = span->activeTxSessions;
	while ( current != empty_list ) {
		resumeSession((LTPSession*)current->data);
		current = current->next;
	}
}

static void _destroyAllSessions(void* _span) {
	LTPSpan* span = (LTPSpan*) _span;

	List next = NULL;
	List current = span->activeRxSessions;
//	// Destroy all RX sessions
//	List current = span->activeRxSessions;
//	List next = NULL;
//	while ( current != empty_list ) {
//		next = current->next;
//		cleanAndCloseSession((LTPSession*)current->data);
//		current = next;
//	}

	// Destroy all TX sessions
	current = span->activeTxSessions;
	while ( current != empty_list ) {
		next = current->next;
		cleanAndCloseSession((LTPSession*)current->data);
		current = next;
	}
}

static bool _contactIsInThePast(void* data, size_t size) {
	Contact* contact = (Contact*) data;
	if ( contact->toTime <= time(NULL) ) {
		doLog("Info:\tRemoving contact from %d to %d starting at %d", contact->fromNode, contact->toNode, contact->fromTime);
		return true;
	} else {
		return false;
	}
}

static bool _rangeIsInThePast(void* data, size_t size) {
	Range* range = (Range*) data;
	if ( range->toTime <= time(NULL) ) {
		doLog("Info:\tRemoving range from %d to %d, starting at %d", range->fromNode, range->toNode, range ->fromTime);
		return true;
	} else {
		return false;
	}
}

static void* _mainCongestionControl(void* _span) {
	LTPSpan* span = (LTPSpan*) _span;
    long int lastUpdate;
    long int timeElapsed;

	while (!span->isRemoved ) {
			lastUpdate=nowInMillis();
			waitUntilSpanOn(span);
			usleep(1000 * TOKEN_BUCKET_UPDATE_TIME);
			pthread_mutex_lock(&(span->lock));
			timeElapsed=nowInMillis()-lastUpdate;
			unsigned int newBytesAllowed = span->xmitRate * (timeElapsed/1000.0); //CCaini modified in order to consider the real time elapsed
			span->bytesAllowed = min(span->bytesAllowed + newBytesAllowed, span->currentMaxBurstSize);
//			doLog("Span %d:\ttimeElapsed=%d %lu", span->nodeNumber, timeElapsed, span->bytesAllowed);
			pthread_cond_signal(&(span->waitingForCongestionControlCondition));
			pthread_mutex_unlock(&(span->lock));
	}

	doLog("Span %d:\tmainCongestionControl ended", span->nodeNumber);
	return NULL;
}

static void* _mainGetterDataFromUpperProtocol(void* _span) {
	int result;
	LTPSpan* span = (LTPSpan*) _span;

	while (  !span->isRemoved ) {
		waitUntilSpanOn(span);
		if ( span->isRemoved ) break;

		if ( canReceiveDataFromUpperProtocol(span->nodeNumber, &span->dataPassedFromUpperProtocol) ) {
			if ( span->isRemoved  ) break;

			sem_wait(&(span->freeTxConcurrentSessionsSemaphore));
			// Signal new data is available
			uint64_t dummy = 1; // 1 because I have 1 new data
			result=write(span->newBlockEventFD, &dummy, sizeof(dummy));
	                if (result<0) { 
				doLog("Span %d:\t _mainGetter, error in function write", span->nodeNumber);
                	        exit (-1);
                	}
		}
	}

	doLog("Span %d:\tgetterDataFromUpperProtocol ended", span->nodeNumber);

	return NULL;
}


/***** HANDLER FUNCTIONS *****/

static void _handlerCheckForTerminatedSession(TimerID timerID, void* _span) {
	LTPSpan* span = (LTPSpan*) _span;

	if ( span == NULL || span->isRemoved ) {
		return;
	}

	list_remove_if(&(span->recentlyClosedRxSessionIDs), _checkIsToRemoveTerminatedSession);
	list_remove_if(&(span->recentlyClosedTxSessionIDs), _checkIsToRemoveTerminatedSession);

//	span->timerCleanerTerminatedSessions = startTimer(1000*RECENTLY_CLOSED_LIST_UPDATE_TIME, 1, _handlerCheckForTerminatedSession, span);
}

static bool _checkIsToRemoveTerminatedSession(void* data, size_t size) {
	EndedSession* endedSession = (EndedSession*) data;
	struct timeval currentTime;
	gettimeofday(&currentTime, NULL);

	if ( timercmp(&currentTime, &(endedSession->endTime), >=) ) { // If time is over -> remove
		doLog("Session (E%d,#%d):\tRemoving the session from the recently closed session list", endedSession->sessionID.sessionOriginator, endedSession->sessionID.sessionNumber);
		return true;
	}

	return false;
}

static inline void _cleanSession(void* _session, size_t size) {
	cleanSession((LTPSession*)_session);
}

static inline void _closeSpan(void *span, size_t size) {
	closeSpan((LTPSpan*) span);
}

/***** COMPARE FUNCTIONS *****/

static int _findActiveSpanFromNodeNumber(void* _nodeNumber, size_t nodeNumberSize, void* _span, size_t spanSize) {
	unsigned long long* nodeNumber = (unsigned long long*) _nodeNumber;
	LTPSpan* span = (LTPSpan*) _span;
	if ( span->nodeNumber == *nodeNumber )
		return 0; // Equals
	else
		return -1; // Not equals
}

int findSpanFromTXSessionID(void* _sessionID, size_t sessionIDSize, void* _span, size_t spanSize) {
	LTPSessionID* sessionID = (LTPSessionID*) _sessionID;
	LTPSpan* span = (LTPSpan*) _span;
	if ( list_find(span->activeTxSessions, sessionID, sizeof(LTPSessionID), _findActiveTXSessionFromSessionID) > 0 )
		return 0; // Found
	else
		return -1; // Not found
}

static int _findActiveTXSessionFromSessionID(void* _sessionID, size_t sessionIDSize, void* _session, size_t sessionSize) {
	LTPSessionID* sessionID = (LTPSessionID*) _sessionID;
	LTPSession* session = (LTPSession*) _session;
	if ( session->sessionID.sessionNumber == sessionID->sessionNumber
		&& session->sessionID.sessionOriginator == sessionID->sessionOriginator)
		return 0; // Equals
	else
		return -1; // Not equals
}

int findSpanFromTerminatedTXSessionID(void* _sessionID, size_t sessionIDSize, void* _span, size_t spanSize) {
	LTPSessionID* sessionID = (LTPSessionID*) _sessionID;
	LTPSpan* span = (LTPSpan*) _span;
	if ( list_find(span->recentlyClosedTxSessionIDs, sessionID, sizeof(LTPSessionID), _findTerminatedTXSessionFromSessionID) > 0 )
		return 0; // Found
	else
		return -1; // Not found
}

static int _findTerminatedTXSessionFromSessionID(void* _sessionID1, size_t sessionID1Size, void* _sessionID2, size_t sessionID2Size) {
	LTPSessionID* sessionID1 = (LTPSessionID*) _sessionID1;
	LTPSessionID* sessionID2 = (LTPSessionID*) _sessionID2;
	if ( sessionID1->sessionNumber == sessionID2->sessionNumber
		&& sessionID1->sessionOriginator == sessionID2->sessionOriginator)
		return 0; // Equals
	else
		return -1; // Not equals
}
