/*
 * spanHandlers.c
 *
 *  Created on: 18 dic 2022
 *      Author: root
 */
#include <stdio.h>
#include "spanHandlers.h"
#include "../sender/sender.h"
#include "../logger/logger.h"
#include "../generic/generic.h"
#include "span.h"
#include "../config.h"
#include "../LTPsession/session.h"
#include "../adapters/protocol/upperProtocol.h"
#include "../sender/sendNotif.h"
#include "../config.h"
#include "spanHandlers.h"
#include "spanBlock.h"
#include "../receiver/receiver.h" //CCaini to share lockThreads mutex


static bool resendUnclaimedDataInRS(LTPSession* session, LTPSpan* span, LTPSegment* receivedSegment);


void handleSignalSegmentToRespond(LTPSpan* span) { // Handle signal segments passed by Receiver thread
	pthread_mutex_lock(&span->lock);
	LTPSegment* segment = (LTPSegment*) list_pop_front(&(span->receivedSignalSegmentsForSpanThread), NULL); // Remove and get first segment
	pthread_mutex_unlock(&span->lock);

	if ( segment == NULL ) {
		return; // The segment can't be NULL, it should never happen
	}

	switch (segment->header.typeFlag){
	case LTP_Red_Data_CP:
	case LTP_Red_Data_CP_EORP:
	case LTP_Red_Data_CP_EORP_EOB:
		spanHandler_CP(span, segment);
		break;
	case LTP_Orange_Data_EOB:
		spanHandler_OrangeEOB(span, segment);
		break;
	case LTP_RS:
		spanHandler_RS(span, segment);
		break;
	case LTP_CS:
		spanHandler_CS(span, segment);
		break;
	case LTP_CR:
		spanHandler_CR(span, segment);
		break;
	}
	FREE(segment);
}


/*
 * spanHandlers (For segments that requires a response once received, in type order: CP, Orange EOB, RS, CS, CR)
*/

void spanHandler_CP(LTPSpan* span, LTPSegment* receivedSegment) {//Types 1,2,3
//Mutex already Locked

	LTPSession* session;
	session_list_state_t listState;
	int result;

	listState=findRxSession(span, receivedSegment, &session);
	switch (listState) {
		case ACTIVE ://A few anamaly checks, in the normal case break to go on)
			if ( session->RxSession.state == CS_RECEIVED){
				doLog("Session (E%d,#%d):\tCP %d refers to a session that is in CS_RECEIVED state: discarded", receivedSegment->header.sessionID.sessionOriginator,
							receivedSegment->header.sessionID.sessionNumber, receivedSegment->dataSegment.checkpointSerialNumber);
				destroySegment(receivedSegment);
				return;
			}
			if ( session->RxSession.state == CR_SENT){
				doLog("Session (E%d,#%d):\tCP %d refers to a session that is in CR_SENT state: discarded", receivedSegment->header.sessionID.sessionOriginator,
							receivedSegment->header.sessionID.sessionNumber, receivedSegment->dataSegment.checkpointSerialNumber);
				destroySegment(receivedSegment);
				return;
			}

			//Check if the current CP is a duplicate. If yes, discard it.

			//CCaini Chiedee a Bisacchi perché riprocessa i CP duplicati.

			//Normal case
			break;//Normal case, go on.
		case RECENTLY_CLOSED :
			doLog("Session (E%d,#%d):\tCP %d refers to a session that has been recently closed: discarded", receivedSegment->header.sessionID.sessionOriginator,
						receivedSegment->header.sessionID.sessionNumber, receivedSegment->dataSegment.checkpointSerialNumber);
			destroySegment(receivedSegment);//free allocated memory of pointers of receivedSegment
			return;
			break;
		case UNKNOWN :
			doLog("Session (E%d,#%d):\tCP %d refers to a session that is unknown: discarded", receivedSegment->header.sessionID.sessionOriginator,
						receivedSegment->header.sessionID.sessionNumber, receivedSegment->dataSegment.checkpointSerialNumber);
			destroySegment(receivedSegment);//free the pointers of the receivedSegment structure
			return;
			break;
		}

//Normal case

	// Calculate the upper & lower bounds of the new RS to be generated in response to this CP
	unsigned int upperByteToAck = receivedSegment->dataSegment.offset + receivedSegment->dataSegment.length;
	unsigned int lowerByteToAck = 0;

	if ( receivedSegment->dataSegment.reportSerialNumber > 0 ) { // This CP is in response to an RS (it follows a group of retransmitted segments)
		// Look for RS to get upper & lower byte to ack
		int index = list_find(session->RxSession.RSWaitingForCP, &(receivedSegment->dataSegment.reportSerialNumber), sizeof(receivedSegment->dataSegment.reportSerialNumber), findReportSegmentFromCheckpoint);
		if ( index > 0 ) { // If found remove from the list (the remove return the pointer to the element removed),
			      //take from it the lower and upper bounds, then you can safely destroy and free
			LTPSegment* reportSegment = (LTPSegment*) list_remove_index_get_pointer(&(session->RxSession.RSWaitingForCP), index, NULL);
			lowerByteToAck = reportSegment->reportSegment.lowerBound;
/*
 * In the RS the scope is reported explicitly, while In the CP triggered by an RS, the scope is not reported (interess... ed Cazzat).
 * It must be recovered form the previous RS at its arrival.
 * It is fundamental to get the lower and upper bounds of the RS previously sent, to which the current CP is in response, in order
 * to know the scope of the current CP, which is essential to generate the claims only within this scope.
 * Tell CCSDS to decouple CP triggered by an RS from RS data.
 */
			upperByteToAck = reportSegment->reportSegment.upperBound;
			destroySegment(reportSegment);//"Free all the pointers" in the reportSegmant structure
			FREE(reportSegment);//Free the reportSegment structure itself
		}
	}


	// Calculate the claims & generate the RS
	bool isLastRS;
	List claims = getClaimsFromSession(session, lowerByteToAck, upperByteToAck, &isLastRS);

//	_generateRS(session, span, claims, receivedSegment->dataSegment.checkpointSerialNumber, (isLastRS ? 0 : lowerByteToAck));
	generateRSB(session, span, claims, receivedSegment->dataSegment.checkpointSerialNumber, (isLastRS ? 0 : lowerByteToAck));
	list_destroy(&claims);
}

void spanHandler_OrangeEOB(LTPSpan* span, LTPSegment* receivedSegment) {//Type 6

	LTPSession* session = list_get_pointer_data(span->activeRxSessions, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), findSessionFromSessionID); // Get the session from the active sessions
	if ( session == NULL ) {	// If the Rx session of the received EOB is not active, skip it
		doLog("Session (E%d,#%d):\tOrange EOB %d refers to a session that is no active: skipped", receivedSegment->header.sessionID.sessionOriginator,
						receivedSegment->header.sessionID.sessionNumber);
		destroySegment(receivedSegment);
		return;
	}

	if ( session->RxSession.state == ALL_DATA_RECEIVED ) { // If all data received -> Generate a PN (positive notification)
		//_generatePN(session, span, receivedSegment);
		generate_PN_NN_RA_CAS_CAR(LTP_Orange_PN, span, receivedSegment);
		doLog("Session (E%d,#%d):\tOrange Positive Notification (PN) sent, closing session", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);

	} else { // Generate a NN (negative notification)
		generate_PN_NN_RA_CAS_CAR(LTP_Orange_NN, span, receivedSegment);
		doLog("Session (E%d,#%d):\tOrange Negative Notification (NN) sent, closing session", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
	}
	cleanAndCloseSession(session);
// The segment should not be "destroyed" to avoid a double free. This because the Orange EOB is a mixed segment
// and as that it is also in the Rx_buffer that is cleaned by the celanAndCloseSession.
}

void spanHandler_RS(LTPSpan* span, LTPSegment* receivedSegment) {//Type 8
//Mutex already locked
	LTPSession* session;
	session_list_state_t listState;

	listState=findTxSession(span, receivedSegment, &session);
	doLog("RS in process by spanHandler_RS, listState %d", listState);
	//Send the RA when suitable
	switch (listState) {
	case ACTIVE ://A few anamaly checks, in the normal case break to go on)
		if(session->TxSession.state == CR_RECEIVED){
			doLog("Session (E%d,#%d):\tRS %d refers to a session in CR_RECEIVED state: RS discarded", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->reportSegment.reportSerialNumber);
			destroySegment(receivedSegment);
			return;
		}
		if ( session->TxSession.state == CS_SENT) {	// If the session state is CS_SENT -> send RA
			//_generateRA(session, span, receivedSegment); // Send RA CCaini vedere in RFC se devo farlo
			generate_PN_NN_RA_CAS_CAR(LTP_RA, span, receivedSegment);
			doLog("Session (E%d,#%d):\tRA sent in response to RS %d, but the session was in CS_SENT state: RS discarded", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->reportSegment.reportSerialNumber);
			destroySegment(receivedSegment);
			return;
		}

		generate_PN_NN_RA_CAS_CAR(LTP_RA, span, receivedSegment);
		doLog("Session (E%d,#%d):\tRA sent in response to RS %d", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->reportSegment.reportSerialNumber);
		//Check if the current RS is a duplicate. If yes, send the RA and discard it.
		if ( list_find(session->TxSession.RSProcessed, &(receivedSegment->reportSegment.reportSerialNumber), sizeof(receivedSegment->reportSegment.reportSerialNumber), NULL /*default compare*/) > 0 ) { // RS is already received and processed//
			doLog("Session (E%d,#%d):\tRS %d is a duplicate: RS discarded", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->reportSegment.reportSerialNumber);
			destroySegment(receivedSegment); //RA has already been sent
			return; // The current RS is a duplicate, discard it
		}
		break;//Normal case, go on.
	case RECENTLY_CLOSED :
		generate_PN_NN_RA_CAS_CAR(LTP_RA, span, receivedSegment);
		doLog("Session (E%d,#%d):\tRA sent in response to RS %d, but the session has been recently closed: RS discarded", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber, receivedSegment->reportSegment.reportSerialNumber);	destroySegment(receivedSegment);//free allocated memory of pointers of receivedSegment
		return;
		break;
	case UNKNOWN :
		generate_PN_NN_RA_CAS_CAR(LTP_RA, span, receivedSegment);
		doLog("(E%d,#%d):\t RA sent in response to RS %d, but the session was unknown: RS discarded", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber, receivedSegment->reportSegment.reportSerialNumber);
		destroySegment(receivedSegment);//free allocated memory of pointers of receivedSegment
		return;
		break;
	}

	//In the Normal case (session active, not cancelled, RS is not a duplicate) go on by processing the RS

	//1) insert in the list of already processed RSs
	list_push_front(&(session->TxSession.RSProcessed), &(receivedSegment->reportSegment.reportSerialNumber), sizeof(receivedSegment->reportSegment.reportSerialNumber));

	// 2) Save the claims declared by this RS into the session list of AckedRanges (useful if we have multiple RSs triggered by the same CP? Ask Bisacchi)
	saveAckedRanges(session, receivedSegment);

	// 3) Resend missing data (from the current RS claims)
//	printf("lockThread unblocked by spanHandlersRS\n");
	fflush (stdout);
	pthread_mutex_unlock(&lockThreads);
	resendUnclaimedDataInRS(session, span, receivedSegment);
	pthread_mutex_lock(&lockThreads);
	printf("lockThread unblocked by spanHandlersRS\n");
	fflush (stdout);

	// 4) Check if the whole block has been received
	if ( !haveGapInAckedRanges(session) ) {
		doLog("Session (E%d,#%d):\tBlock acked, signaling SUCCESS to upper protocol, go on with clean and close", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		if ( session->TxSession.handlerSessionTerminated != NULL )
			session->TxSession.handlerSessionTerminated(session->TxSession.dataNumber, SessionResultSuccess); // Signal the upper protocol that this session is completed correctly (RFC 5326 7.4)
		fflush (stdout);
		cleanAndCloseSession(session);
	}
//Check if necessary after the cleanAndCloseSession
	destroySegment(receivedSegment); // //free allocated memory of pointers of receivedSegment
}

void spanHandler_CS(LTPSpan* span, LTPSegment* receivedSegment) {//Type 12
	LTPSession* session;
	session_list_state_t listState;

	listState=findRxSession(span, receivedSegment, &session);

	switch (listState) {
		case ACTIVE :
			generate_PN_NN_RA_CAS_CAR(LTP_CAS, span, receivedSegment);
			doLog("Session (E%d,#%d):\tCAS sent, closing session.", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
			closeSession(session);
			break;
		case RECENTLY_CLOSED :
			generate_PN_NN_RA_CAS_CAR(LTP_CAS, span, receivedSegment);
			doLog("Session (E%d,#%d):\tCAS sent of a recently closed RX session.", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
			break;
		case UNKNOWN :
			doLog("Session (E%d,#%d):\tCS refers to an unknown session. CAS not sent.", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
			break;
		}
	destroySegment(receivedSegment);//free allocated memory of pointers of receivedSegment
}

void spanHandler_CR(LTPSpan* span, LTPSegment* receivedSegment) {//Type 14
	LTPSession* session;
	session_list_state_t listState;

	listState=findTxSession(span, receivedSegment, &session);

	switch (listState) {
	case ACTIVE :
		generate_PN_NN_RA_CAS_CAR(LTP_CAR, span, receivedSegment);
		if ( session->TxSession.handlerSessionTerminated != NULL ){
			session->TxSession.handlerSessionTerminated(session->TxSession.dataNumber, SessionResultCancel); // Signal the upper protocol this session is cancelled (RFC 5326 7.5)
		}
		doLog("Session (E%d,#%d):\t CAR sent, signaled CANCEL to upper protocol, closing session", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
		closeSession(session);
		break;
	case RECENTLY_CLOSED :
		generate_PN_NN_RA_CAS_CAR(LTP_CAR, span, receivedSegment);
		doLog("Session (E%d,#%d):\tCAR sent of a recently closed TX session.", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
		break;
	case UNKNOWN :
		doLog("Session (E%d,#%d):\tCR refers to an unknown session. CAR not sent.", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
		break;
	}
	destroySegment(receivedSegment);//free allocated memory of pointers of receivedSegment
}



static bool resendUnclaimedDataInRS(LTPSession* session, LTPSpan* span, LTPSegment* receivedSegment) {
	bool isLastRangeSetted = false;
	bool result = false;
	unsigned int currentOffset;

	struct {
		unsigned int from;
		unsigned int to;
	} lastRangeToRetransmit;


	currentOffset = receivedSegment->reportSegment.lowerBound;
	for (int i = 0; i < receivedSegment->reportSegment.receptionClaimCount; i++) {//For each claim
		if ( receivedSegment->reportSegment.lowerBound+receivedSegment->reportSegment.receptionClaims[i].offset > currentOffset ) { 
			// There is a gap before the i-th claim
			if ( !isLastRangeSetted ) { // This is the first gap
				isLastRangeSetted = true;
			} else { // This is not the first gap; send the data of previous gap (i-1)
				doLog("Session (E%d,#%d):\tGap in RS between %d and %d", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, 
						lastRangeToRetransmit.from+1, lastRangeToRetransmit.to);
				sendSegmentsInRange(session, span, lastRangeToRetransmit.from, lastRangeToRetransmit.to, receivedSegment->reportSegment.reportSerialNumber,
						LTP_Red_Data, LTP_Red_Data);
				doLog("Session (E%d,#%d):\tMissing data resent", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);

			}
			lastRangeToRetransmit.from = currentOffset;
			lastRangeToRetransmit.to = receivedSegment->reportSegment.lowerBound+receivedSegment->reportSegment.receptionClaims[i].offset;
			result = true;
		}
		currentOffset = receivedSegment->reportSegment.lowerBound+receivedSegment->reportSegment.receptionClaims[i].offset + receivedSegment->reportSegment.receptionClaims[i].length;
	} //end for

	if ( receivedSegment->reportSegment.upperBound > currentOffset ) {
		//There is a gap after the last claim
		if ( !isLastRangeSetted ) { // This is the first gap
			isLastRangeSetted = true;
		} else { // This is not the first gap; send the data of previous gap
			doLog("Session (E%d,#%d):\tGap in RS between %d and %d", session->sessionID.sessionOriginator, session->sessionID.sessionNumber,
					lastRangeToRetransmit.from+1, lastRangeToRetransmit.to);
			sendSegmentsInRange(session, span, lastRangeToRetransmit.from, lastRangeToRetransmit.to, receivedSegment->reportSegment.reportSerialNumber,
					LTP_Red_Data, LTP_Red_Data);
			doLog("Session (E%d,#%d):\tMissing data resent", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		}
		lastRangeToRetransmit.from = currentOffset;
		lastRangeToRetransmit.to = receivedSegment->reportSegment.upperBound;
		result = true;
	}

	if ( isLastRangeSetted ) { // send the last gap + a checkpoint
		doLog("Session (E%d,#%d):\tGap in RS between %d and %d", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, lastRangeToRetransmit.from+1, lastRangeToRetransmit.to);
		sendSegmentsInRange(session, span, lastRangeToRetransmit.from, lastRangeToRetransmit.to, receivedSegment->reportSegment.reportSerialNumber, LTP_Red_Data, LTP_Red_Data_CP);
	}

	return result;
}


void generateRSB(LTPSession* session, LTPSpan* span, List claims, unsigned int checkpointSerialNumber,
		const unsigned int lowerByteToAck) {
	//Called by spanHandlerCP
	LTPSegment segmentToSend;
	ResendStruct* resendStruct;
	ResendStruct* addedResendStruct;
	Notification* notif1;
	Notification* notif2;
	TimerID timerID;

	unsigned int lastUpperBound = lowerByteToAck;

	int currentNumberOfClaims = 0;
	const int totalNumberOfClaims = list_length(claims);
	unsigned int reportNumber = session->RxSession.lastRSnumber;//for the sake of code readibility;

	//segmentToSend building: header
	segmentToSend.header.typeFlag = LTP_RS;
	segmentToSend.header.sessionID = session->sessionID;//usa la session
	segmentToSend.header.headerExtensionsCount = 0;
	segmentToSend.header.trailerExtensionsCount = 0;

	//segmentToSend building: RS specific fields, CP
	segmentToSend.reportSegment.checkpointSerialNumber = checkpointSerialNumber;

	//segmentToSend building: RS specific fields, claims (from the claim list passed in input)

	//One RS can report a max number of claims, one CP can generate one or more RSs.
	//In the latter case, all of them ack the same CP, but have sequential RS numbers
	while (currentNumberOfClaims < totalNumberOfClaims) {
		reportNumber++;
		if ( reportNumber == 0 ) reportNumber++; // Protect to overflow
		//a new RS is prepared and sent at each cycle
		segmentToSend.reportSegment.reportSerialNumber = reportNumber;
		// Set this lowerBound to the last claim sent
		segmentToSend.reportSegment.lowerBound = lastUpperBound;

		const int claimsStillToSend = totalNumberOfClaims - currentNumberOfClaims;
		const int claimSent = (MAX_NUMBER_OF_CLAIMS_IN_REPORT_SEGMENT <= claimsStillToSend ? MAX_NUMBER_OF_CLAIMS_IN_REPORT_SEGMENT : claimsStillToSend);
		segmentToSend.reportSegment.receptionClaimCount = claimSent;

		doLog("Session (E%d,#%d):\tBuilding RS %d in response to CP %d\tLower Bound = %d\t", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, segmentToSend.reportSegment.reportSerialNumber, segmentToSend.reportSegment.checkpointSerialNumber, segmentToSend.reportSegment.lowerBound);

		segmentToSend.reportSegment.receptionClaims = mallocWrapper(sizeof(LTPReportSegmentClaim) * claimSent);
		int i;
		for(i = 0; i < claimSent && claims != empty_list; i++, claims = claims->next) {
			segmentToSend.reportSegment.receptionClaims[i] = *((LTPReportSegmentClaim*)claims->data);

			if ( segmentToSend.reportSegment.receptionClaims[i].offset != 0 ) { // Don't do if the claim is the "confirm all claim" which starts from 0
				// Claim offset in relative to the RS lower & upper bounds -> change from absolute to relative
				segmentToSend.reportSegment.receptionClaims[i].offset -= segmentToSend.reportSegment.lowerBound;
			}
			doLog("Session (E%d,#%d):\tClaim #%d.\tOffset = %d\tLength = %d\tAbsolute ranges: %d - %d", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, i+1, segmentToSend.reportSegment.receptionClaims[i].offset, segmentToSend.reportSegment.receptionClaims[i].length, segmentToSend.reportSegment.lowerBound+segmentToSend.reportSegment.receptionClaims[i].offset+1, segmentToSend.reportSegment.lowerBound+segmentToSend.reportSegment.receptionClaims[i].offset+segmentToSend.reportSegment.receptionClaims[i].length);
		}

		// Set the upperBound to the last byte acked by the last claim added
		lastUpperBound = segmentToSend.reportSegment.lowerBound + segmentToSend.reportSegment.receptionClaims[i-1].offset + segmentToSend.reportSegment.receptionClaims[i-1].length;
		segmentToSend.reportSegment.upperBound = lastUpperBound;

		currentNumberOfClaims += claimSent;

		if ( session->RxSession.state == ALL_DATA_RECEIVED ) { // This is the final RS --> Save the RS number
			session->RxSession.finalRSSentNumber = segmentToSend.reportSegment.reportSerialNumber;
		}

		/*We have two RS lists.
		 * a) In RSWaitingForCP we insert the segment, but only if this RS is destined to trigger a CP in response
		 * 	  (we need this RS claims to find the scope of the CP)
		 * b) in RSWaitingForRa we always insert the re-send structure, as we always need the timerID to stop the reTx timer
		 *     whenever the RS is confirmed by an RA
		 */

		//if this RS will trigger a CP, add a copy of it to the RxSession.RSWaitingForCP list
		bool condition1=segmentToSend.reportSegment.receptionClaimCount > 1;   // More than 1 claim
		//One gap before the claim
		bool condition2=segmentToSend.reportSegment.lowerBound < segmentToSend.reportSegment.receptionClaims[0].offset;
		//One gap after the claim
		bool condition3=segmentToSend.reportSegment.upperBound > (segmentToSend.reportSegment.lowerBound +
				segmentToSend.reportSegment.receptionClaims[0].offset + segmentToSend.reportSegment.receptionClaims[0].length);

		if (condition1 || condition2 || condition3) {

			LTPSegment* RStoInsertInList=(LTPSegment*) mallocWrapper(sizeof(LTPSegment));
			*RStoInsertInList=segmentToSend;
			RStoInsertInList->reportSegment.receptionClaims=mallocWrapper(sizeof(LTPReportSegmentClaim) * claimSent);
			*RStoInsertInList->reportSegment.receptionClaims=*segmentToSend.reportSegment.receptionClaims;

			LTPSegment* addedResendSegment = list_push_front(&(session->RxSession.RSWaitingForCP), RStoInsertInList, sizeof(LTPSegment));
			FREE(RStoInsertInList);//CCaini to_be_verified
			doLog("Session (E%d,#%d):\tRS %u sent in response to CP %d added to RSWaitingForCP", session->sessionID.sessionOriginator, session->sessionID.sessionNumber,
			segmentToSend.reportSegment.reportSerialNumber, segmentToSend.reportSegment.checkpointSerialNumber);
		}

		/*CCaini the resendStruct has been resumed because we need the timerID to stop the reTx timer (timerID)
		 * when a CP,RS,CS,CR is confirmed
		 * The only field to be used should be the timerID and maybe the number of sendings but only for prints.
		 */
		resendStruct=(ResendStruct*) mallocWrapper(sizeof(ResendStruct));
		resendStruct->numberOfSendings = 0;
		resendStruct->timerID = 0;
		resendStruct->segment=mallocWrapper(sizeof(LTPSegment));
		*resendStruct->segment=segmentToSend;

		addedResendStruct = list_push_front(&(session->RxSession.RSWaitingForRA), resendStruct, sizeof(ResendStruct));
		FREE(resendStruct);//CCaini to_be_verified
		doLog("Session (E%d,#%d):\tRS %u (resendStruct) created in response to CP %d added to RSWaitingForRA", session->sessionID.sessionOriginator, session->sessionID.sessionNumber,
			segmentToSend.reportSegment.reportSerialNumber, segmentToSend.reportSegment.checkpointSerialNumber);
		//Create 2 notifications and start a timer that will send, when fires, notif1 and then, after MAX_RETX_NUMBER, notif2  in input to span thread

		notif1=(Notification*) mallocWrapper(sizeof(Notification));
		*notif1=createNotif(RESEND_RS, session->sessionID, reportNumber, DUMMY, session->span->nodeNumber);
		int dummy=0;
		notif2=(Notification*) mallocWrapper(sizeof(Notification));
		*notif2=createNotif(CANCEL_RED_RX_SESSION, session->sessionID, dummy, RXMTCYCEXC, session->span->nodeNumber);

		int interval=session->span->rtoTime;
		int max_retx=MAX_RETX_NUMBER;

		if (DISTRIBUTED_COPIES) {
			interval=interval/RS_COPIES;
			max_retx=max_retx*RS_COPIES;
		}
		timerID=startTimerSpecificFor_CP_RS_CS_CR (interval, max_retx, timerHandlerForwardNotif2, notif1, notif2);
		//Add Timer ID to the structure in the RSWaitingForRa list
		addedResendStruct->timerID=timerID;
		//Add the timer to the session timers		;
		pthread_mutex_lock(&(session->RTXTimersLock));
		list_append(&(session->RTXTimers), &timerID, sizeof(timerID));
		pthread_mutex_unlock(&(session->RTXTimersLock));

		doLog("Session (E%d,#%d):\tRS %u sent in response to CP %d. Waiting for RA", session->sessionID.sessionOriginator, session->sessionID.sessionNumber,
				segmentToSend.reportSegment.reportSerialNumber, segmentToSend.reportSegment.checkpointSerialNumber);

		int burstLength=(DISTRIBUTED_COPIES ? 1: RS_COPIES);
		for (int i = 1; i <= burstLength; i++) {
			new_sendSegment(&segmentToSend, span, false);
		}
	}//end while
	session->RxSession.lastRSnumber=reportNumber;
}


void generate_PN_NN_RA_CAS_CAR(LTPSegmentTypeCode type, LTPSpan* span, LTPSegment* receivedSegment) {
//Attenzione, non può essere usata nel caso NN per timeout; Occore una funzione apposta, analoga alla CS con codice.
	LTPSegment segmentToSend;
	int copies;

	segmentToSend.header.typeFlag = type;
	segmentToSend.header.sessionID = receivedSegment->header.sessionID;
	segmentToSend.header.headerExtensionsCount = 0;
	segmentToSend.header.trailerExtensionsCount = 0;

	if (type==LTP_RA)
	segmentToSend.reportAckSegment.reportSerialNumber = receivedSegment->reportSegment.reportSerialNumber;

	switch (type) {
		case LTP_Orange_PN:{
			copies=PN_COPIES;
		break;
		}
		case LTP_Orange_NN:{
			copies=NN_COPIES;
		break;
		}
		case LTP_RA:{
			copies=RA_COPIES;
			break;
		}
		case LTP_CAS:{
			copies=CAS_COPIES;
		break;
		}
		case LTP_CAR:{
			copies=CAR_COPIES;
		break;
		}
	}

	for (int i = 1; i <= copies; i++) {
			new_sendSegment(&segmentToSend, span, false);
	}

}


