/*
 * spanBlock.h
 *
 *  Created on: 27 dic 2022
 *      Author: carlo
 */

#ifndef SRC_LTPSPAN_SPANBLOCK_H_
#define SRC_LTPSPAN_SPANBLOCK_H_

#include "spanStruct.h"
#include "../LTPsession/session.h"

void handleNewBlockToSend(LTPSpan* span);
void sendSegmentsInRange(LTPSession* session, LTPSpan* span, unsigned int rangeFrom, unsigned int rangeTo, unsigned int reportSerialNumber, LTPSegmentTypeCode normalType, LTPSegmentTypeCode lastType);

#endif /* SRC_LTPSPAN_SPANBLOCK_H_ */
