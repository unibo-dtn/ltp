/*
 * spanNotifications.h
 *
 *  Created on: 26 dic 2022
 *      Author: root
 */

#ifndef SRC_LTPSPAN_SPANNOTIFICATIONS_H_
#define SRC_LTPSPAN_SPANNOTIFICATIONS_H_

#include "spanStruct.h"


void handleNotification(LTPSpan* span);

#endif /* SRC_LTPSPAN_SPANNOTIFICATIONS_H_ */
