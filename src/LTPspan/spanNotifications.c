/*
 * spanNotifications.c
 *
 *  Created on: 26 dic 2022
 *      Author: root
 */
#include "spanNotifications.h"
#include "spanHandlers.h"
#include "../sender/sender.h"
#include "../logger/logger.h"
#include "../generic/generic.h"
#include "span.h"
#include "../config.h"
#include "../LTPsession/session.h"
#include "../adapters/protocol/upperProtocol.h"
#include "../sender/sendNotif.h"
#include "../config.h"
#include "../receiver/receiver.h" //CCaini to share lockThreads mutex


static void generate_NN_afterForcedClosing (LTPSession* session, LTPSpan* span, LTPCancelReasonCode cancelCode);
static void resend_CP_RS_CS_CR (NotificationType type, LTPSession* session, int sequenceNumber, LTPSpan* span, int* sendings);
static void generate_CS_CR(LTPSegmentTypeCode type, LTPSession* session, LTPSpan* span, LTPCancelReasonCode cancelCode);

void handleNotification(LTPSpan* span) {
	Notification* notif;
	LTPSession* session;
	int sendings;

	pthread_mutex_lock(&span->lock);
	notif = (Notification*) list_pop_front(&(span->notifToSpanThread), NULL); // Remove and get first notification
	pthread_mutex_unlock(&span->lock);
//	doLog("Entered in handleNotification");

	if(notif == NULL){
		doLog("Error: Notif is NULL in handleNotification: dropped");
		return; // The notif can't be NULL, it should never happen
	}

	if (notif->type==CANCEL_RED_TX_SESSION || notif->type==RESEND_CP|| notif->type==RESEND_CS|| notif->type==CLOSE_CANCELED_TX_SESSION) {
		session = list_get_pointer_data(span->activeTxSessions, &notif->sessionID, sizeof(LTPSessionID), findSessionFromSessionID); // Get the session from the active sessions
	}
	else if (notif->type==CANCEL_RED_RX_SESSION || notif->type==RESEND_RS|| notif->type==RESEND_CR || notif->type==CLOSE_CANCELED_RX_SESSION||notif->type==CLOSE_ORANGE_RX_SESSION) {
		session = list_get_pointer_data(span->activeRxSessions, &notif->sessionID, sizeof(LTPSessionID), findSessionFromSessionID); // Get the session from the active sessions
	}

	if (session==NULL) {
		doLog("Warinig: notif type %d, seq numb %d,  refers to session (E%d,#%d) which is NULL (no more active) in handleNotification: dropped", notif->type, notif->sequenceNumber, notif->sessionID.sessionOriginator, notif->sessionID.sessionNumber);
		FREE(notif);
    	return; // The session is no more active; skip
    }
	long int timeElapsed=nowInMillis()-notif->timestamp;
//	doLog("Session (E%d,#%d):\tNotif type %d, seq numb %d, forwarded to span %d, at time %ld, to be processed with delay %ld ms", notif->sessionID.sessionOriginator, notif->sessionID.sessionNumber,notif->type, notif->sequenceNumber, notif->spanID, notif->timestamp, timeElapsed);
	switch (notif->type){

//Close a red session that was already cancelled (to many CS or CR retransmitted)
	case CLOSE_CANCELED_TX_SESSION:
		doLog("Session (E%d,#%d):\tNotif CLOSE_CANCELED_TX_SESSION on going to be processed.", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		if(session->TxSession.state == CS_SENT){
			doLog("Session (E%d,#%d):\tToo many CS retransmissions: signaling FAILURE to upper protocol, Tx Session closed. ", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
			if ( session->TxSession.handlerSessionTerminated != NULL ) session->TxSession.handlerSessionTerminated(session->TxSession.dataNumber, SessionResultError);
			cleanAndCloseSession(session);
		}
		break;
	case CLOSE_CANCELED_RX_SESSION:
		doLog("Session (E%d,#%d):\tNotif CLOSE_CANCELED_RX_SESSION on going to be processed.", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		if(session->RxSession.state == CR_SENT){
			doLog("Session (E%d,#%d):\tToo many CR retransmissions: Rx Session closed.", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
			cleanAndCloseSession(session);
		}
        break;
//Close an orange Rx-session (after generating a negative notification)
	case CLOSE_ORANGE_RX_SESSION:
		doLog("Session (E%d,#%d):\tNotif CLOSE_ORANGE_RX_SESSION on going to be processed.", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		generate_NN_afterForcedClosing (session, span, notif->cancelCode);
		doLog("Session (E%d,#%d):\tOrange NN sent (forced closing); Rx session closed", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		cleanAndCloseSession(session);
		break;
//Cancel the red Tx session, generate a CS segment, save it, start a reTx timer
	case CANCEL_RED_TX_SESSION:
		doLog("Session (E%d,#%d):\tNotif CANCEL_RED_TX_SESSION on going to be processed.", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		if(session->TxSession.state != CS_SENT){
			session->TxSession.state = CS_SENT; //change state
			generate_CS_CR(LTP_CS, session, span, notif->cancelCode);
			doLog("Session (E%d,#%d):\tCS sent with cancel code %d. Waiting for CAS", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, notif->cancelCode);
		}
		break;
//Cancel the red Rx session, generate a CR segment, save it, start a reTx timer
	case CANCEL_RED_RX_SESSION:
		doLog("Session (E%d,#%d):\tNotif CANCEL_RED_RX_SESSION on going to be processed.", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		if(session->RxSession.state != CR_SENT){
			session->RxSession.state = CR_SENT; //change state
			clearRXBuffer(session);
			generate_CS_CR(LTP_CR, session, span, notif->cancelCode);
			doLog("Session (E%d,#%d):\tCR sent with cancel code %d. Waiting for CAR", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, notif->cancelCode);
		}
		break;
//Resend a stored segment
	case RESEND_CP:
		doLog("Session (E%d,#%d):\tNotif RESEND_CP %d on going to be processed.", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, notif->sequenceNumber);
		resend_CP_RS_CS_CR (notif->type, session, notif->sequenceNumber, span, &sendings);
		doLog("Session (E%d,#%d):\tCP %d resent (%d times)", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, notif->sequenceNumber, sendings);
        break;
	case RESEND_RS:
		doLog("Session (E%d,#%d):\tNotif RESEND_RS %d on going to be processed.", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, notif->sequenceNumber);
		resend_CP_RS_CS_CR (notif->type, session, notif->sequenceNumber, span, &sendings);
		doLog("Session (E%d,#%d):\tRS %d resent (%d times)", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, notif->sequenceNumber, sendings);
		break;
	case RESEND_CS:
		doLog("Session (E%d,#%d):\tNotif RESEND_CS on going to be processed.", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		resend_CP_RS_CS_CR (notif->type, session, notif->sequenceNumber, span, &sendings);
		doLog("Session (E%d,#%d):\tCS resent (%d times)", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, sendings);
		break;
	case RESEND_CR:
		doLog("Session (E%d,#%d):\tNotif RESEND_CR on going to be processed.", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		resend_CP_RS_CS_CR (notif->type, session, notif->sequenceNumber, span, &sendings);
		doLog("Session (E%d,#%d):\tCR resent (%d times))", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, sendings);
		break;
	}
	FREE(notif);
}

static void generate_CS_CR(LTPSegmentTypeCode type, LTPSession* session, LTPSpan* span, LTPCancelReasonCode cancelCode) {
	int dummy=0;
	int cx_copies;
	LTPSegment segmentToSend;
	Notification* notif1;
	Notification* notif2;
	TimerID timerID;
	ResendStruct* resendStruct;

	segmentToSend.header.typeFlag = type;
	segmentToSend.header.sessionID = session->sessionID;
	segmentToSend.header.headerExtensionsCount = 0;
	segmentToSend.header.trailerExtensionsCount = 0;
	segmentToSend.cancelSegment.cancelCode = cancelCode;

	//Create addedResendStruct (necessary for the timer)
	//
	resendStruct=(ResendStruct*) mallocWrapper(sizeof(ResendStruct));
	resendStruct->numberOfSendings = 0;
	resendStruct->timerID = 0;
	resendStruct->segment=mallocWrapper(sizeof(LTPSegment));
	*resendStruct->segment=segmentToSend;

	if (type==LTP_CS) {
		cx_copies=CS_COPIES;
		session->TxSession.CSWaitingForCAS=(ResendStruct*) resendStruct;

		notif1=(Notification*) mallocWrapper(sizeof(Notification));
		*notif1=createNotif(RESEND_CS, session->sessionID, dummy, DUMMY, session->span->nodeNumber);
		notif2=(Notification*) mallocWrapper(sizeof(Notification));
		*notif2=createNotif(CLOSE_CANCELED_TX_SESSION, session->sessionID, dummy, DUMMY, session->span->nodeNumber);
	}

	else if (type== LTP_CR) {
		cx_copies=CR_COPIES;

		session->RxSession.CRWaitingForCAR=(ResendStruct*) resendStruct;

		notif1=(Notification*) mallocWrapper(sizeof(Notification));
		*notif1=createNotif(RESEND_CR, session->sessionID, dummy, DUMMY, session->span->nodeNumber);
		notif2=(Notification*) mallocWrapper(sizeof(Notification));
		*notif2=createNotif(CLOSE_CANCELED_RX_SESSION, session->sessionID, dummy, DUMMY, session->span->nodeNumber);
	}
	else {
		doLog("Session (E%d,#%d):\tERROR:  Wrong type %d in generate_CS_CR)", session->sessionID.sessionOriginator, type);
		exit(1); //Programming error
	}

	int interval=session->span->rtoTime;
	int max_retx=MAX_RETX_NUMBER;

	if (DISTRIBUTED_COPIES) {
		interval=interval/cx_copies;
		max_retx=max_retx*cx_copies;
	}

	timerID=startTimerSpecificFor_CP_RS_CS_CR (interval, max_retx, timerHandlerForwardNotif2, notif1, notif2);

	doLog("timerID %d", timerID);
	resendStruct->timerID=timerID;
	//	doLog("resendStructure->timerID%d", resendStruct->timerID);
	pthread_mutex_lock(&(session->RTXTimersLock));
	//added
	list_append(&(session->RTXTimers), &timerID, sizeof(timerID));
	pthread_mutex_unlock(&(session->RTXTimersLock));

	int burstLength=(DISTRIBUTED_COPIES ? 1: cx_copies);
	for (int i = 1; i <= burstLength; i++) {
		new_sendSegment(&segmentToSend, span, false);
	}
}

void generate_NN_afterForcedClosing (LTPSession* session, LTPSpan* span, LTPCancelReasonCode cancelCode) {

	LTPSegment segmentToSend;

	segmentToSend.header.typeFlag = LTP_Orange_NN;
	segmentToSend.header.sessionID = session->sessionID;
	segmentToSend.header.headerExtensionsCount = 0;
	segmentToSend.header.trailerExtensionsCount = 0;
	segmentToSend.cancelSegment.cancelCode = cancelCode;//It should not be serialized if Orange NN

	for (int i = 1; i <= NN_COPIES; i++) {
			new_sendSegment(&segmentToSend, span, false);
	}
//	doLog("Session (E%d,#%d):\tNN sent (forced closing)", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
}

static void resend_CP_RS_CS_CR (NotificationType type, LTPSession* session, int sequenceNumber, LTPSpan* span, int* sendings) {
	ResendStruct* resendStruct;
	LTPSegment* segmentToSend;
	int copies;

	switch (type) {
	case RESEND_CP:
		copies=CP_COPIES;
		resendStruct=(ResendStruct*)list_get_pointer_data(session->TxSession.CPWaitingForRS, &sequenceNumber,
						sizeof(sequenceNumber), findCheckpointFromCheckpointNumber);
		break;
	case RESEND_RS:
		copies=RS_COPIES;
		resendStruct= (ResendStruct*)list_get_pointer_data(session->RxSession.RSWaitingForRA, &sequenceNumber,
					sizeof(sequenceNumber), findReportFromReportNumber);
		break;
	case RESEND_CS:
		copies=CS_COPIES;
		resendStruct=(ResendStruct*)session->TxSession.CSWaitingForCAS;
		break;
	case RESEND_CR:
		copies=CR_COPIES;
		resendStruct=(ResendStruct*)session->RxSession.CRWaitingForCAR;

		break;
	}

	if (resendStruct!=NULL){
		resendStruct->numberOfSendings +=1;
		*sendings=resendStruct->numberOfSendings;
		segmentToSend=resendStruct->segment;
		for (int i = 1; i <= copies; i++) {
			new_sendSegment(segmentToSend, span, false);
		}
	}
	else
		doLog("Session (E%d,#%d):\t ERROR in resendCP_RS_CS_CR; resendStruct not found", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		//It acts as a filter, should the segment to retransmit be confirmed in between timer firing and notification processing.
}

