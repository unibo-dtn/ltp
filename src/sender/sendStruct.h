/** \file sendStruct.h
 *
 * \brief This file contains the definitions of the SendStruct structure, used to send LTP segments
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 * \authors Chiara Cippitelli, chiara.cippitelli@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef SRC_SENDER_SENDSTRUCT_H_
#define SRC_SENDER_SENDSTRUCT_H_



//#include "../LTPsession/session.h"
//#include "../LTPspan/spanStruct.h"
#include "../timer/timerStruct.h"
#include "../LTPsegment/segment.h"


typedef struct  {
	LTPSegment*			segment;
	int 		numberOfSendings;
	TimerID				timerID;
} ResendStruct;


#endif /* SRC_SENDER_SENDSTRUCT_H_ */
