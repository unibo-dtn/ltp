/*
 * sendNotif.c
 *
 *  Created on: 20 dic 2022
 *      Author: carlo
 */

#include <stddef.h>
#include <unistd.h>
#include "sendNotif.h"
#include "..//LTPspan/spanStruct.h"
#include "..//LTPspan/span.h"
#include "..//logger/logger.h"
#include "..//generic/generic.h"

Notification createNotif(NotificationType type, LTPSessionID sessionID, int sequenceNumber, LTPCancelReasonCode cancelCode, unsigned long long spanID){
	Notification notif;
	notif.type=type;
	notif.sessionID= sessionID;
	notif.spanID=spanID;
	notif.sequenceNumber= sequenceNumber;
	notif.cancelCode=cancelCode;
	return notif;
}


void timerHandlerForwardNotif2(TimerID timerID, void* _notif) {
	Notification* notif= (Notification*) _notif;
	int result;
	LTPSpan* span = NULL;

//From now on it is the same as forwardingNotifToSpanThread
	//	doLog("Session (E%d,#%d):\tNotif type %d.", notif->sessionID.sessionOriginator, notif->sessionID.sessionNumber, notif->type);
	// Find the span. If the span is not found return
	span = getSpanFromNodeNumber(notif->spanID);
	if ( span == NULL ) { // span NULL -> span not found
		doLog("Info:\tNot found span for spanID= %d. Ignoring notification", notif->spanID);
		return;
	}

	pthread_mutex_lock(&(span->lock));
	notif->timestamp=nowInMillis();
	list_append(&(span->notifToSpanThread), notif, sizeof(Notification)); // Add the notification pointer to the list
//XXX CCaini forse notification*
	pthread_mutex_unlock(&(span->lock));

	// Signal the presence of a new segment in the list
	uint64_t dummy = 1; // 1 because I have 1 new segment to process
	result=write(span->receivedNotifEventFD, &dummy, sizeof(dummy));
	if (result<0) {
		doLog("Span %d:\t forwarding Span New Notif, error in function write", span->nodeNumber);
		exit (-1);
	}
//TODO add
}

void timerHandlerSessionTimeoutNotif(TimerID timerID, void* _notif) {
	Notification* notif= (Notification*) _notif;
	int result;
	LTPSpan* span = NULL;
	LTPSession* session=NULL;
	uint64_t dummy = 1; // 1 because I have 1 new segment to process

	//	doLog("Session (E%d,#%d):\tNotif type %d.", notif->sessionID.sessionOriginator, notif->sessionID.sessionNumber, notif->type);
	//This function is called by the timer thread; it is necessary to check if the span and the session are still active.

	//1) Find the span. If the span is not found return
	span = getSpanFromNodeNumber(notif->spanID);
	if ( span == NULL ) { // span NULL -> span not found
		doLog("Info:\tNot found span for spanID= %d. Ignoring notification", notif->spanID);
		FREE(notif);
		return;
	}

	// 2) Find the session. If the session is not found return
	if (notif->type==CANCEL_RED_TX_SESSION) {
		session = list_get_pointer_data(span->activeTxSessions, &notif->sessionID, sizeof(LTPSessionID), findSessionFromSessionID); // Get the session from the active Tx sessions
	}
	else if (notif->type==CANCEL_RED_RX_SESSION || notif->type==CLOSE_ORANGE_RX_SESSION) {
		session = list_get_pointer_data(span->activeRxSessions, &notif->sessionID, sizeof(LTPSessionID), findSessionFromSessionID); // Get the session from the active Rx sessions
	}
	if (session==NULL) {
		doLog("Warinig: notif type %d, seq numb %d,  refers to session (E%d,#%d) which is NULL (no more active) in handleNotification: dropped", notif->type, notif->sequenceNumber, notif->sessionID.sessionOriginator, notif->sessionID.sessionNumber);
		FREE(notif);
		return; // The session is no more active; return
	}

	//3) The session is active, go on performing
	if (notif->type==CANCEL_RED_TX_SESSION) {
		if ( session->TxSession.handlerSessionTerminated != NULL ) {
			session->TxSession.handlerSessionTerminated(session->TxSession.dataNumber, SessionResultCancel); // Signal the upper protocol this Tx session has been canceled
		}
	}
	else if (notif->type==CANCEL_RED_RX_SESSION ||notif->type==CLOSE_ORANGE_RX_SESSION) {
		clearRXBuffer(session);
	}

	//4) Forward the notif to span thread
	pthread_mutex_lock(&(span->lock));
	notif->timestamp=nowInMillis();
	list_append(&(span->notifToSpanThread), notif, sizeof(Notification)); // Add the notification pointer to the list
	result=write(span->receivedNotifEventFD, &dummy, sizeof(dummy));// Signal the presence of a new segment in the list
	pthread_mutex_unlock(&(span->lock));

	if (result<0) {
		doLog("Span %d:\t forwarding Span New Notif, error in function write", span->nodeNumber);
		exit (-1);
	}
}



void forwardingNotifToSpanThread(Notification* notif) {
	int result;
	LTPSpan* span = NULL;

//	doLog("Session (E%d,#%d):\tNotif type %d.", notif->sessionID.sessionOriginator, notif->sessionID.sessionNumber, notif->type);

// Find the span. If the span is not found return
	span = getSpanFromNodeNumber(notif->sessionID.sessionOriginator);
	if ( span == NULL ) { // span NULL -> span not found
		doLog("ERROR:\tNot found span for sessionOriginator= %d, in forwardingNotifToSpanThread", notif->sessionID.sessionOriginator);
		return;
	}
	pthread_mutex_lock(&(span->lock));
	notif->timestamp=nowInMillis();
	list_append(&(span->notifToSpanThread), notif, sizeof(Notification)); // Add the notification pointer to the list
	pthread_mutex_unlock(&(span->lock));
	doLog("Session (E%d,#%d):\tNotif (by forwardNotifToSpanThread)type %d, seq numb %d, forwarded to span %d, at time %ld", notif->sessionID.sessionOriginator, notif->sessionID.sessionNumber,notif->type, notif->sequenceNumber, notif->spanID, notif->timestamp );
	// Signal the presence of a new segment in the list notif->sessionID.sessionOriginator
	uint64_t dummy = 1; // 1 because I have 1 new segment to process
	result=write(span->receivedNotifEventFD, &dummy, sizeof(dummy));
        if (result<0) {
		doLog("Span %d:\t forwardingNotifToSpanThread, error in function write", span->nodeNumber);
                exit (-1);
        }

}
