/** \file sender.c
 *
 * \brief This file contains the implementation of the functions used to send LTP segments
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/time.h>

#include "sender.h"

#include "../adapters/protocol/lowerProtocol.h"
#include "../adapters/protocol/upperProtocol.h"
#include "../generic/generic.h"
#include "../list/list.h"
#include "../config.h"
#include "../LTPspan/span.h"
#include "../LTPsegment/segment.h"
#include "../logger/logger.h"


void new_sendSegment(LTPSegment* segment, LTPSpan* span, bool paced) {
//It takes the (pointer to) segment, serialize it into a buffer, pass the serialized segment (the buffer) to the lower protocol (e.g. UDP)

	if ( segment == NULL || span==NULL) return;

	char buffer[LOWER_LEVEL_MAX_PACKET_SIZE];

	memset(&(buffer[0]), '\0', LOWER_LEVEL_MAX_PACKET_SIZE); // XXX: theoretical useless

	unsigned int segmentTotalSize = serializeSegment(segment, &(buffer[0]), LOWER_LEVEL_MAX_PACKET_SIZE);

	if (paced) {
		waitUntilAllowedByCongCtrl(span, segmentTotalSize);
	}

	doLogFile(segment);//Added by CCaini

	if (!passSerializedSegmentToLowerProtocol(span->lowerProtocol, buffer, segmentTotalSize, span->address)) {
		fprintf(stderr, "Error on passSerializedSegmentToLowerProtocol\n");
		//exit(1);
	}
}
