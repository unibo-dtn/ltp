/*
 * sendNotif.h
 *
 *  Created on: 20 dic 2022
 *      Author: carlo
 */

#ifndef SRC_SENDER_SENDNOTIF_H_
#define SRC_SENDER_SENDNOTIF_H_

#include "../LTPsession/sessionID.h"
#include "../LTPsegment/segment.h"
#include "../timer/timer.h"

typedef enum {
	CLOSE_ORANGE_RX_SESSION=0,
	CANCEL_RED_TX_SESSION,
	CANCEL_RED_RX_SESSION,

	RESEND_CP,
	RESEND_RS,
	RESEND_CS,
	RESEND_CR,

	CLOSE_CANCELED_TX_SESSION,
	CLOSE_CANCELED_RX_SESSION
} NotificationType;

typedef struct {
	NotificationType type;
	LTPSessionID	sessionID;
	unsigned long long	int spanID;
	int sequenceNumber; //for CP and RS only
	LTPCancelReasonCode 	cancelCode; //  for CS and CR only
	long int timestamp;
} Notification;

Notification createNotif(NotificationType type, LTPSessionID sessionID, int sequenceNumber, LTPCancelReasonCode cancelCode, unsigned long long spanID);
void forwardingNotifToSpanThread(Notification* notif);
void timerHandlerForwardNotif2 (TimerID timerID, void* notif);
void timerHandlerSessionTimeoutNotif (TimerID timerID, void* _notif);
#endif /* SRC_SENDER_SENDNOTIF_H_ */
