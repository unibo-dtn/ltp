/** \file lowerProtocol.c
 *
 * \brief This file contains the implementation of the interface to lower protocols
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <netinet/ip_icmp.h>
#include <netinet/udp.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/eventfd.h>
#include <sys/select.h>
#include <stdbool.h>

#include "../../list/list.h"
#include "lowerProtocol.h"
#include "lowerProtocols/lowerProtocolAdapter.h"
#include "../../generic/generic.h"
#include "../../network/network_utils.h"

static int _findLowerProtocolByName(void* _name, size_t _nameSize, void* _adapter, size_t _adapterSize);
static void _forEachDisposeProtocol(void* _adapter, size_t _adapterSize);

static List lowerProtocolsList = empty_list;
static List lowerProtocolsInitializedList = empty_list;

bool initLowerLevel() {
	LowerProtocolAdapter adapter = getUDPLowerProtocolAdapter();
	list_append(&lowerProtocolsList, &adapter, sizeof(adapter));

	adapter = getECLSALowerProtocolAdapter();
	list_append(&lowerProtocolsList, &adapter, sizeof(adapter));

	// XXX insert here new lower protocols

	return true;
}

void disposeLowerProtocol() {
	list_for_each(lowerProtocolsInitializedList, _forEachDisposeProtocol);
	list_destroy(&lowerProtocolsInitializedList);
	list_destroy(&lowerProtocolsList);
}

bool initLowerLevelProtocol(const char* protocol, char* initString) {
	LowerProtocolAdapter* lowerProtocol = (LowerProtocolAdapter*) list_get_pointer_data(lowerProtocolsList, (void*) protocol, sizeof(protocol), _findLowerProtocolByName);

	if ( lowerProtocol == NULL ) {
		return false;
	}

	bool initialized = lowerProtocol->init(initString);

	if ( initialized ) {
//		list_append(&lowerProtocolsInitializedList, lowerProtocol, sizeof(*lowerProtocol));
		list_append(&lowerProtocolsInitializedList, lowerProtocol, sizeof(LowerProtocolAdapter));
	}

	return initialized;
}

bool initOutLowerLevel(const char* protocol, unsigned int span, const char* initString, destination_struct* address) {
	LowerProtocolAdapter* lowerProtocol = (LowerProtocolAdapter*) list_get_pointer_data(lowerProtocolsInitializedList, (void*) protocol, sizeof(protocol), _findLowerProtocolByName);

	if ( lowerProtocol == NULL ) {
		return false;
	}

	return lowerProtocol->initOut((char*) initString, address);
}

void disposeLowerLevelProtocol(const char* protocol) {
	LowerProtocolAdapter* lowerProtocol = (LowerProtocolAdapter*) list_get_pointer_data(lowerProtocolsInitializedList, (void*) protocol, sizeof(protocol), _findLowerProtocolByName);

	if ( lowerProtocol != NULL ) {
		lowerProtocol->dispose();
//		list_remove_data(&lowerProtocolsInitializedList, lowerProtocol, sizeof(*lowerProtocol), NULL /*default*/);
		list_remove_data(&lowerProtocolsInitializedList, lowerProtocol, sizeof(LowerProtocolAdapter), NULL /*default*/);
	}
}

bool passSerializedSegmentToLowerProtocol(const char* protocol, char* buffer, int bufferLength, destination_struct address) {
	LowerProtocolAdapter* lowerProtocol = (LowerProtocolAdapter*) list_get_pointer_data(lowerProtocolsInitializedList, (void*) protocol, sizeof(protocol), _findLowerProtocolByName);

	if ( lowerProtocol == NULL ) {
		return false;
	}

	return lowerProtocol->send(buffer, bufferLength, address);
}

bool getSerializedSegmentFromLowerProtocol(char* buffer, int *bufferLength) {
	fd_set fdset; // cache the value, avoid to re-calculate every time
	int maxFD = -1;

	FD_ZERO(&fdset);

	if ( lowerProtocolsInitializedList == empty_list ) {
		fprintf(stderr, "getSerializedSegmentFromLowerProtocol failure; no adapters found\n");
		return false;
	}

	// Sets the FD from the lower protocols
	for (List lowerProtocolList = lowerProtocolsInitializedList; lowerProtocolList != empty_list; lowerProtocolList = lowerProtocolList->next) {
		LowerProtocolAdapter* lowerProtocol = (LowerProtocolAdapter*) lowerProtocolList->data;
		maxFD = max(maxFD, lowerProtocol->setFDForSelect(&fdset));
	}

	struct timeval timeout = {1,0}; // seconds of timeout
	select(maxFD+1, &fdset, NULL, NULL, &timeout);

	// Return the data of the first lower protocol
	for (List lowerProtocolList = lowerProtocolsInitializedList; lowerProtocolList != empty_list; lowerProtocolList = lowerProtocolList->next) {
		LowerProtocolAdapter* lowerProtocol = (LowerProtocolAdapter*) lowerProtocolList->data;
		if ( lowerProtocol->receivedFromFDForSelect(&fdset) ) {
			return lowerProtocol->receive(buffer, bufferLength);
		}
	}

	return false;
}

/***** PRIVATE FUNCTIONS  *****/

static int _findLowerProtocolByName(void* _name, size_t _nameSize, void* _adapter, size_t _adapterSize) {
	char* name = (char*) _name;
	LowerProtocolAdapter* adapter = (LowerProtocolAdapter*) _adapter;
	return strcmp(name, adapter->protocolName);
}

static void _forEachDisposeProtocol(void* _adapter, size_t _adapterSize) {
	LowerProtocolAdapter* adapter = (LowerProtocolAdapter*) _adapter;

	if ( adapter != NULL ) {
		disposeLowerLevelProtocol(adapter->protocolName);
	}
}


