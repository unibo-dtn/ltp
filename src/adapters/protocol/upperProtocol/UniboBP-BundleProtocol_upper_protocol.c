/** \file UniboBP-BundleProtocol_upper_protocol.c
 *
 * \brief This file contains the implementation of one upper protocol based on Bundle Protocol implemented in Unibo-BP
 *
 * \copyright Copyright (c) 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Lorenzo Persampieri, lorenzo.persampieri@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

extern int dummy_unibo_bp_variable_to_avoid_pedantic_error;

//Decomment when debugging in Eclipse; otherwise let it commented
//#define HAVE_UNIBO_BP

// DECOMMENT WHEN EXECUTING/COMPILING
#ifdef HAVE_UNIBO_BP

#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <sys/time.h>
#include <math.h>
#include <unistd.h>

#include "../upperProtocol.h"
#include "../upperProtocolReceivedData.h"

#include "../../../logger/logger.h"
#include "../../../LTPsession/session.h"
#include "../../../list/list.h"
#include "../../../generic/generic.h"
#include "../../../contact/contact.h"
#include "../../../range/range.h"
#include "../../../LTPspan/span.h"
#include "../../../timer/timer.h"
#include "../../../timer/timerStruct.h"
#include "../../../config.h"

#include "Unibo-BP/cla.h"

#ifndef DEFAULT_LTP_CLA_ID
#define DEFAULT_LTP_CLA_ID 2000
#endif

//CCaini this structure is no more necessary if we insert in the transmission structure the parsed version of
//ECOS
/*typedef struct {
    UniboBPECOS ecos; // UNPARSED ECOS of the bundle
    struct timeval deadline; // deadline to complete the LTP Tx session
    uint64_t reTx_counter; // number of bundle retransmissions
} TransmissionBundleMetadata;
*/
void parseECOS(UniboBPECOS unparsedEcos, LTPEcos *parsedEcos);

typedef struct {
    unsigned int ltp_transmission_id;
    uint64_t link_id;
    uint64_t transmission_id;
} SendingListObject;

typedef struct {
    uint64_t link_id;
    uint64_t transmission_id;

    int fd; // file descriptor of the file containing the bundle
    uint64_t length; //length of the file containing the bundle

    //NUOVA STRUTTURA AUSILIARIA: metadata NON parsati...
//TransmissionBundleMetadata metadata;
    LTPBundleMetadata metadata;
} UniboBPTransmission;

typedef struct {
    uint64_t link_id;
    UniboBPTransmission* current_pdu;
} UniboBPReceivedData;

typedef struct {
    UniboBPEID eid;
    uint64_t peer_engine_id;
    uint64_t link_id;
    List transmissions;

    // note: we must use a pointer here because the item are copied by the List library.
    // Copying pthread_* variables is undefined behaviour.
    pthread_cond_t* tx_cv;
} UniboBPPeerData;

/* global data */

static UniboBPCLAHandle* get_cla_handle(void) {
    static UniboBPCLAHandle handle = NULL;
    return &handle;
}

static pthread_mutex_t* get_global_mutex() {
    static pthread_mutex_t mtx;
    return &mtx;
}

static List* get_sending_list() {
    static List l;
    return &l;
}

static List* get_transmission_list() {
    static List l;
    return &l;
}

static List* get_peer_data_list() {
    static List l;
    return &l;
}

static void handleUniboBPError() {
    fprintf(stderr, "Unibo-BP error\n");
    exit(1);
}

static unsigned int generate_ltp_transmission_id() {
    static unsigned int ltp_transmission_id = 1;
    if (ltp_transmission_id == 0)
        return ++ltp_transmission_id;
    else
        return ltp_transmission_id++;
}


/*
static UniboBPTransmission* create_transmission_struct(void) {
    UniboBPTransmission* transmission = mallocWrapper(sizeof(UniboBPTransmission));
    memset(transmission, 0, sizeof(UniboBPTransmission));
    UniboBPError error = unibo_bp_ecos_create(&transmission->ecos);
    if (error != UniboBP_NoError) {
        fprintf(stderr, "Malloc failed.");
        exit(1);
    }
    return transmission;
}
*/

static void free_transmission_struct(UniboBPTransmission* transmission) {
//CCaini non capito    unibo_bp_ecos_destroy(&transmission->metadata.ecos);
    FREE(transmission);
}

static void initialize_peer_data(UniboBPEID eid, uint64_t peerEngineID, uint64_t linkID, UniboBPPeerData* peerData) {
    memset(peerData, 0, sizeof(UniboBPPeerData));
    peerData->eid = eid;
    peerData->peer_engine_id = peerEngineID;
    peerData->link_id = linkID;
    peerData->tx_cv = mallocWrapper(sizeof(pthread_cond_t));
    pthread_cond_init(peerData->tx_cv, NULL);
}

static void clear_peer_data(UniboBPPeerData* peerData) {
    unibo_bp_eid_destroy(&peerData->eid);
    pthread_cond_destroy(peerData->tx_cv);
    FREE(peerData->tx_cv);
}

static void free_peer_data(UniboBPPeerData* peerData) {
    clear_peer_data(peerData);
    FREE(peerData);
}

/*
static int _findPeerDataFromPeerEngineID(void* _peerEngineID, size_t peerEngineIDSize, void* _peerData, size_t peerDataSize) {
    (void) peerEngineIDSize; // unused
    (void) peerDataSize; // unused
    uint64_t* peerEngineID = _peerEngineID;
    UniboBPPeerData* peerData = _peerData;
    if (*peerEngineID == peerData->peer_engine_id)
        return 0;
    else
        return 1;
}


static UniboBPPeerData* getPeerDataFromEngineID(uint64_t engine_id) {
    UniboBPPeerData* result = NULL;
    pthread_mutex_lock(get_global_mutex());
    result = list_get_pointer_data(*get_peer_data_list(), &engine_id, sizeof(engine_id), _findPeerDataFromPeerEngineID);
    pthread_mutex_unlock(get_global_mutex());
    return result;
}
*/
static int _findPeerDataFromLinkID(void* _linkID, size_t linkIDSize, void* _peerData, size_t peerDataSize) {
    (void) linkIDSize; // unused
    (void) peerDataSize; // unused
    uint64_t* linkID = _linkID;
    UniboBPPeerData* peerData = _peerData;
    if (*linkID == peerData->link_id)
        return 0;
    else
        return 1;
}

static int _findPeerDataFromEID(void* _eid, size_t eidSize, void* _peerData, size_t peerDataSize) {
    (void) eidSize; // unused
    (void) peerDataSize; // unused
    ConstUniboBPEID eid = _eid;
    UniboBPPeerData* peerData = _peerData;
    if (unibo_bp_eid_is_same_node(eid, unibo_bp_const_cast_eid(peerData->eid)))
        return 0;
    else
        return 1;
}

static UniboBPPeerData* getPeerDataFromLinkID(uint64_t link_id) {
    UniboBPPeerData* result = NULL;
    pthread_mutex_lock(get_global_mutex());
    result = list_get_pointer_data(*get_peer_data_list(), &link_id, sizeof(link_id), _findPeerDataFromLinkID);
    pthread_mutex_unlock(get_global_mutex());
    return result;
}

static int _findSendingListObjectFromLTPTransmissionID(void* _ltpTransmissionID, size_t ltpTransmissionIDSize, void* _sendingListObject, size_t sendingListObjectSize) {
    (void) ltpTransmissionIDSize; // unused
    (void) sendingListObjectSize; // unused
    unsigned int* ltpTransmissionID = _ltpTransmissionID;
    SendingListObject* sendingListObject = _sendingListObject;
    if (*ltpTransmissionID == sendingListObject->ltp_transmission_id)
        return 0;
    else
        return 1;
}

static SendingListObject* getSendingListObjectFromLTPTransmissionID(unsigned int ltp_transmission_id) {
    SendingListObject* result = NULL;
    pthread_mutex_lock(get_global_mutex());
    result = list_get_pointer_data(*get_sending_list(), &ltp_transmission_id, sizeof(ltp_transmission_id), _findSendingListObjectFromLTPTransmissionID);
    pthread_mutex_unlock(get_global_mutex());
    return result;
}

static unsigned int pushSendingListObject(UniboBPTransmission* transmission) {
    SendingListObject sendingObject;
    sendingObject.link_id = transmission->link_id;
    sendingObject.transmission_id = transmission->transmission_id;
    sendingObject.ltp_transmission_id = generate_ltp_transmission_id();
    pthread_mutex_lock(get_global_mutex());
    list_append(get_sending_list(), &sendingObject, sizeof(sendingObject));
    pthread_mutex_unlock(get_global_mutex());
    return sendingObject.ltp_transmission_id;
}

static void _handlerSessionTerminated(unsigned int dataNumber, LTPSessionResultCode resultCode) {

    SendingListObject* object = getSendingListObjectFromLTPTransmissionID(dataNumber);

    if (!object) return;

    switch ( resultCode ) {
        case SessionResultSuccess:
            unibo_bp_cla_transmission_success(*get_cla_handle(), object->link_id, object->transmission_id);
            break;
        case SessionResultCancel: /* fallthrough */
        case SessionResultError:
            unibo_bp_cla_transmission_failure(*get_cla_handle(), object->link_id, object->transmission_id);
            break;
        case SessionNoResult:
        default:
            doLog("Why here?");
            break;
    }

    list_remove_data(get_sending_list(), &dataNumber, sizeof(dataNumber), _findSendingListObjectFromLTPTransmissionID);
}

static void onUpdateConfigFlags(uint64_t enabled_flags, uint64_t disabled_flags, void* userArg) {
    (void) userArg;
    doLog("received enabled flags: %" PRIu64, enabled_flags);
    doLog("received disabled flags: %" PRIu64, disabled_flags);
/*
 * Write what to do when notified that a config flag in Unibo-BP has changed...
 */

    /* ... do something in Unibo-LTP ... */
}

static bool onOutboundPdu(uint64_t link_id, ConstUniboBPCLAOutboundPDU pdu, void* userArg) {
/*
 * It is called when Unibo-LTP is notified by Unibo-BP that a bundle is ready to be transmitted
 * The bundle pdu is serialized in a file, whose file descriptor is saved in a Unibo-LTP structure (transmission)
 * The same structure contains also other fields, some to manage the pdu (e.g. the length), others to save QoS metadata
 * (ecos, retransmission counter, etc.)
 * The structure, once built is added to a queue (a list). This queue contains the bundles (the "transmission" structure
 * released for transmission by Unibo-BP flow control (based on contacts and data speed as in ION); they will be extracted from the queue by Unibo-LTP when allowed
 * by Unibo-LTP flow control (e.g. max number of concurrent Tx session)
 */
    (void) userArg;
    UniboBPTransmission transmission;
    transmission.link_id = link_id;
    transmission.transmission_id = unibo_bp_cla_outbound_pdu_get_transmission_id(pdu);

    transmission.fd = unibo_bp_cla_outbound_pdu_get_fd(pdu);
    transmission.length = unibo_bp_cla_outbound_pdu_get_size(pdu);

//CCaini The lines below insert the parsed version of ECOS into transmission.metadata.ecos
    UniboBPECOS temp_ecos;
    UniboBPError error = unibo_bp_ecos_clone(unibo_bp_cla_outbound_pdu_get_ecos(pdu), &temp_ecos);
    if (error != UniboBP_NoError) {
       handleUniboBPError();
    }
    parseECOS(temp_ecos, &transmission.metadata.ecos);


    unibo_bp_cla_outbound_pdu_get_deadline_hint(pdu, &transmission.metadata.deadline);
    transmission.metadata.reTx_counter = unibo_bp_cla_outbound_pdu_get_retransmission_counter(pdu);

    pthread_mutex_lock(get_global_mutex());

    UniboBPPeerData* peer_data = getPeerDataFromLinkID(link_id);
    if (!peer_data) {
        pthread_mutex_unlock(get_global_mutex());
        return false;
    }
    if (list_length(peer_data->transmissions) == 0) {
        pthread_cond_signal(peer_data->tx_cv);
    }
    list_append(&peer_data->transmissions, &transmission, sizeof(transmission));
    pthread_mutex_unlock(get_global_mutex());

    return true;
}

static void onStop(void* userArg) {
    (void) userArg; // unused
    fprintf(stderr, "Unibo-BP stopped\n");
    exit(1);
}

static void buildContact(uint64_t peer_engine_id, bool uplink, struct timeval* start_time,
                         struct timeval* end_time, uint64_t xmit_rate_Bps, Contact* contact) {
    if (uplink) {
        contact->fromNode = getMyNodeNumber();
        contact->toNode = peer_engine_id;
    } else {
        contact->fromNode = peer_engine_id;
        contact->toNode = getMyNodeNumber();
    }
    contact->fromTime = start_time->tv_sec;
    if (end_time) {
        contact->toTime = end_time->tv_sec;
    }
    contact->xmitRate = xmit_rate_Bps;
}

static void onAddContact(ConstUniboBPEID neighbor, bool uplink, struct timeval* start_time,
                         struct timeval* end_time, uint64_t xmit_rate_Bps, void* userArg) {
    (void) userArg; // unused
    if (!uplink) return;
    pthread_mutex_lock(get_global_mutex());
    UniboBPPeerData *peerData = list_get_pointer_data(*get_peer_data_list(), neighbor, sizeof(neighbor), _findPeerDataFromEID);
    
    if (!peerData) {
        pthread_mutex_unlock(get_global_mutex());
        return;
    }
    Contact contact;

    buildContact(peerData->peer_engine_id, uplink, start_time, end_time, xmit_rate_Bps, &contact);

    pthread_mutex_unlock(get_global_mutex());
    
    LTPSpan *span = getSpanFromNodeNumber(contact.toNode);
    if ( span != NULL ) {
        addContactToSpan(&contact, span);
    }
}

static void onRemoveContact(ConstUniboBPEID neighbor, bool uplink, struct timeval* start_time, void* userArg) {
    (void) userArg; // unused
    if (!uplink) return;
    pthread_mutex_lock(get_global_mutex());
    UniboBPPeerData *peerData = list_get_pointer_data(*get_peer_data_list(), neighbor, sizeof(neighbor), _findPeerDataFromEID);

    if (!peerData) {
        pthread_mutex_unlock(get_global_mutex());
        return;
    }
    Contact contact;

    buildContact(peerData->peer_engine_id, uplink, start_time, NULL, 0, &contact);

    pthread_mutex_unlock(get_global_mutex());

    LTPSpan *span = getSpanFromNodeNumber(contact.toNode);
    if ( span != NULL ) {
        removeContactFromSpan(&contact, span);
    }
}

static void buildRange(uint64_t peer_engine_id, bool uplink, struct timeval* start_time,
        struct timeval* end_time, uint64_t owlt_s, Range* range) {
    if (uplink) {
        range->fromNode = getMyNodeNumber();
        range->toNode = peer_engine_id;
    } else {
        range->fromNode = peer_engine_id;
        range->toNode = getMyNodeNumber();
    }
    range->fromTime = start_time->tv_sec;
    if (end_time) {
        range->toTime = end_time->tv_sec;
    }
    range->seconds = owlt_s;
}

static void onAddRange(ConstUniboBPEID neighbor, bool uplink, struct timeval* start_time,
                         struct timeval* end_time, uint64_t owlt_s, void* userArg) {
    (void) userArg; // unused
    pthread_mutex_lock(get_global_mutex());
    UniboBPPeerData *peerData = list_get_pointer_data(*get_peer_data_list(), neighbor, sizeof(neighbor), _findPeerDataFromEID);

    if (!peerData) {
        pthread_mutex_unlock(get_global_mutex());
        return;
    }
    Range range;

    buildRange(peerData->peer_engine_id, uplink, start_time, end_time, owlt_s, &range);

    pthread_mutex_unlock(get_global_mutex());

    LTPSpan *span = getSpanFromNodeNumber(range.toNode);
    if ( span != NULL ) {
        addRangeToSpan(&range, span);
    }
    if ( range.toNode != range.fromNode ) {
        span = getSpanFromNodeNumber(range.fromNode);
        if ( span != NULL ) {
            addRangeToSpan(&range, span);
        }
    }
}

static void onRemoveRange(ConstUniboBPEID neighbor, bool uplink, struct timeval* start_time, void* userArg) {
    (void) userArg; // unused
    pthread_mutex_lock(get_global_mutex());
    UniboBPPeerData *peerData = list_get_pointer_data(*get_peer_data_list(), neighbor, sizeof(neighbor), _findPeerDataFromEID);

    if (!peerData) {
        pthread_mutex_unlock(get_global_mutex());
        return;
    }
    Range range;

    buildRange(peerData->peer_engine_id, uplink, start_time, NULL, 0, &range);

    pthread_mutex_unlock(get_global_mutex());

    LTPSpan *span = getSpanFromNodeNumber(range.toNode);
    if ( span != NULL ) {
        removeRangeFromSpan(&range, span);
    }
    if ( range.toNode != range.fromNode ) {
        span = getSpanFromNodeNumber(range.fromNode);
        if ( span != NULL ) {
            removeRangeFromSpan(&range, span);
        }
    }
}

unsigned int getUpperProtocolClientServiceID() {
	return 1; // Bundle Protocol is 1
}

void loadContactPlanUpperProtocol() {
	/* do nothing */
}

void deliverBlockToUpperProtocol(LTPBlock* block) {
    UniboBPCLAHandle *handle = get_cla_handle();
    int fd = unibo_bp_cla_open_file(*handle);
    if (fd < 0) return;
    uint64_t remaining = block->blockLength;
    uint8_t* cursor = block->data;
    const size_t chunk_size = 4096U;
    while (remaining) {
        size_t to_write = (remaining > chunk_size) ? chunk_size : remaining;
        ssize_t n_written = write(fd, cursor, to_write);
        if (n_written < 0) {
            close(fd);
            return;
        }
        remaining -= n_written;
        cursor += n_written;
    }
    lseek(fd, 0, SEEK_SET);
    unibo_bp_cla_inbound_pdu(*handle, 0, fd, block->blockLength);
    close(fd);
}

void parseECOS(UniboBPECOS unparsedEcos, LTPEcos *parsedEcos){
	// get functions offered by Unibo-BP; see "ECOS.h"
    parsedEcos->fields.critical = unibo_bp_ecos_get_flag(unibo_bp_const_cast_ecos(unparsedEcos), UniboBPECOSFlag_critical);
    parsedEcos->fields.bestEffort = unibo_bp_ecos_get_flag(unibo_bp_const_cast_ecos(unparsedEcos), UniboBPECOSFlag_bestEffortForward);
    parsedEcos->fields.qosTagIsPresent = unibo_bp_ecos_get_flag(unibo_bp_const_cast_ecos(unparsedEcos), UniboBPECOSFlag_qosTag);
    parsedEcos->fields.reliable = unibo_bp_ecos_get_flag(unibo_bp_const_cast_ecos(unparsedEcos), UniboBPECOSFlag_reliableForward);
    parsedEcos->fields.bssp = unibo_bp_ecos_get_flag(unibo_bp_const_cast_ecos(unparsedEcos), UniboBPECOSFlag_bsspRequired);
    parsedEcos->fields.bibe = unibo_bp_ecos_get_flag(unibo_bp_const_cast_ecos(unparsedEcos), UniboBPECOSFlag_bibeRequired);
    parsedEcos->fields.bibeCustody = unibo_bp_ecos_get_flag(unibo_bp_const_cast_ecos(unparsedEcos), UniboBPECOSFlag_bibeCustodyTransferRequired);

    parsedEcos->priority.cardinal = unibo_bp_ecos_get_cardinal_priority(unibo_bp_const_cast_ecos(unparsedEcos));
    parsedEcos->priority.ordinal = unibo_bp_ecos_get_ordinal_priority(unibo_bp_const_cast_ecos(unparsedEcos));

    parsedEcos->QoS_tag = unibo_bp_ecos_get_qos_tag(unibo_bp_const_cast_ecos(unparsedEcos));
}

void passedDataFromUpperProtocol(unsigned long long destination, UpperProtocolDataToSend* dataToSend,
		UpperProtocolReceivedData* receivedData) {
/*
 * It is called by handleNewBlockToSend of _mainSpanThread when Unibo-LTP is ready to send a new block (start a new session)
 */
	(void) destination; // unused
    if ( dataToSend == NULL ) {
		doLog("receiveDataFromUpperProtocol has NULL value");
		return;
	}

	// Wait for the data (see function canReceiveDataFromUpperProtocol)
	sem_wait(&receivedData->semIsDataPresent);

/*
 * Ci sono le scatole cinesi:
 * 1) receivedData "punta" a
 * 2) specificData, che al suo interno contiene nel campo
 * 3) "current_pdu", una struttura di tipo "transmission", ovvero il tipo usato per inserire
 * un bundle serializzato insieme ai suoi metadati, inclusi quelli del QoS
 * Transmission viene usato, in parte, per creare una nuova struttura
 * 4) dataToSend. In particolare veiene allocata la memoria per copiare il bundle serializzato dal file
 * in un buffer, ed il puntatore viene salvato in dataToSend->buffer
 *
 */

    UniboBPReceivedData* specificData = receivedData->bundleOpaque;

    UniboBPTransmission* transmission = specificData->current_pdu;

/*
 * Altre scatole cinesi
 * 1) UniboBPTransmission contiene il file descriptor del bundle serializzato + una serie di metadati
 * per la sua gestione e per il QoS
 * 2) UniboBPReceivedData ha solo due campi, link_id e current_pdu; il secondo è del tipo UniboBPTransmission
 * Fin qui...
 * Il punto è che il primo campo di UniboBPTransmission è un link_id
 * e qui non ci si capisce più nulla
 */

	dataToSend->bufferLength = transmission->length;

    int fd = transmission->fd;
	dataToSend->buffer = mallocWrapper(dataToSend->bufferLength);
    uint64_t remaining = dataToSend->bufferLength;
    uint8_t* cursor = (uint8_t*) dataToSend->buffer;
    const size_t chunk_size = 4096U;
    while (remaining) {
        size_t to_read = (remaining > chunk_size) ? chunk_size : remaining;
        ssize_t n_read = read(fd, cursor, to_read);
        if (n_read < 0) {
            close(fd);
            free_transmission_struct(transmission);
            return;
        }
        remaining -= n_read;
        cursor += n_read;
    }

	dataToSend->dataNumber = pushSendingListObject(transmission);
	dataToSend->handlerSessionTerminated = _handlerSessionTerminated;


    dataToSend->metadata=transmission->metadata;
//CCaini Having now only one type of metadata structure, common to transmission and dataToSend, it is possible
//to copy the metadata structure instead of its 3 components.
//  parseECOS(transmission->metadata.ecos, &dataToSend->metadata.ecos);
//	dataToSend->metadata.deadline = transmission->metadata.deadline;
//  dataToSend->metadata.reTx_counter = transmission->metadata.reTx_counter;

    close(fd);
    free_transmission_struct(transmission);

	// Unlock the other thread on canReceiveDataFromUpperProtocol
	sem_post(&receivedData->semCanReceiveNewData);
}

bool canReceiveDataFromUpperProtocol(unsigned long long destination, UpperProtocolReceivedData* receivedData) {
/*
 * Called by _mainGetterFromUpperProctol thread
 * immediately before passedDataFromUpperProtcol (other thread)
 * It is the function that removes one bundle ("transmission" structure) from the LTP queue when
 * permitted by the LTP flow control (number of concurrent sessions).
 * The "transmission" structure is copied into specificData->current_pdu
 * C'è al solito un gioco illeggibile di puntatori per in definitiva passare transmission alla
 * passedDataFromUpperProtcol (vedi sopra) che viene chiamata DOPO.
 */

	(void) destination; // unused
    if ( receivedData == NULL ) {
		printf("receiveDataFromUpperProtocol has NULL value(s)\n");
		return false;
	}

	sem_wait(&receivedData->semCanReceiveNewData);

    UniboBPReceivedData* specificData = receivedData->bundleOpaque;

    pthread_mutex_lock(get_global_mutex());

    UniboBPPeerData* peer_data = getPeerDataFromLinkID(specificData->link_id);

    while (list_length(peer_data->transmissions) <= 0) {
        pthread_cond_wait(peer_data->tx_cv, get_global_mutex());
    }
//Si sblocca ed arriva qui se ci sono dei dati da trasmettere, es. invio di un bundle da dtnperf)
    UniboBPTransmission* transmission = list_pop_front(&peer_data->transmissions, NULL);

/*
 * In the ION interface metadata are read here from ancillary data and copied in receivedData
 * Then receivedDat is copied in dataToSend in another function
 * Here it seems possible to write directly dataToSend metadata from transmission in the other function
 *
    receivedData->metadata.ecos = transmission->metadata.ecos;
    receivedData->metadata.deadline = transmission->metadata.deadline;
    receivedData->metadata.reTx_counter = transmission->metadata.reTx_counter;
*/
    specificData->current_pdu = transmission;

 //come caspita fa a passare fuori specificData?  Non capisco.

    pthread_mutex_unlock(get_global_mutex());

	sem_post(&receivedData->semIsDataPresent);

	return true;
}

bool initUpperProtocol() {

    pthread_mutexattr_t ma;
    pthread_mutexattr_init(&ma);
    pthread_mutexattr_settype(&ma, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init(get_global_mutex(), &ma);

    UniboBPError error;
    UniboBPCLAHandle* handle = get_cla_handle();

    error = unibo_bp_cla_connect_default(handle);

    if (error != UniboBP_NoError) {
        return false;
    }

    error = unibo_bp_cla_set_id(*handle, DEFAULT_LTP_CLA_ID);
    if (error != UniboBP_NoError) {
        unibo_bp_cla_disconnect(handle);
        return false;
    }

/*
 * The callback functions are defined in Unibo-BP. They are called here by Unibo-LTP to be notified of events
 *  happening on Unibo-BP, such as
 *  1) the addition/romoval/modification of contacts and ranges
 *  2) the modification of config_flags; these are deigned to take advantage of Unibo-BP commands
 *  to set configuration flags destined to Unibo-LTP at run_time, like what done with Unibo-CGR options.
 *  E.g. to consider or not the number of bundle retransmissions, or an ecos flag to set the LTP color
 *  3) that a bundle has been released by Unibo-BP flow control (throttled by contact times and nominal rate)
 *  ( outbound_pdu)
 *  When an event happens in Unibo-BP, Unibo-LTP is notified.
 */

    unibo_bp_cla_register_callback_update_config_flags(*handle, onUpdateConfigFlags);
    unibo_bp_cla_register_callback_add_contact(*handle, onAddContact);
    unibo_bp_cla_register_callback_remove_contact(*handle, onRemoveContact);
    unibo_bp_cla_register_callback_change_contact_start_time(*handle, NULL);   // not supported by Unibo-LTP
    unibo_bp_cla_register_callback_change_contact_end_time(*handle, NULL);     // not supported by Unibo-LTP
    unibo_bp_cla_register_callback_change_contact_xmit_rate(*handle, NULL);    // not supported by Unibo-LTP
    unibo_bp_cla_register_callback_add_range(*handle, onAddRange);
    unibo_bp_cla_register_callback_remove_range(*handle, onRemoveRange);
    unibo_bp_cla_register_callback_change_range_start_time(*handle, NULL);     // not supported by Unibo-LTP
    unibo_bp_cla_register_callback_change_range_end_time(*handle, NULL);       // not supported by Unibo-LTP
    unibo_bp_cla_register_callback_change_range_owlt(*handle, NULL);           // not supported by Unibo-LTP
    unibo_bp_cla_register_callback_cancel_transmission(*handle, NULL);         // not supported by Unibo-LTP
    unibo_bp_cla_register_callback_outbound_pdu(*handle, onOutboundPdu);
    unibo_bp_cla_register_callback_stop(*handle, onStop);

    error = unibo_bp_cla_set_cla_name(*handle, "LTPCLA");
    if (error != UniboBP_NoError) {
        unibo_bp_cla_disconnect(handle);
        return false;
    }
    return true;
}

void disposeUpperProtocol() {
    unibo_bp_cla_disconnect(get_cla_handle());
	list_destroy(get_sending_list());
    while (list_length(*get_transmission_list()) > 0) {
        UniboBPTransmission * transmission = list_pop_front(get_transmission_list(), NULL);
        free_transmission_struct(transmission);
    }
    while (list_length(*get_peer_data_list()) > 0) {
        UniboBPPeerData* peer_data = list_pop_front(get_peer_data_list(), NULL);
        free_peer_data(peer_data);
    }
    pthread_mutex_destroy(get_global_mutex());
}

void initUpperProtocolReceivedData(UpperProtocolReceivedData* receivedData, uint64_t peerEngineID, bool isReliableLink) {
    sem_init(&receivedData->semCanReceiveNewData, 0, 1);
    sem_init(&receivedData->semIsDataPresent, 0, 0);

    UniboBPCLAHandle* handle = get_cla_handle();
    UniboBPError error;
    UniboBPReceivedData* specificReceivedData = mallocWrapper(sizeof(UniboBPReceivedData));
    memset(specificReceivedData, 0, sizeof(UniboBPReceivedData));
    UniboBPIPN ipn_eid = NULL;
    error = unibo_bp_eid_ipn_create(peerEngineID, 0, &ipn_eid);
    if (error != UniboBP_NoError) {
        handleUniboBPError();
        return;
    }
    error = unibo_bp_cla_create_link(*handle,
                                     unibo_bp_const_cast_eid(ipn_eid),
                                     true,
                                     false,
                                     &specificReceivedData->link_id);
    if (error != UniboBP_NoError) {
        handleUniboBPError();
    }
    error = unibo_bp_cla_set_link_param(*handle, specificReceivedData->link_id, isReliableLink, 0);
    if (error != UniboBP_NoError) {
        handleUniboBPError();
    }

    receivedData->bundleOpaque = specificReceivedData;

    UniboBPPeerData peer_data;
    initialize_peer_data(unibo_bp_cast_eid(ipn_eid), peerEngineID, specificReceivedData->link_id, &peer_data);
    pthread_mutex_lock(get_global_mutex());
    list_append(&*get_peer_data_list(), &peer_data, sizeof(peer_data));
    pthread_mutex_unlock(get_global_mutex());

    error = unibo_bp_cla_start_observation_planned_neighbor(*handle, unibo_bp_const_cast_eid(ipn_eid));
    if (error != UniboBP_NoError) {
        handleUniboBPError();
    }

    error = unibo_bp_cla_open_link(*handle, specificReceivedData->link_id);
    if (error != UniboBP_NoError) {
        handleUniboBPError();
    }
}

void destroyUpperProtocolReceivedData(UpperProtocolReceivedData* receivedData) {
    UniboBPCLAHandle *handle = get_cla_handle();
    UniboBPReceivedData *specificReceivedData = receivedData->bundleOpaque;
    uint64_t link_id = specificReceivedData->link_id;
    unibo_bp_cla_destroy_link(*handle, link_id);
    sem_destroy(&receivedData->semCanReceiveNewData);
    sem_destroy(&receivedData->semIsDataPresent);
    pthread_mutex_lock(get_global_mutex());
    UniboBPPeerData *peer_data = getPeerDataFromLinkID(link_id);
    unibo_bp_cla_stop_observation_planned_neighbor(*handle, unibo_bp_const_cast_eid(peer_data->eid));
    clear_peer_data(peer_data);
    list_remove_data(get_peer_data_list(), &link_id, sizeof(link_id), _findPeerDataFromLinkID);
    pthread_mutex_unlock(get_global_mutex());
    FREE(specificReceivedData);
}

#endif /* HAVE_UNIBO_BP */
