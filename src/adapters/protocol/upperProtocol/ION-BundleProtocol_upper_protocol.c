/** \file ION-BundleProtocol_upper_protocol.c
 *
 * \brief This file contains the implementation of one upper protocol based on Bundle Protocol implemented in ION
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

extern int dummy_ion_variable_to_avoid_pedantic_error;

#ifdef HAVE_ION

#include "../upperProtocol.h"
#include "../upperProtocolReceivedData.h"

#include <stdio.h>
#include <sys/time.h>
#include <math.h>

#include "../../../logger/logger.h"
#include "../../../LTPsession/session.h"
#include "../../../list/list.h"
#include "../../../generic/generic.h"
#include "../../../contact/contact.h"
#include "../../../range/range.h"
#include "../../../LTPspan/span.h"
#include "../../../timer/timer.h"
#include "../../../timer/timerStruct.h"
#include "../../../config.h"

#include <bpP.h>
#include "rfx.h"
#include "bp.h"
#include "zco.h"
#include <llcv.h>
#include <stdbool.h>

/***** PRIVATE FUNCTIONS *****/

static void _handlerSessionTerminated(unsigned int dataNumber, LTPSessionResultCode resultCode);
static char* _numberToString(unsigned long long number);
static unsigned int _insertBundleZcoInSendingList(Object bundleZco);
static Object _getBundleZcoInSendingListFromDataNumber(unsigned int dataNumber);
static int _findInSendingListFromDataNumber(void* _dataNumber, size_t dataNumberSize, void* _sendingListObject, size_t sendingListObjectSize);
static void _handlerUpdateContactPlan(TimerID timerID, void* _span);

static void add_contact(IonCXref *IonContact);
static void add_new_contacts(PsmPartition ionwm, IonVdb *ionvdb);
static void add_new_ranges(PsmPartition ionwm, IonVdb *ionvdb);
static void add_range(IonRXref *IonRange);
static void update_contact_plan(PsmPartition ionwm, IonVdb *ionvdb);

/*!
 * \brief Sending List object
 */
typedef struct {
	/**
	 * \brief Data number
	 */
	unsigned int dataNumber;
	/**
	 * \brief Bundle ZCO
	 */
	Object bundleZco;
} SendingListObject;

static List _sendingList = empty_list;
static unsigned int nextDataNumber = 1;

static AcqWorkArea	*_work;
static VInduct		*_vduct;
static TimerID updateContactPlanTimerID;


unsigned int getUpperProtocolClientServiceID() {
	return 1; // Bundle Protocol is 1
}

void loadContactPlanUpperProtocol() {
	static bool firstTimeLoadContactPlanUpperProtocol = true;
	Sdr		sdr = getIonsdr();
	PsmPartition	ionwm;
	IonVdb		*ionvdb;

	if (sdr == NULL)
	{
		printf("Cannot get ION SDR\n");
		exit(-1);
	}

	if ( firstTimeLoadContactPlanUpperProtocol ) {
		doLog("Contact plan:\tFirst loading...");
		int cycles=0;// 0 means periodic timer
		updateContactPlanTimerID = startTimer(1000 * CONTACT_PLAN_UPDATE_TIME, cycles, _handlerUpdateContactPlan, NULL);
		firstTimeLoadContactPlanUpperProtocol = false;
	}

	ionwm = getIonwm();
	ionvdb = getIonVdb();
	sdr_begin_xn(sdr);
	update_contact_plan(ionwm, ionvdb);
	sdr_exit_xn(sdr);

	doLog("Contact plan:\tChecked. Next check in %d seconds", CONTACT_PLAN_UPDATE_TIME);

//	updateContactPlanTimerID = startTimer(1000 * CONTACT_PLAN_UPDATE_TIME, 1, _handlerUpdateContactPlan, NULL);
}

void deliverBlockToUpperProtocol(LTPBlock* block) {
	AcqWorkArea *work = bpGetAcqArea(_vduct);

	bpBeginAcq(work, 0, NULL);
	bpContinueAcq(work, block->data, block->blockLength, NULL, 0);
	bpEndAcq(work);

	bpReleaseAcqArea(work);
}

void passedDataFromUpperProtocol(unsigned long long destination, UpperProtocolDataToSend* dataToSend, UpperProtocolReceivedData* receivedData) {
	if ( dataToSend == NULL ) {
		doLog("receiveDataFromUpperProtocol has NULL value");
		return;
	}

	// Wait for the data (see function canReceiveDataFromUpperProtocol)
	sem_wait(&receivedData->semIsDataPresent);

	Sdr					sdr = getIonsdr();
	ZcoReader			reader;

	sdr_begin_xn(sdr);

    Object* bundleZco = (Object*) receivedData->bundleOpaque;
	zco_start_transmitting(*bundleZco, &reader);
	dataToSend->bufferLength = zco_length(sdr, *bundleZco); // Get the bundle length

	dataToSend->buffer = (char*) mallocWrapper(dataToSend->bufferLength);

	zco_transmit(sdr, &reader, dataToSend->bufferLength, dataToSend->buffer); // Puts data in buffer

	sdr_end_xn(sdr);

	dataToSend->dataNumber = _insertBundleZcoInSendingList(*bundleZco);
	dataToSend->handlerSessionTerminated = _handlerSessionTerminated;
//CCaini check if the instruction below could take the information directly form the bundle
//instead of from receivedData
//	dataToSend->isUnreliableBundle = receivedData->isUnreliableBundle;
	dataToSend->metadata.ecos.fields.bestEffort=receivedData->metadata.ecos.fields.bestEffort;
	dataToSend->metadata.ecos.fields.reliable=receivedData->metadata.ecos.fields.reliable;
	dataToSend->metadata.reTx_counter=0; //Temporary fix for compatibility with UniboBP
	// Unlock the other thread on canReceiveDataFromUpperProtocol
	sem_post(&receivedData->semCanReceiveNewData);
}

bool canReceiveDataFromUpperProtocol(unsigned long long destination, UpperProtocolReceivedData* receivedData) {
	if ( receivedData == NULL ) {
		printf("receiveDataFromUpperProtocol has NULL value(s)\n");
		return false;
	}

	BpAncillaryData		ancillaryData;
	VOutduct* 			vductOut;
	PsmAddress			vductElt;

	char* stringNodeNumber = _numberToString(destination);

	findOutduct("ltp", stringNodeNumber, &vductOut, &vductElt); // Find the outduct
	FREE(stringNodeNumber);

	if ( vductElt == 0 ) { // Not found
		fprintf(stderr, "Outduct not found. Probably ION is closed.\n");
		exit(0);
	}

	sem_wait(&receivedData->semCanReceiveNewData);

    Object* bundleZco = (Object*) receivedData->bundleOpaque;
	if ( bpDequeue(vductOut, bundleZco, &ancillaryData, -1) < 0 ) {
		fprintf(stderr, "Can't dequeue bundle\n");
		return false;
	}

	if(ancillaryData.flags & BP_BEST_EFFORT)

//CCaini to fix with the metadata.ecos fields of receivedData; also to extend to read all ecos fields
		receivedData->metadata.ecos.fields.bestEffort=true;
//		receivedData->isUnreliableBundle=true;
	else
		receivedData->metadata.ecos.fields.bestEffort=false;
		//		receivedData->isUnreliableBundle=false;
	if(ancillaryData.flags & BP_RELIABLE)//At present this flag is present but cannot be set in ION

//CCaini to fix with the metadata.ecos fields of receivedData; also to extend to read all ecos fields
		receivedData->metadata.ecos.fields.reliable=true;
//		receivedData->isUnreliableBundle=true;
	else
		receivedData->metadata.ecos.fields.reliable=false;
		//		receivedData->isUnreliableBundle=false;

	if ( *bundleZco == 0 || *bundleZco == 1 ) {
		return false;
	}

	sem_post(&receivedData->semIsDataPresent);

	return true;
}

bool initUpperProtocol() {
	VInduct			*vduct;
	PsmAddress		vductElt;

	if (bpAttach() < 0)
		return false;

	char* stringNodeNumber = _numberToString(getMyNodeNumber());

	findInduct("ltp", stringNodeNumber, &vduct, &vductElt); // Find the induct
	FREE(stringNodeNumber);

	if ( vductElt == 0 ) { // Not found induct
		return false;
	}

	_work = bpGetAcqArea(vduct); // this will get (and create) the work area used to deliver bundles to BP

	_vduct = vduct;

	return (_work != NULL);
}

void disposeUpperProtocol() {
	stopTimer(updateContactPlanTimerID);

	bpReleaseAcqArea(_work);
	bpDetach();
	list_destroy(&_sendingList);
}

void initUpperProtocolReceivedData(UpperProtocolReceivedData* receivedData, uint64_t peerEngineID, bool isReliableLink) {
    (void) peerEngineID; // unused
    (void) isReliableLink; // unused
    receivedData->bundleOpaque = mallocWrapper(sizeof(Object));
	sem_init(&receivedData->semCanReceiveNewData, 0, 1);
	sem_init(&receivedData->semIsDataPresent, 0, 0);
}

void destroyUpperProtocolReceivedData(UpperProtocolReceivedData* receivedData) {
    FREE(receivedData->bundleOpaque);
	sem_destroy(&receivedData->semCanReceiveNewData);
	sem_destroy(&receivedData->semIsDataPresent);
}

/***** PRIVATE FUNCTIONS *****/

static void _handlerSessionTerminated(unsigned int dataNumber, LTPSessionResultCode resultCode) {
	Object bundleZco = _getBundleZcoInSendingListFromDataNumber(dataNumber);

	//printf("Session with dataNumber %d terminated with code %d\tbundleZco = %d\n", dataNumber, resultCode, bundleZco);

	switch ( resultCode ) {
		case SessionResultSuccess:
#if (BP==6) //CCaini
			bpHandleXmitSuccess(bundleZco, 0); //in bpv6
#else
			bpHandleXmitSuccess(bundleZco); //in bp v7
#endif
			break;

		case SessionResultCancel:
		case SessionResultError:
			bpHandleXmitFailure(bundleZco);
			break;

		case SessionNoResult:
		default:
			doLog("Why here?");
			break;
	}

	list_remove_data(&_sendingList, &dataNumber, sizeof(dataNumber), _findInSendingListFromDataNumber);
}

static char* _numberToString(unsigned long long number) {
	int numberOfDigits;
	if ( number <= 9 ) {
		numberOfDigits = 1;
	} else {
		numberOfDigits = (int)((ceil(log10(number))));
	}
	char* numberInString = (char*) mallocWrapper(sizeof(char) * (numberOfDigits+1));
	if (numberInString == NULL)
		return NULL;
	sprintf(numberInString, "%llu", number); // transform node number in string
	numberInString[numberOfDigits] = '\0'; // let the node number string be a well-formatted string
	return numberInString;
}

static unsigned int _insertBundleZcoInSendingList(Object bundleZco) {
	SendingListObject sendingObject;
	sendingObject.bundleZco = bundleZco;
	sendingObject.dataNumber = nextDataNumber;
	nextDataNumber++; if ( nextDataNumber==0 ) nextDataNumber++;
	list_append(&_sendingList, &sendingObject, sizeof(sendingObject));
	return sendingObject.dataNumber;
}

static Object _getBundleZcoInSendingListFromDataNumber(unsigned int dataNumber) {
	SendingListObject *result = list_get_pointer_data(_sendingList, &dataNumber, sizeof(dataNumber), _findInSendingListFromDataNumber);
	return result->bundleZco;
}

static int _findInSendingListFromDataNumber(void* _dataNumber, size_t dataNumberSize, void* _sendingListObject, size_t sendingListObjectSize) {
	unsigned int* dataNumber = (unsigned int*)_dataNumber;
	SendingListObject* sendingListObject = (SendingListObject*) _sendingListObject;
	if ( *dataNumber == sendingListObject->dataNumber )
		return 0;
	else
		return 1;
}

static void add_contact(IonCXref *ionContact)
{
	Contact contact;

	if (ionContact->type == CtRegistration)
	{
		contact.fromNode = ionContact->fromNode;
		contact.toNode = ionContact->toNode;
		contact.fromTime = MAX_POSIX_TIME;
		contact.toTime = MAX_POSIX_TIME;
		contact.xmitRate = 0;
	}
	else if (ionContact->type == CtScheduled)
	{
		contact.fromNode = ionContact->fromNode;
		contact.toNode = ionContact->toNode;
		contact.fromTime = ionContact->fromTime;
		contact.toTime = ionContact->toTime;
		contact.xmitRate = ionContact->xmitRate;
	}

	LTPSpan *span = getSpanFromNodeNumber(contact.toNode);
	if ( span != NULL ) {
		addContactToSpan(&contact, span);
	}
	if ( contact.toNode != contact.fromNode ) {
		span = getSpanFromNodeNumber(contact.fromNode);
		if ( span != NULL ) {
			addContactToSpan(&contact, span);
		}
	}
}

static void add_new_contacts(PsmPartition ionwm, IonVdb *ionvdb)
{
	PsmAddress nodeAddr;
	IonCXref *currentIonContact = NULL;

	for (nodeAddr = sm_rbt_first(ionwm, ionvdb->contactIndex); nodeAddr != 0; nodeAddr = sm_rbt_next(ionwm, nodeAddr))
	{
		currentIonContact = psp(ionwm, sm_rbt_data(ionwm, nodeAddr));

		if (currentIonContact->type == CtRegistration || currentIonContact->type == CtScheduled)
		{
			add_contact(currentIonContact);
		}
	}
}

static void add_new_ranges(PsmPartition ionwm, IonVdb *ionvdb)
{
	PsmAddress nodeAddr;
	IonRXref *currentIonRange = NULL;

	for (nodeAddr = sm_rbt_first(ionwm, ionvdb->rangeIndex); nodeAddr != 0; nodeAddr = sm_rbt_next(ionwm, nodeAddr))
	{
		currentIonRange = psp(ionwm, sm_rbt_data(ionwm, nodeAddr));
		add_range(currentIonRange);
	}
}

static void add_range(IonRXref *IonRange)
{
	Range range;

	range.fromNode = IonRange->fromNode;
	range.toNode = IonRange->toNode;
	range.fromTime = IonRange->fromTime;
	range.toTime = IonRange->toTime;
	range.seconds = IonRange->owlt;

	LTPSpan *span = getSpanFromNodeNumber(range.toNode);
	if ( span != NULL ) {
		addRangeToSpan(&range, span);
	}
	if ( range.toNode != range.fromNode ) {
		span = getSpanFromNodeNumber(range.fromNode);
		if ( span != NULL ) {
			addRangeToSpan(&range, span);
		}
	}
}

static void update_contact_plan(PsmPartition ionwm, IonVdb *ionvdb)
{
	static struct timeval lastEditTime = {0, 0};

	if ( timercmp(&(ionvdb->lastEditTime), &lastEditTime, >) ) {

		if ( lastEditTime.tv_sec != 0 && lastEditTime.tv_usec != 0 ) { // Not the first time
			doLog("Contact plan:\tChanges found!");
		}

		add_new_contacts(ionwm, ionvdb);
		add_new_ranges(ionwm, ionvdb);

		lastEditTime.tv_sec = ionvdb->lastEditTime.tv_sec;
		lastEditTime.tv_usec = ionvdb->lastEditTime.tv_usec;
	}
}

static void _handlerUpdateContactPlan(TimerID timerID, void* _null) {
	loadContactPlanUpperProtocol();
}

#endif /* HAVE_ION */
