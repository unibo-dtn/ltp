/** \file UDP_lower_protocol.c
 *
 * \brief This file contains one implementation of the lower protocol which uses UDP
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <netinet/ip_icmp.h>
#include <netinet/udp.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/eventfd.h>
#include <sys/select.h>

#include "lowerProtocolAdapter.h"
#include "../lowerProtocol.h"
#include "../../../config.h"
#include "../../../generic/generic.h"
#include "../../../network/network_utils.h"

static bool _UDPinitInLowerLevel(char* initString);
static bool _UDPinitOutLowerLevel(char* initString, destination_struct* address);
static void _UDPdisposeLowerProtocol();
static bool _UDPreceivePacketFromLowerProtocol(char* buffer, int *bufferLength);
static bool _UDPsendPacketToLowerProtocol(char* buffer, int bufferLength, destination_struct destination);
static int _UDPsetFDForSelect (fd_set* fdstruct);
static bool _UDPreceivedFromFDForSelect (fd_set* fdstruct);

#define UDP_DEFAULT_PORT (1113)

#ifndef LOCALLYUSED
#define LOCALLYUSED
	int	parseSocketSpec2(char *ip, char * port, destination_struct *addr);
	bool getInternetAddress(char *hostname, destination_struct *addr);
#endif

static struct {
	int 				socketDescriptor;
} udpEnv;

LowerProtocolAdapter getUDPLowerProtocolAdapter() {
	LowerProtocolAdapter result;
	result.protocolName 			= "UDP";
	result.init 					= _UDPinitInLowerLevel;
	result.initOut 					= _UDPinitOutLowerLevel;

	result.dispose 					= _UDPdisposeLowerProtocol;
	result.send 					= _UDPsendPacketToLowerProtocol;
	result.receive		 			= _UDPreceivePacketFromLowerProtocol;

	result.setFDForSelect 			= _UDPsetFDForSelect;
	result.receivedFromFDForSelect	= _UDPreceivedFromFDForSelect;
	return result;
}

/***** PRIVATE FUNCTIONS *****/

static bool _UDPinitInLowerLevel(char* initString) {
	destination_struct	    from;
	struct sockaddr*		socketName;//it is the socket in input
	socklen_t nameLength = sizeof(struct sockaddr);

    if (parseHostnameAndPort(initString, &(from.inet),UDP_DEFAULT_PORT)<0) {
    	fprintf(stderr, "Error while parsing ip and port string=%s\n", initString);
    	return false;
    }

    switch (from.inet.ai_family) {
	case AF_INET:
		socketName = (struct sockaddr*) &(from.inet.address.ipv4);
		break;

	case AF_INET6:
		socketName = (struct sockaddr*) &(from.inet.address.ipv6);
		break;
	default:
		fprintf(stderr, "Error while parsing AI_family for string=%s\n", initString);
	    return false; // invalid IP protocol (neither IPv4 nor IPv6)
	}

	udpEnv.socketDescriptor = socket(from.inet.ai_family, SOCK_DGRAM, IPPROTO_UDP);
	if (udpEnv.socketDescriptor < 0)
	{
		fprintf(stderr, "Lower protocol can't open UDP socket\n");
		return false;
	}

	if ( bind(udpEnv.socketDescriptor, socketName, nameLength) < 0
		|| getsockname(udpEnv.socketDescriptor, socketName, &nameLength) < 0 )
	{
		close(udpEnv.socketDescriptor);
		fprintf(stderr, "Can't initialize UDP socket\n");
		return false;
	}

	struct timeval timeout = {0, 1000 * 500};//0s and 500 000 micros (500 ms)
	if (setsockopt(udpEnv.socketDescriptor, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) < 0 ) {
		fprintf(stderr, "setsockopt failed on UDP socket\n");
		return false;
	}

	return true;
}

static bool _UDPinitOutLowerLevel(char* initString, destination_struct* address) {
//The asymmetry with respect to the IN because I need to write to many IP addresses
//one for each neighbor
//This is why I do not create a socket here
    if (parseHostnameAndPort(initString, &(address->inet), UDP_DEFAULT_PORT)<0) {
    	fprintf(stderr, "Error while parsing ip and port string=%s\n", initString);
    	return false;
    }
	return true;
}

static void _UDPdisposeLowerProtocol() {
//It closes the input socket
	shutdown(udpEnv.socketDescriptor, SHUT_RD);
	close(udpEnv.socketDescriptor);
}

static bool _UDPreceivePacketFromLowerProtocol(char* buffer, int *bufferLength) {
	struct sockaddr_in	fromAddr;//system structure used to store the IP & port of the sender
	socklen_t			fromSize = sizeof(struct sockaddr_in);

	if ( bufferLength == NULL ) {
		fprintf(stderr, "bufferLength is NULL\n");
		return false;
	}
//socket descriptor is the fd of the input socket
	*bufferLength = recvfrom(udpEnv.socketDescriptor, buffer, LOWER_LEVEL_MAX_PACKET_SIZE,	0, (struct sockaddr *) &fromAddr, &fromSize);

	if ( *bufferLength <= 0 ) { // If socket timed out (500ms set in the initIN)
		return false;
	}

	return true;//a datagram has been read
}

static bool _UDPsendPacketToLowerProtocol(char* buffer, int bufferLength, destination_struct destination) {
	int fd;
	struct sockaddr* toAddr;//to address is the destination address
	bzero(&toAddr,sizeof(toAddr));
//Open a socket any time a new datagram must be sent
//I cannot use a sole socket because I have one thread for each destination
//It could be improved by introducing a socket pool
	fd = socket(destination.inet.ai_family, SOCK_DGRAM, 0);
	if ( fd < 0 ) {
		fprintf(stderr, "Cannot open UDP socket for sending\n");
		return false;
	}

	if(destination.inet.ai_family == AF_INET) {
		toAddr = (struct sockaddr*) &destination.inet.address.ipv4;
	} else {
		toAddr = (struct sockaddr*) &destination.inet.address.ipv6;
	}


	if ( sendto(fd, buffer, bufferLength, 0, (struct sockaddr *) toAddr, sizeof(struct sockaddr)) < 0 ) {
		fprintf(stderr, "Cannot send UDP datagram\n");
		close(fd);
		return false;
	}

	close(fd);//close the "one-time" socket
	return true;
}

static int _UDPsetFDForSelect (fd_set* fdstruct) {
	FD_SET(udpEnv.socketDescriptor, fdstruct);
	return udpEnv.socketDescriptor;
}

static bool _UDPreceivedFromFDForSelect (fd_set* fdstruct) {
	return FD_ISSET(udpEnv.socketDescriptor, fdstruct);
}
