/** \file lowerProtocolAdapter.h
 *
 * \brief This file contains the definitions of the adapter to a specific lower protocol
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef SRC_ADAPTERS_PROTOCOL_LOWERPROTOCOLADAPTER_H_
#define SRC_ADAPTERS_PROTOCOL_LOWERPROTOCOLADAPTER_H_

#include "../lowerProtocol.h"
/**
 * \brief The struct used to generalize one lower protocol adapter
 */
typedef struct {
	/**
	 * \brief The protocol name
	 */
	const char* 	protocolName;
	/**
	 * \brief The function which is called when the adapter is initialized
	 */
	bool 			(*init)(char* initString);
	/**
	* \brief The function which is called when the adapter is initialized toward a span
	*/
	bool			(*initOut)(char* initString, destination_struct* address);
	/**
	* \brief The function which is called to dispose the adapter
	*/
	void 			(*dispose)();
	/**
	* \brief The function which is called to receive data
	*/
	bool 			(*receive)(char* buffer, int *bufferLength);
	/**
	* \brief The function which is called to send data
	*/
	bool 			(*send)(char* buffer, int bufferLength, destination_struct address);

	/**
	* \brief The function which is called before the select. It must set the FD in the select & return the max FD number
	*/
	int 			(*setFDForSelect)(fd_set* fdstruct);
	/**
	* \brief The function which is called to check if the select returned one of the FD the adapter handles
	*/
	bool 			(*receivedFromFDForSelect)(fd_set* fdstruct);
} LowerProtocolAdapter;





/***** GETTER PROTOCOL SPECIFIC *****/

/**
 * \brief Function to get the UDP adapter
 */
LowerProtocolAdapter 		getUDPLowerProtocolAdapter();

/**
 * \brief Function to get the ECLSA adapter
 */
LowerProtocolAdapter 		getECLSALowerProtocolAdapter();



#endif /* SRC_ADAPTERS_PROTOCOL_LOWERPROTOCOLADAPTER_H_ */
