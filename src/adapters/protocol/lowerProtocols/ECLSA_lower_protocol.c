/** \file ECLSA_lower_protocol.c
 *
 * \brief This file contains one implementation of the lower protocol which uses ECLSA
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/eventfd.h>
#include <sys/select.h>
#include <ctype.h>

#include "lowerProtocolAdapter.h"
#include "../lowerProtocol.h"
#include "../../../config.h"
#include "../../../list/list.h"
#include "../../../generic/generic.h"
#include "../../../network/network_utils.h"

static bool _ECLSAinitLowerLevel(char* initString);
static bool _ECLSAinitOutLowerLevel(char* initString, destination_struct* address);
static void _ECLSAdisposeLowerProtocol();
static bool _ECLSAreceivePacketFromLowerProtocol(char* buffer, int *bufferLength);
static bool _ECLSAsendPacketToLowerProtocol(char* buffer, int bufferLength, destination_struct address);
static int  _ECLSAsetFDForSelect (fd_set* fdstruct);
static bool _ECLSAreceivedFromFDForSelect (fd_set* fdstruct);
static int  _findEclsoFDFromID (void* _ID, size_t IDSize, void* _eclsoFD, size_t eclsoFDSize);

/**
 * \brief ECLSI program
 *
 * \hideinitializer
 */
#define ECLSI_PROGRAM "eclsi"

/**
 * \brief ECLSO program
 *
 * \hideinitializer
 */
#define ECLSO_PROGRAM "eclso"

static int 		fdReadFromEclsi;
static List 	fdListToEclso = empty_list;

/**
 * \brief The ID used to get which ECLSO must be used
 */
//typedef struct {
//	ECLSA_address_struct address;
//} EclsoFdID;

/**
 * \brief ECLSO ID and file descriptor (pipe) used to communicate with it
 */
//typedef struct {
	/**
	 * \brief The ECLSO ID
	 */
//	EclsoFdID		ID;
	/**
	 * \brief The file descriptor (pipe)
	 */
	int 			fd;
//} EclsoFD;

LowerProtocolAdapter getECLSALowerProtocolAdapter() {
	LowerProtocolAdapter result;
	result.protocolName 			= "ECLSA";
	result.init 					= _ECLSAinitLowerLevel;
	result.initOut 					= _ECLSAinitOutLowerLevel;

	result.dispose 					= _ECLSAdisposeLowerProtocol;
	result.send 					= _ECLSAsendPacketToLowerProtocol;
	result.receive		 			= _ECLSAreceivePacketFromLowerProtocol;

	result.setFDForSelect 			= _ECLSAsetFDForSelect;
	result.receivedFromFDForSelect	= _ECLSAreceivedFromFDForSelect;
	return result;
}

/***** PRIVATE FUNCTIONS *****/

static bool _ECLSAinitLowerLevel(char* initString) {
	int pipe_fd[2];
	if ( pipe(pipe_fd) != 0 ) {
		fprintf(stderr, "Error on opening pipe for ECLSI\n");
		return false;
	}

	fdReadFromEclsi = pipe_fd[0];

	int childPID = fork();

	if ( childPID < 0 ) {
		fprintf(stderr, "Error on fork\n");
		return false;
	} else if ( childPID == 0 ) { // child
		close(pipe_fd[0]);

		dup2(pipe_fd[1], 3); // Move the pipe to the fd 3
		for (int i = 4; i <= 20; i++) { // Close other files
			if ( i != pipe_fd[1] ) close(i);
		}

		int count;
		char** argvString = str_split(initString, ' ', &count);
		char** argv = malloc(sizeof(char*) * (count + 2));

		argv[0] = ECLSI_PROGRAM;
		for(int i = 0; i < count; i++) {
			argv[i+1] = malloc(sizeof(char) * (strlen(argvString[i]) + 1));
			strcpy(argv[i+1], argvString[i]);
		}

		argv[count+1] = NULL;

		execvp(ECLSI_PROGRAM, argv);
		fprintf(stderr, "Failure on opening ECLSI\n");
		exit(0);

	} else { // Father
		close(pipe_fd[1]);
	}

	return true;
}

static bool _ECLSAinitOutLowerLevel(char* initString, destination_struct* address) {
	printf("ECLSA=%s\n", initString);
	//EclsoFD eclsoFD;

	int pipe_fd[2];
        int result;
	if ( pipe(pipe_fd) != 0 ) {
		fprintf(stderr, "Error on opening pipe to ECLSO\n");
		return false;
	}

	int childPID = fork();

	if ( childPID < 0 ) {
		fprintf(stderr, "Error on fork\n");
		return false;
	} else if ( childPID == 0 ) { // child
		close(pipe_fd[1]);

		dup2(pipe_fd[0], 3); // Move the pipe to the fd 3
		for (int i = 4; i <= 20; i++) { // Close other files
			if (i != pipe_fd[0]) close(i);
		}
		
		int count = 0;
		char** argvString = str_split(initString, ' ', &count);
		char** argv = malloc(sizeof(char*) * (count + 3));

		argv[0] = ECLSO_PROGRAM;
		for(int i = 0; i < count; i++) {
			argv[i+1] = malloc(sizeof(char) * (strlen(argvString[i]) + 1));
			strcpy(argv[i+1], argvString[i]);
		}

		//argv[count+1] = "_useless_param_";
		argv[count+1] = NULL;
		
		execvp(ECLSO_PROGRAM, argv);
		fprintf(stderr, "Failure on opening ECLSO\n");
		exit(0);

	} else { // Father
		close(pipe_fd[0]);
	}
	address->ECLSA_fd = pipe_fd[1];

	unsigned long long myNodeNumber = getMyNodeNumber();
	result=write(address->ECLSA_fd, &myNodeNumber, sizeof(unsigned long long));
	if ( result < 0 ) {
              fprintf(stderr, "Failure on writing myNodeNumber in ECLSO\n");
                exit(0);
	}

	/*
	int returned_value = read(pipe_fd[0], &(eclsoFD.ID.address.ai_family), sizeof(eclsoFD.ID.address.ai_family));
	if(returned_value < 0) {
		printf("Error on getting ECLSO process address!\n");
		exit(1);
	}
	if(eclsoFD.ID.address.ai_family == AF_INET) {
		eclsoFD.ID.address.ai_family = AF_INET;
		eclsoFD.ID.address.address.ipv4 = (struct sockaddr_in *) malloc(sizeof(struct sockaddr_in));
		returned_value = read(pipe_fd[0], &(eclsoFD.ID.address.address.ipv4->sin_addr), sizeof(eclsoFD.ID.address.address.ipv4->sin_addr));
		returned_value = read(pipe_fd[0], &(eclsoFD.ID.address.address.ipv4->sin_port), sizeof(eclsoFD.ID.address.address.ipv4->sin_port));
		returned_value = read(pipe_fd[0], &(eclsoFD.ID.address.address.ipv4->sin_family), sizeof(eclsoFD.ID.address.address.ipv4->sin_family));
	} else {
		if (eclsoFD.ID.address.ai_family == AF_INET6) {
			eclsoFD.ID.address.ai_family = AF_INET6;
			eclsoFD.ID.address.address.ipv6 = (struct sockaddr_in6 *) malloc(sizeof(struct sockaddr_in6));
			returned_value = read(pipe_fd[0], &(eclsoFD.ID.address.address.ipv6->sin6_addr), sizeof(eclsoFD.ID.address.address.ipv6->sin6_addr));
			returned_value = read(pipe_fd[0], &(eclsoFD.ID.address.address.ipv6->sin6_port), sizeof(eclsoFD.ID.address.address.ipv6->sin6_port));
			returned_value = read(pipe_fd[0], &(eclsoFD.ID.address.address.ipv6->sin6_family), sizeof(eclsoFD.ID.address.address.ipv6->sin6_family));
		} else {
			printf("Error on AI_FAMILY variable!\n");
			close(pipe_fd[0]);
			exit(1);
		}
	}
	close(pipe_fd[0]);
	*/

	//list_append(&fdListToEclso, &eclsoFD, sizeof(eclsoFD));

	return true;
}

static void _ECLSAdisposeLowerProtocol() {
	// TODO chiudere tutti i processi aperti (ECLSI e tutti gli ECLSO)
}

static bool _ECLSAreceivePacketFromLowerProtocol(char* buffer, int *bufferLength) {
	// Read the segment size
	int result = read(fdReadFromEclsi, bufferLength, sizeof(int));
	if ( result < 0 ) {
		fprintf(stderr, "Error, pipe to ECLSI is broken\n");
		exit(-1);
		return false;
	} else if ( result != sizeof(int) ) {
		return false;
	}

	// Read the segment
	result = read(fdReadFromEclsi, buffer, *bufferLength);
	if ( result < 0 ) {
		fprintf(stderr, "Error, pipe to ECLSI is broken\n");
		exit(-1);
		return false;
	} else if ( result != *bufferLength ) {
		return false;
	}

	return true;
}

static bool _ECLSAsendPacketToLowerProtocol(char* buffer, int bufferLength, destination_struct address) {
	/*EclsoFdID ID;
	ID.address = address;
	//ID.ipAddress = ipAddress;
	//ID.port = portNumber;

	EclsoFD* eclsoFD = (EclsoFD*) list_get_pointer_data(fdListToEclso, &ID, sizeof(ID), _findEclsoFDFromID);

	if ( eclsoFD == NULL ) {
		fprintf(stderr, "Error, ECLSO not found\n");
		exit(-1);
		return false;
	}*/

	// Write the segment size
	int result = write(address.ECLSA_fd, &bufferLength, sizeof(int));
	if ( result < 0 ) {
		fprintf(stderr, "Error, pipe to ECLSI is broken\n");
		exit(-1);
		return false;
	} else if ( result != sizeof(int) ) {
		return false;
	}

	// Read the segment
	result = write(address.ECLSA_fd, buffer, bufferLength);
	if ( result < 0 ) {
		fprintf(stderr, "Error, pipe to ECLSI is broken\n");
		exit(-1);
		return false;
	} else if ( result != bufferLength ) {
		return false;
	}

	return true;
}

static int _ECLSAsetFDForSelect (fd_set* fdstruct) {
	FD_SET(fdReadFromEclsi, fdstruct);
	return fdReadFromEclsi;
}

static bool _ECLSAreceivedFromFDForSelect (fd_set* fdstruct) {
	return FD_ISSET(fdReadFromEclsi, fdstruct);
}

/*static int _findEclsoFDFromID (void* _ID, size_t IDSize, void* _eclsoFD, size_t eclsoFDSize) {
	EclsoFdID* eclsoFdID = (EclsoFdID*) _ID;
	EclsoFD*   eclsoFD 	 = (EclsoFD*) 	_eclsoFD;
	if(eclsoFdID->address.ai_family == AF_INET) { // IPv4
		if ( eclsoFdID->address.address.ipv4->sin_addr.s_addr == eclsoFD->ID.address.address.ipv4->sin_addr.s_addr && eclsoFdID->address.address.ipv4->sin_port == eclsoFD->ID.address.address.ipv4->sin_port )
			return 0;
		else
			return -1; // Not equals
	} else {
		if(eclsoFdID->address.ai_family == AF_INET6) { // IPv6
			if ( eclsoFdID->address.address.ipv6->sin6_addr.s6_addr == eclsoFD->ID.address.address.ipv6->sin6_addr.s6_addr && eclsoFdID->address.address.ipv6->sin6_port == eclsoFD->ID.address.address.ipv6->sin6_port )
				return 0;
			else
				return -1; // Not equals
		} else {
			return -1; // wrong ai_family
		}
	}
}*/

