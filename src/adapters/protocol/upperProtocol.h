/** \file upperProtocol.h
 *
 * \brief This file contains the defines of the upper protocol
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef SRC_ADAPTERS_PROTOCOL_UPPERPROTOCOL_H_
#define SRC_ADAPTERS_PROTOCOL_UPPERPROTOCOL_H_

#include "../../LTPsession/session.h"
#include "upperProtocolReceivedData.h"

#include <sys/time.h>
#include <stdbool.h>

/*!
 * \brief Upper protocol data to send
 */
typedef struct {
	/**
	 * \brief The data to send
	 */
	char*						buffer;
	/**
	 * \brief The length of the data to send
	 */
	long long int				bufferLength;
	/**
	* \brief The bool to indicate if the bundle is unreliable
	*/
	//bool 	isUnreliableBundle;

	/**
	 * \brief The handler to call when at the end of the session (for success or failure)
	 */
	void	(*handlerSessionTerminated)(unsigned int, LTPSessionResultCode);
	/**
	 * \brief The number used to recognize this sending
	 */
	unsigned int		 		dataNumber;

	LTPBundleMetadata metadata;
} UpperProtocolDataToSend;

/**
 * \par Function Name:
 *      initUpperProtocol
 *
 * \brief  Initializes the upper protocol
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return bool
 *
 * \retval  True if success, false in case of errors.
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
bool 	initUpperProtocol				();

/**
 * \par Function Name:
 *      disposeUpperProtocol
 *
 * \brief  Disposes the upper protocol
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 	disposeUpperProtocol			();


/**
 * \par Function Name:
 *      deliverBlockToUpperProtocol
 *
 * \brief  Delivers the block to the upper protocol
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  block 		The block to deliver
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void	deliverBlockToUpperProtocol		(LTPBlock* block);

/**
 * \par Function Name:
 *      canReceiveDataFromUpperProtocol
 *
 * \brief  Tells if can receive data from the upper protocol
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return bool
 *
 * \retval  True if data are present, false otherwise
 *
 * \param  destination 		The destination toward it have to check if there are new data
 * \param  receivedData		The received data
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
bool 	canReceiveDataFromUpperProtocol(unsigned long long destination, UpperProtocolReceivedData* receivedData);

/**
 * \par Function Name:
 *      passedDataFromUpperProtocol
 *
 * \brief  Passes data to send from the upper protocol
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  destination 		The destination toward it have to get the data
 * \param  dataToSend		The data to send structure to be written
 * \param  receivedData		The received data
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 	passedDataFromUpperProtocol(unsigned long long destination, UpperProtocolDataToSend* dataToSend, UpperProtocolReceivedData* receivedData);

/**
 * \par Function Name:
 *      getUpperProtocolCleintServiceID
 *
 * \brief  Sends data to lower protocol
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return unsigned int
 *
 * \retval  The upper protocol client service ID
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
unsigned int getUpperProtocolClientServiceID();

/**
 * \par Function Name:
 *      loadContactPlanUpperProtocol
 *
 * \brief  loads the contact plan from the upper protocol
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 	loadContactPlanUpperProtocol();


#endif /* SRC_ADAPTERS_PROTOCOL_UPPERPROTOCOL_H_ */
