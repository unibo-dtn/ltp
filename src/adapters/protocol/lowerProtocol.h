/** \file lowerProtocol.h
 *
 * \brief This file contains the definitions of the lower protocol
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef SRC_ADAPTERS_PROTOCOL_LOWERPROTOCOL_H_
#define SRC_ADAPTERS_PROTOCOL_LOWERPROTOCOL_H_

#include <stdbool.h>
#include "../../network/network_utils.h"

/**
 * \brief Max packet size of lower protocol
 *
 * \hideinitializer
 */
#define LOWER_LEVEL_MAX_PACKET_SIZE ((256*256)-1)

typedef struct {
	INET_struct inet;
	int ECLSA_fd;
	//Add other address types if necessary
} destination_struct;

/**
 * \par Function Name:
 *      initLowerLevel
 *
 * \brief  Initializes the lower protocol
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return bool
 *
 * \retval  True if success, false in case of errors.
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
bool 	initLowerLevel ();

/**
 * \par Function Name:
 *      disposeLowerProtocol
 *
 * \brief  Disposes the lower protocol
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 	disposeLowerProtocol			();

/**
 * \par Function Name:
 *      receivePacketFromLowerProtocol
 *
 * \brief  Receives a packet from lower protocol
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return bool
 *
 * \retval  True if success, false in case of errors.
 *
 * \param  buffer 			The buffer where write the received data
 * \param  bufferLength		The length of the buffer
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
bool 	getSerializedSegmentFromLowerProtocol	(char* buffer, int *bufferLength);

/**
 * \par Function Name:
 *      sendPacketToLowerProtocol
 *
 * \brief  Sends data to lower protocol
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return bool
 *
 * \retval  True if success, false in case of errors.
 *
 * \param  protocol		The protocol to use
 * \param  buffer 		The data to send
 * \param  bufferLength The length of the buffer
 * \param  address		The structure that contains the receiver address
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *  -------- | --------------- | -----------------------------------------------
 *  23/05/24 | A. Lo Buglio    |  Modifying spans.json file parser
 *****************************************************************************/
bool	passSerializedSegmentToLowerProtocol		(const char* protocol, char* buffer, int bufferLength, destination_struct address);

/**
 * \par Function Name:
 *      initLowerLevelProtocol
 *
 * \brief  It initializes the lower protocol
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return bool
 *
 * \retval  True if success, false in case of errors.
 *
 * \param  protocol		The protocol to initialize
 * \param  initString 	The string to init the protocol
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
bool 	initLowerLevelProtocol(const char* protocol, char* initString);

/**
 * \par Function Name:
 *      disposeLowerLevelProtocol
 *
 * \brief  It disposes the lower protocol
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  protocol		The protocol to dispose
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 	disposeLowerLevelProtocol(const char* protocol);

/**
 * \par Function Name:
 *      initOutLowerLevel
 *
 * \brief  It initializes the protocol toward a span
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return bool
 *
 * \retval  True if success, false in case of errors.
 *
 * \param  protocol		The protocol to use
 * \param  span 		The span number
 * \param  initString	The string usedto initialize
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *  -------- | --------------- | -----------------------------------------------
 *  23/05/24 | A. Lo Buglio    |  Modifying spans.json file parser
 *****************************************************************************/
bool 	initOutLowerLevel(const char* protocol, unsigned int span, const char* initString, destination_struct* address);

#endif /* SRC_ADAPTERS_PROTOCOL_LOWERPROTOCOL_H_ */
