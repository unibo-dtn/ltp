/** \file upperProtocolReceivedData.h
 *
 * \brief This file contains the defines of UpperProtocolReceivedData struct
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef SRC_ADAPTERS_PROTOCOL_UPPERPROTOCOLRECEIVEDDATA_H_
#define SRC_ADAPTERS_PROTOCOL_UPPERPROTOCOLRECEIVEDDATA_H_

#include <stdint.h>
#include <stdbool.h>


//CCaini This file is used by all BP implementations, therefore it cannot include dependencies
//of specific BP implementation, such as Unibo-BP. This is why I have inserted sa new enum
//ECOSCardinalPriority instead of relying on the equivalent of Unibo-BP.
//#include "Unibo-BP/ECOS.h"

typedef struct {
    bool critical;
    bool bestEffort;
    bool qosTagIsPresent;                     
    bool reliable; 
    bool bssp;
    bool bibe;
    bool bibeCustody;
} LTPEcos_fields;

typedef enum ECOSCardinalPriority {
    ECOSCardinalPriority_bulk      = 0,
    ECOSCardinalPriority_normal    = 1,
    ECOSCardinalPriority_expedited = 2,
    ECOSCardinalPriority_unknown   = 3,
} ECOSCardinalPriority;

typedef struct {
	ECOSCardinalPriority cardinal;
	int ordinal;
} LTPEcos_priority;

typedef struct {
	LTPEcos_fields fields;
	LTPEcos_priority priority;
	uint32_t QoS_tag;
} LTPEcos;


typedef struct {
    LTPEcos ecos; //PARSED ECOS of the bundle
    struct timeval deadline; // deadline to complete the LTP Tx session
    uint64_t reTx_counter; // number of bundle retransmissions
} LTPBundleMetadata;



/*!
 * \brief Upper protocol received data
 */
typedef struct {
	/**
	 * \brief The Bundle ZCO
	 */
	void* 	bundleOpaque;
	/**
	* \brief The bool to indicate if the bundle is unreliable
	*/
	//bool 	isUnreliableBundle;
	/**
	 * \brief The semaphore to indicate if data is present
	 */
	sem_t 	semIsDataPresent;
	/**
	 * \brief The semaphore to indicate if can receive new data
	 */
	sem_t 	semCanReceiveNewData;

	LTPBundleMetadata metadata; //necessary for ION
} UpperProtocolReceivedData;


/**
 * \par Function Name:
 *      initUpperProtocolReceivedData
 *
 * \brief  Initializes the received data
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  receivedData 		The received data to initialize
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void initUpperProtocolReceivedData(UpperProtocolReceivedData* receivedData, uint64_t peerEngineID, bool isReliableLink);

/**
 * \par Function Name:
 *      destroyUpperProtocolReceivedData
 *
 * \brief  Destroys the received data
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  receivedData 	The received data
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void destroyUpperProtocolReceivedData(UpperProtocolReceivedData* receivedData);


#endif /* SRC_ADAPTERS_PROTOCOL_UPPERPROTOCOLRECEIVEDDATA_H_ */
