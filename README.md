# Unibo-LTP

Unibo-LTP is an experimental implementation of the LTP protocol [1].

It is part of the Unibo-DTN "umbrella" project that aims to implement the most important DTN protocols.
Give a glance to https://gitlab.com/unibo-dtn/ to see our ancillary projects.

#### Features

Unibo-LTP is essentially compliant with RFC5326 and CCSDS blue book on LTP, but it supports only monochrome blocks

It contains a few experimental features, such as:

- a new experimental color (orange) in addition to red and green.
- max red session length timers (to close stalled red sessions)
- max inter-segment timer (to close green and orange Rx-sessions after the max inter-segment timer has elapsed)
- the possibility to set a default session color for each span
- a token-bucket traffic shaper, to control both the average Tx rate and the max burst dimension

Notes:

- LTP green segments are never passed to the bundle protocol unless the full block has arrived (i.e. missing parts are
  not filled with zeros as in ION)
- Bundle aggregation (as in CCSDS blue book) has not been implemented yet, i.e. one bundle is always encapsulated in
  one LTP block.

Unibo-LTP 2.x.x releases are compatible with both ION and Unibo-BP bundle protocol implementations.

Unibo-LTP can use as Link Service adapters, i.e. protocols below LTP, either UDP or ECLSA (
see https://gitlab.com/unibo-dtn/elcsa).

Unibo-LTP is independently built (it has its own Makefile), configured (spans.json file) and launched (in ION
in alternative to ION-LTP, which must be disabled first). Details on how to compile and use are given in next sessions.

#### Copyright

Copyright (c) 2021, University of Bologna.

#### License

Unibo-LTP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.  
Unibo-CGR is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.  
You should have received a copy of the GNU General Public License along with Unibo-LTP. If not,
see <http://www.gnu.org/licenses/>.

#### Dependencies

Unibo-LTP requires a `libjson` parser. On Debian machines you can install it with the following command:

```bash
sudo apt-get install libjson-c-dev
```

#### Building and installing

From the ltp directory, which contains the Unibo-LTP Makefile:

```bash
make < UNIBO_BP=1 | ION_DIR=<ion_dir> >
sudo make install
```

add `DEBUG=1` to compile with debug symbols

e.g.:

```bash
make UNIBO_BP=1 DEBUG=1
```

To set compatibility with ION BPv6, add `BP=bpv6`; `BP=bpv7` is default.

Note that Unibo-BP is v7 only.

A few defaults, such as max session timer, max inter-segment timer, etc. can be overridden by changing
their values in `/ltp/src/config.h`

#### Configuration file (`spans.json`)

Unibo-LTP configuration is based on the `spans.json` file, which must be located in the working directory.

Most settings are the same as those usually inserted in the `ltpadmin` section of ION configuration files, with a few
exception
for new features, such as the token bucket traffic shaper.
See the template file included in tha package.

In Unibo-LTP, as in ION, there is a receiver serving all neighbors and one dedicated "span" to each neighbor
(which can alternatively use UDP or ECLSA).

Note that you can alternatively select UDP or ECLSA as lower protocols for each outgoing span, while UDP and ECLSA can be
concurrently in use ingoing (of course listening on different ports, such as 1113 for udp and 1114 for eclsi).

#### Use with Unibo-BP

Steps:

1) Configure contacts and ranges in Unibo-BP (e.g. by editing Unibo-BP start.sh file)
2) disable concurrent CLs pointing to the same neighbors of wanted LTP spans
3) Write span settings in the spans.json file (start by modifying the template)
4) start Unibo-BP as usual
5) start Unibo-LTP with this syntax (`--log` to enable standard output logs)

```bash
UniboLTP -n nodenumber --log
```

e.g. `UniboLTP -n 2 --log`

For other options enter:

```bash
UniboLTP --help
```

#### Use with ION

Steps:

1) Configure ION as if you wanted to use ION-LTP and test the correctness of your configuration before going on
2) Translate your `ltpadmin` settings into the `spans.json` file (start from the template)
3) Disable ION-LTP. A convenient way is just to comment the "enable" line in `ltpadmin`
4) start ION as usual
5) start Unibo-LTP as described in the "Use with Unibo-BP" section above

#### Logs

Unibo-LTP is designed with research and teaching in mind, thus comprehensive logs are one of the most distinctive
features.

There are two kinds of logs, one both of which can be selectively enabled at run-time.

- `-- log` Print verbose logs on standard output
- `--csv` Print logs (one line per segment) in Unibo-LTP.csv file

The former is mainly intended for teaching and debugging;
the latter, for research (they can be used in input of dedicated tools to extract a large variety of LTP statistics, as
done in [2]).

#### Documentation

Advanced non-standard features are fully documented in:

[1] A. Bisacchi, C. Caini and T. de Cola, "Multicolor Licklider Transmission Protocol: An LTP Version for Future
Interplanetary Links," in IEEE Transactions on Aerospace and Electronic Systems, vol. 58, no. 5, pp. 3859-3869, Oct.
2022,
doi: 10.1109/TAES.2022.3176847. (Open Source) <https://doi.org/10.1109/TAES.2022.3176847>

Unibo-LTP use to assess LTP performance, including non-standard features, is shown in:

[2] C. Caini, T. de Cola, A. Shrestha and A. Zappacosta, "LTP Performance on Near Earth Optical Links,"
in IEEE Transactions on Aerospace and Electronic Systems,
vol. 59, no. 6, pp. 9501-9511, Dec. 2023, doi: 10.1109/TAES.2023.3322392 (Open
Source) <https://doi.org/10.1109/TAES.2023.3322392>

#### Known issues

The current implementation of timer freezing is inaccurate in case of asymmetric contact intervals.

#### Credits

- Carlo Caini (co-author of 2.x.x release and project supervisor), carlo.caini@unibo.it
- Andrea Bisacchi (main author of first release), andreabisacchi@gmail.com
- Lorenzo Persampieri (author of the interface to Unibo-BP in 2.x.x releases)
